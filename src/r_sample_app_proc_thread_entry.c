/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

/*******************************************************************************
 * File Name : r_sample_app_proc_thread_entry.c
 * Description : Sample application processing thread entry
 ******************************************************************************/

/******************************************************************************
 * Includes
 ******************************************************************************/
#include "r_typedefs.h"
#include "r_g3mac_sap.h"
#include "r_adp_sap.h"
#include "r_demo_app_thread.h"
#include "r_sample_app_proc_thread.h"

/*!
 \fn void r_sample_app_proc_thread_entry(void)
 \brief Sample Application ProcessingThread entry function.
 */
void r_sample_app_proc_thread_entry(void)
{
    /* Periodically call the application processing thread. */
    while (1)
    {
        /* The processing function internally waits for new content in the queue. */
        R_DEMO_AppThread ();
    }
}
