/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_common.c
*    @version
*        $Rev: 2142 $
*    @last editor
*        $Author: a0202438 $
*    @date  
*        $Date:: 2016-09-15 17:22:30 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include <stdlib.h>
#include "r_typedefs.h"
#include "r_config.h"
#include "r_byte_swap.h"
#include "r_c3sap_api.h"
#include "r_g3_sap.h"
#include "r_adp_sap.h"
#ifdef R_SYNERGY_PLC
#include "tx_api.h"
#include "nx_api.h"
#else
#include "r_io_vec.h"
#include "r_ipv6_headers.h"
#include "r_udp_headers.h"
#include "r_icmp_v6.h"
#include "r_check_sum.h"
#endif
#include "r_demo_app.h"
#include "r_demo_common.h"
#include "r_demo_api.h"
#include "r_stdio_api.h"


/******************************************************************************
Macro definitions
******************************************************************************/
#define R_ICMP_ID_OFFSET                    (4u)    /*!< ICMP header offset for identifier */
#define R_ICMP_SEQ_OFFSET                   (6u)    /*!< ICMP header offset for sequence number */
#ifdef R_SYNERGY_PLC
#define R_ICMP_ECHO_MESSAGE_HEADER_LENGTH   (8u)    /*!< Length of the header for an ICMP echo message (incl. identifier and sequence fields) */
#endif
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: generate_linklocal_ipv6_address
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void generate_linklocal_ipv6_address(const uint16_t panId,
                                            const uint16_t address,
                                            uint8_t   *pAddr);

/******************************************************************************
* Function Name: handle_remote_getset_req
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t handle_remote_getset_req(const uint8_t* nsduIn,
                                           const r_ipv6_hdr_t* ipv6HdrIn,
                                           uint16_t* ipPayloadLength,
                                           r_boolean_t* allFramesSent);
#endif
/******************************************************************************
Exported global variables
******************************************************************************/
#ifdef R_SYNERGY_PLC
extern NX_IP                            ip_0;               /* NetXDuo IP Control Block */
extern NX_PACKET_POOL                   pool_0;             /* NetXDuo packet pool */
extern NX_UDP_SOCKET                    udp_socket_0xABCD;  /* UDP socket control block for port 0xABCD */
extern NX_UDP_SOCKET                    udp_socket_0xF0BF;  /* UDP socket control block for port 0xF0BF */

#else
extern r_demo_entity_t                  g_demo_entity;
#endif
extern r_demo_buff_t                    g_demo_buff;
extern r_demo_config_t                  g_demo_config;

extern TX_THREAD                        lora_main;
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Functions
******************************************************************************/
/******************************************************************************
* Function Name: R_DEMO_GenerateUdpFrame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_GenerateUdpFrame(const uint16_t n,
                             const uint16_t panId,
                             const uint16_t srcAddress,
                             const uint16_t dstAddress,
                             const char*    msg_kayboard_input)
{
#ifdef R_SYNERGY_PLC

    UNUSED(srcAddress);

    uint16_t i;

    /* Set payload to msg_kayboard_input */
    for (i = 0; i < n; i++)
    {
        *(uint8_t*) (((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE) + i) = (uint8_t)(msg_kayboard_input[i]);
    }


    /* Declare variables to hold the destination address and the status. */
    NXD_ADDRESS ip_address;
    UINT        status;
    NX_PACKET   *packet_ptr;

    /* Allocate NetX packet. */
    if (nx_packet_allocate(&pool_0,
                           &packet_ptr,
                           NX_IPv6_UDP_PACKET,
                           TX_NO_WAIT) != NX_SUCCESS)
    {
        R_STDIO_Printf("\n UDP packet allocation failed.");
        return;
    }

    /* Append receive packet. */
    if (nx_packet_data_append(packet_ptr,
                              (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE,
                              n,
                              &pool_0,
                              TX_NO_WAIT) != NX_SUCCESS)
    {
        R_STDIO_Printf("\n UDP payload preparation failed.");
        return;
    }

    /* Set the IPv6 address. */
    ip_address.nxd_ip_version = NX_IP_VERSION_V6;
    ip_address.nxd_ip_address.v6[0] = 0xFE800000;
    ip_address.nxd_ip_address.v6[1] = 0;
    ip_address.nxd_ip_address.v6[2] = (ULONG) (panId << 16) + 0xFFu;
    ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + dstAddress;

    status = nxd_udp_socket_send(&udp_socket_0xABCD,
                                 packet_ptr,
                                 &ip_address,
                                 0xF0BF);

    R_STDIO_Printf("\n UDP transmission to port 0xF0BF returned with status 0x%.2X.", status);
#else

    uint16_t i;
    uint16_t sumWrittenBytes;

    r_ipv6_hdr_t   ipv6Hdr;
    r_udp_hdr_t udpHdr;

    /* Set payload to increasing sequence */
    for (i = 0; i < n; i++)
    {
        *(uint8_t*) (((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE) + i) = (uint8_t)(i % 256);
    }

    /* Prepare NSDU */
    ipv6Hdr.version       = 0x06;
    ipv6Hdr.trafficClass  = 0x00;
    ipv6Hdr.flowLabel     = 0x00;
    ipv6Hdr.payloadLength = (uint16_t)( R_UDP_HEADER_SIZE + n);
    ipv6Hdr.nextHdr       = R_IPV6_NEXT_HDR_UDP; // UDP
    ipv6Hdr.hopLimit      = 0x01;
    generate_linklocal_ipv6_address(panId, srcAddress,ipv6Hdr.src);

    if (dstAddress == 0xFFFF)
    {
        /* All nodes address (broadcast) */
        ipv6Hdr.dst[0]  = 0xFF;
        ipv6Hdr.dst[1]  = 0x02;
        ipv6Hdr.dst[2]  = 0x00;
        ipv6Hdr.dst[3]  = 0x00;
        ipv6Hdr.dst[4]  = 0x00;
        ipv6Hdr.dst[5]  = 0x00;
        ipv6Hdr.dst[6]  = 0x00;
        ipv6Hdr.dst[7]  = 0x00;
        ipv6Hdr.dst[8]  = 0x00;
        ipv6Hdr.dst[9]  = 0x00;
        ipv6Hdr.dst[10]  = 0x00;
        ipv6Hdr.dst[11]  = 0x00;
        ipv6Hdr.dst[12]  = 0x00;
        ipv6Hdr.dst[13]  = 0x00;
        ipv6Hdr.dst[14]  = 0x00;
        ipv6Hdr.dst[15]  = 0x01;
    }
    else
    {
        /* Group addresses (multicast) Sec. 9 RFC4944 */
        generate_linklocal_ipv6_address(panId, dstAddress,ipv6Hdr.dst);
    }

    udpHdr.srcPort = 0xABCD;
    udpHdr.dstPort = 0xF0BF;
    udpHdr.length = (uint16_t)(R_UDP_HEADER_SIZE + n);

    /* Pack IPv6 header. */
    if (R_IPV6_PackHeader(&ipv6Hdr,
                        g_demo_buff.Nsdu,
                        &sumWrittenBytes) == R_RESULT_SUCCESS)
    {
        /* Compute UDP checksum. */
        if (R_UDP_ComputeCheckSum(&ipv6Hdr.src[0],
                                &ipv6Hdr.dst[0],
                                (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE,
                                &udpHdr,
                                &udpHdr.checkSum) == R_RESULT_SUCCESS)
        {
            /* Pack UDP header. */
            if (R_UDP_PackHeader(&udpHdr,
                                 g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) == R_RESULT_SUCCESS)
            {
                /* Set data request */
                r_adp_adpd_data_req_t req;
                r_adp_adpd_data_cnf_t *cnf;

                req.discoverRoute   = R_TRUE;
                req.qualityOfService   = R_G3MAC_QOS_NORMAL;
                req.pNsdu              = g_demo_buff.Nsdu;
                req.nsduLength     = (uint16_t)((n + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE);
                req.nsduHandle     = g_demo_entity.nsduHandle++;

                /* Call ADPD-DATA function. */
                R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH, &req, &cnf);

            }
        }
    }
#endif
}
/******************************************************************************
   End of function  R_DEMO_GenerateUdpFrame
******************************************************************************/

#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: R_DEMO_ReplyUdpFrame
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_ReplyUdpFrame(const uint8_t* nsduIn,
                          const r_ipv6_hdr_t* ipv6HdrIn)
{
    uint16_t     ipPayloadLength;
    r_boolean_t  allFramesSent = R_FALSE;
    uint16_t sumWrittenBytes;

    r_ipv6_hdr_t  ipv6HdrOut;
    r_udp_hdr_t   udpHdrIn;
    r_udp_hdr_t   udpHdrOut;

    /* Create ioVec element. Two elements for message and dispatch. */
    IOVEC_CREATE(ioVec, 1)
    IOVEC_INIT(ioVec)

    /* Parse UDP header. */
    R_UDP_UnpackHeader(nsduIn + R_IPV6_HEADER_SIZE,
                     &udpHdrIn);

    /* Check if destination port is the expected one. */
    if(udpHdrIn.dstPort == 0xD000)
    {
        if (R_BYTE_ArrToUInt32((nsduIn + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE) == R_CAP_SYNCWORD)
        {
            /* Handle remote get set request. */
            if (handle_remote_getset_req(nsduIn,
                                            ipv6HdrIn,
                                            &ipPayloadLength,
                                            &allFramesSent) != R_RESULT_SUCCESS)
            {
                /* Message creation failed, return. */
                return;
            }

        }
        else
        {
            return;
        }
    }
    else if (((udpHdrIn.dstPort == 0xF0BF) ||
        (udpHdrIn.dstPort == 0xF0B8)) ||
        (udpHdrIn.dstPort == 0xF0B0))
    {
        /* Copy payload. */
        R_memcpy((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE,
                 (nsduIn + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE,
                 ipv6HdrIn->payloadLength - R_UDP_HEADER_SIZE);

        /* Payload length identical to incoming message. */
        ipPayloadLength = ipv6HdrIn->payloadLength;
    }
    else
    {
        /* ignore the port */
        return;
    }

    /* Prepare NSDU */
    ipv6HdrOut.version       = ipv6HdrIn->version;
    ipv6HdrOut.trafficClass = ipv6HdrIn->trafficClass;
    ipv6HdrOut.flowLabel     = ipv6HdrIn->flowLabel;
    ipv6HdrOut.payloadLength = ipPayloadLength;
    ipv6HdrOut.nextHdr       = ipv6HdrIn->nextHdr;
    ipv6HdrOut.hopLimit     = ipv6HdrIn->hopLimit;

    if(0xFF == ipv6HdrIn->dst[0])
    {
        /* Multicast case use LinlLocal src address */
        generate_linklocal_ipv6_address(g_demo_entity.panId, g_demo_entity.shortAddress,ipv6HdrOut.src);
    }
    else
    {
        R_memcpy(ipv6HdrOut.src, ipv6HdrIn->dst, 16);
    }
    R_memcpy(ipv6HdrOut.dst, ipv6HdrIn->src, 16);

    udpHdrOut.srcPort = udpHdrIn.dstPort;
    udpHdrOut.dstPort = udpHdrIn.srcPort;
    udpHdrOut.length= ipPayloadLength;

    /* Pack IPv6 header. */
    if (R_IPV6_PackHeader(&ipv6HdrOut,
                            g_demo_buff.Nsdu,
                            &sumWrittenBytes) == R_RESULT_SUCCESS)
    {

        /* Compute UDP checksum. */
        if (R_UDP_ComputeCheckSum(&ipv6HdrOut.src[0],
                                    &ipv6HdrOut.dst[0],
                                    (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE,
                                    &udpHdrOut,
                                    &udpHdrOut.checkSum) == R_RESULT_SUCCESS)
        {
            /* Pack UDP header. */
            if (R_UDP_PackHeader(&udpHdrOut,
                                 g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) == R_RESULT_SUCCESS)
            {
                /* Set data request */
                r_adp_adpd_data_req_t req;
                r_adp_adpd_data_cnf_t *cnf;

                req.discoverRoute   = R_TRUE;
                req.qualityOfService   = R_G3MAC_QOS_NORMAL;
                req.pNsdu              = g_demo_buff.Nsdu;
                req.nsduLength     = (uint16_t)(ipv6HdrOut.payloadLength + R_IPV6_HEADER_SIZE);
                req.nsduHandle     = g_demo_entity.nsduHandle++;

                /* Call ADPD-DATA function. */
                R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH, &req, &cnf);
            }
        }
    }
    }
/******************************************************************************
   End of function R_DEMO_ReplyUdpFrame
******************************************************************************/
#endif

/******************************************************************************
* Function Name: R_DEMO_GenerateIcmpRequest
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_GenerateIcmpRequest(const uint16_t n,
                                const uint16_t panId,
                                const uint16_t srcAddress,
                                const uint16_t dstAddress)
{
#ifdef R_SYNERGY_PLC

    uint16_t i;

    /* Set payload to 0xFF. */
    for (i = 0; i < n; i++)
    {
        *(uint8_t*) ((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + (R_ICMP_ECHO_MESSAGE_HEADER_LENGTH + i)) = 0xFF;
    }

    UNUSED(srcAddress);

    /* Declare variables to hold the destination address,
       the status and the response ICMP packet. */
    NXD_ADDRESS ip_address;
    UINT        status;
    NX_PACKET   *response_ptr;

    /* Set the IPv6 address. */
    ip_address.nxd_ip_version = NX_IP_VERSION_V6;
    ip_address.nxd_ip_address.v6[0] = 0xFE800000;
    ip_address.nxd_ip_address.v6[1] = 0;
    ip_address.nxd_ip_address.v6[2] = (ULONG) (panId << 16) + 0xFFu;
    ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + dstAddress;

    /* Create an entry in the neighbor cache with for the destination address to
       avoid transmission of additional NS messages. The hardware address will not
       be used by the IPv6 only network driver, so it is set to all zero. */
    UCHAR hw_address[6] = {0, 0, 0, 0, 0, 0};

    /* Call neighbor cache set function. */
    status = nxd_nd_cache_entry_set(&ip_0,
                                    &ip_address.nxd_ip_address.v6[0],
                                    R_DEMO_G3_USE_PRIMARY_CH,
                                    (char*) hw_address);

    /* Check status. */
    if (status != 0)
    {
        R_STDIO_Printf("\n Neighbor cache setting failed.");
        return;
    }

    /* Manually set value for ICMP sequence number to be compliant to certification test specification. */
    ip_0.nx_ip_icmp_sequence = 0x0506;

    /* To follow the certification test specification ICMP ID value has to be set to 0x0102. As a work around this is
       currently done in the demo driver implementation. */

    /* Trigger ICMP ping. */
    status = nxd_icmp_ping(&ip_0,
                           &ip_address,
                           (char*) (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                           n,
                           &response_ptr,
                           1000);

    /* Print status. */
    R_STDIO_Printf("\n Ping process returned with status 0x%.2X.", status);

#else
    uint16_t i;

    uint16_t sumWrittenBytes;

    r_ipv6_hdr_t   ipv6Hdr;

    /* Create ioVec element. Two elements for message and dispatch. */
    IOVEC_CREATE(ioVec, 2)
    IOVEC_INIT(ioVec)

    /* Set payload to 0xFF. */
    for (i = 0; i < n; i++)
    {
        *(uint8_t*) ((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + (R_ICMP_ECHO_MESSAGE_HEADER_LENGTH + i)) = 0xFF;
    }

    /* Prepare NSDU */
    ipv6Hdr.version       = 0x06;
    ipv6Hdr.trafficClass  = 0x00;
    ipv6Hdr.flowLabel     = 0x00;
    ipv6Hdr.payloadLength = (uint16_t)(R_ICMP_ECHO_MESSAGE_HEADER_LENGTH + n);
    ipv6Hdr.nextHdr       = R_IPV6_NEXT_HDR_ICMPV6; // UDP
    ipv6Hdr.hopLimit      = 0x01;

    generate_linklocal_ipv6_address(panId, srcAddress,ipv6Hdr.src);

    if (dstAddress == 0xFFFF)
    {
        /* All nodes address (broadcast) */
        ipv6Hdr.dst[0]  = 0xFF;
        ipv6Hdr.dst[1]  = 0x02;
        ipv6Hdr.dst[2]  = 0x00;
        ipv6Hdr.dst[3]  = 0x00;
        ipv6Hdr.dst[4]  = 0x00;
        ipv6Hdr.dst[5]  = 0x00;
        ipv6Hdr.dst[6]  = 0x00;
        ipv6Hdr.dst[7]  = 0x00;
        ipv6Hdr.dst[8]  = 0x00;
        ipv6Hdr.dst[9]  = 0x00;
        ipv6Hdr.dst[10]  = 0x00;
        ipv6Hdr.dst[11]  = 0x00;
        ipv6Hdr.dst[12]  = 0x00;
        ipv6Hdr.dst[13]  = 0x00;
        ipv6Hdr.dst[14]  = 0x00;
        ipv6Hdr.dst[15]  = 0x01;
    }
    else
    {
        /* Group addresses (multicast) Sec. 9 RFC4944 */
        generate_linklocal_ipv6_address(panId, dstAddress,ipv6Hdr.dst);
    }

    /* Pack IPv6 header. */
    if (R_IPV6_PackHeader(&ipv6Hdr,
                        g_demo_buff.Nsdu,
                        &sumWrittenBytes) == R_RESULT_SUCCESS)
    {
        /* Create echo message. */
        if (R_ICMP_CreateEchoMessage(&ioVec,
                                     R_TRUE,
                                     0x0102,
                                     0x0506,
                                     n,
                                     (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                                     g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) == R_ICMP_RESULT_SUCCESS)
        {
            /* Compute ICMP checksum. */
            if (R_ICMP_Checksum(&ipv6Hdr,
                                &ioVec) == R_ICMP_RESULT_SUCCESS)
            {
                /* Set data request */
                r_adp_adpd_data_req_t req;
                r_adp_adpd_data_cnf_t *cnf;

                req.discoverRoute   = R_TRUE;
                req.qualityOfService   = R_G3MAC_QOS_NORMAL;
                req.pNsdu              = g_demo_buff.Nsdu;
                req.nsduLength     = (uint16_t)((n + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH);
                req.nsduHandle     = g_demo_entity.nsduHandle++;

                /* Call ADPD-DATA function. */
                R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH, &req, &cnf);
            }
        }
    }
#endif
}
/******************************************************************************
   End of function  R_DEMO_GenerateIcmpRequest
******************************************************************************/

#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: R_DEMO_ReplyIcmpRequest
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_ReplyIcmpRequest(const uint8_t* nsduIn,
                             const r_ipv6_hdr_t* ipv6HdrIn)
{
    uint16_t     sumWrittenBytes;
    uint16_t     ipPayloadLength;
    r_ipv6_hdr_t ipv6HdrOut;
    r_boolean_t  allFramesSent = R_FALSE;

    /* Create ioVec element. Two elements for message and dispatch. */
    IOVEC_CREATE(ioVec, 2)
    IOVEC_INIT(ioVec)

    /* Pointer check. */
    if ((nsduIn == NULL) ||
     (ipv6HdrIn == NULL))
    {
        return;
    }

    /* Loop until all response packets have been sent. Normally that is only
     a single one, but for larger logging buffers it could be more. */
    while(allFramesSent == R_FALSE)
    {
        /* By default we send only a single frame, so set flag to TRUE. */
        allFramesSent = R_TRUE;

        /* Check if this is an ICMP echo request. */
        if ((*(nsduIn + R_IPV6_HEADER_SIZE)) == 0x80)
        {
        /* Check if this is a valid message. */
        if (R_BYTE_ArrToUInt32((nsduIn + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH) == R_CAP_SYNCWORD)
        {
            /* Handle remote get set request. */
            if (handle_remote_getset_req(nsduIn,
                                            ipv6HdrIn,
                                            &ipPayloadLength,
                                            &allFramesSent) != R_RESULT_SUCCESS)
            {
                /* Message creation failed, return. */
                return;
            }
        }
        else
        {
            /* Copy payload. */
            R_memcpy((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                     (nsduIn + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                     ipv6HdrIn->payloadLength - R_ICMP_ECHO_MESSAGE_HEADER_LENGTH);

            /* Payload length identical to incoming message. */
            ipPayloadLength = ipv6HdrIn->payloadLength;
        }

        /* Prepare NSDU */
        ipv6HdrOut.version       = ipv6HdrIn->version;
        ipv6HdrOut.trafficClass = ipv6HdrIn->trafficClass;
        ipv6HdrOut.flowLabel     = ipv6HdrIn->flowLabel;
        ipv6HdrOut.payloadLength = ipPayloadLength;
        ipv6HdrOut.nextHdr       = ipv6HdrIn->nextHdr;
        ipv6HdrOut.hopLimit     = ipv6HdrIn->hopLimit;

        if(0xFF == ipv6HdrIn->dst[0])
        {
            /* Multicast case use LinlLocal src address */
            generate_linklocal_ipv6_address(g_demo_entity.panId, g_demo_entity.shortAddress,ipv6HdrOut.src);
        }
        else
        {
            R_memcpy(ipv6HdrOut.src, ipv6HdrIn->dst, 16);
        }
        R_memcpy(ipv6HdrOut.dst, ipv6HdrIn->src, 16);

        /* Pack IPv6 header. */
        if (R_IPV6_PackHeader(&ipv6HdrOut,
                                g_demo_buff.Nsdu,
                                &sumWrittenBytes) == R_RESULT_SUCCESS)
        {
            /* Create echo message. */
            if (R_ICMP_CreateEchoMessage(&ioVec,
                                         R_FALSE,
                                         R_BYTE_ArrToUInt16((nsduIn + R_IPV6_HEADER_SIZE) + R_ICMP_ID_OFFSET),
                                         R_BYTE_ArrToUInt16((nsduIn + R_IPV6_HEADER_SIZE) + R_ICMP_SEQ_OFFSET),
                                         (uint16_t)(ipv6HdrIn->payloadLength - R_ICMP_ECHO_MESSAGE_HEADER_LENGTH),
                                         (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                                         g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) == R_ICMP_RESULT_SUCCESS)
            {
                /* Compute ICMP checksum. */
                if (R_ICMP_Checksum(&ipv6HdrOut,
                                    &ioVec) == R_ICMP_RESULT_SUCCESS)
                {
                    /* Set data request */
                    r_adp_adpd_data_req_t req;
                    r_adp_adpd_data_cnf_t *cnf;

                    req.discoverRoute   = R_TRUE;
                    req.qualityOfService   = R_G3MAC_QOS_NORMAL;
                    req.pNsdu              = g_demo_buff.Nsdu;
                    req.nsduLength     = (uint16_t)(ipv6HdrOut.payloadLength + R_IPV6_HEADER_SIZE);
                    req.nsduHandle     = g_demo_entity.nsduHandle++;

                    /* Call ADPD-DATA function. */
                    R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH, &req, &cnf);

                    /* Clear IOVEC for next iteration. */
                    R_IO_VecRese(&ioVec);
                }
            }
        }
        }
    }
}
/******************************************************************************
   End of function R_DEMO_ReplyIcmpRequest
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_ReplyIcmpRequestExtHeaders
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_ReplyIcmpRequestExtHeaders(const uint8_t* nsduIn,
                                       const r_ipv6_hdr_t* ipv6HdrIn)
{
    uint16_t sumWrittenBytes;
    uint16_t realHopByHopHdrLength;
    uint16_t realDspOptionsHdrLength;
    r_ipv6_hdr_t  ipv6HdrOut;
    uint8_t nextHdrLength;

    /* Create ioVec element. Two elements for message and dispatch. */
    IOVEC_CREATE(ioVec, 2)
    IOVEC_INIT(ioVec)

    /* get hop by hop header's length */
    nextHdrLength = nsduIn[R_IPV6_HEADER_SIZE + 1]; /* Length of this header in 8-octet units, not including the first 8 octets */
    realHopByHopHdrLength = (uint16_t)((nextHdrLength + 1) * 8); /* real length in bytes */

    /* get destination options header's length */
    nextHdrLength = nsduIn[(R_IPV6_HEADER_SIZE + realHopByHopHdrLength) + 1]; /* Length of this header in 8-octet units, not including the first 8 octets */
    realDspOptionsHdrLength = (uint16_t)((nextHdrLength + 1) * 8); /* real length in bytes */

    /* Check if this is a request. */
    if ((*((nsduIn + R_IPV6_HEADER_SIZE) + (realHopByHopHdrLength + realDspOptionsHdrLength))) == 0x80)
    {
    /* Prepare NSDU */
    ipv6HdrOut.version       = ipv6HdrIn->version;
    ipv6HdrOut.trafficClass = ipv6HdrIn->trafficClass;
    ipv6HdrOut.flowLabel     = ipv6HdrIn->flowLabel;
    ipv6HdrOut.payloadLength = (uint16_t)((ipv6HdrIn->payloadLength - realHopByHopHdrLength) - realDspOptionsHdrLength);
    ipv6HdrOut.nextHdr       = R_IPV6_NEXT_HDR_ICMPV6; /* echo reply */
    ipv6HdrOut.hopLimit     = ipv6HdrIn->hopLimit;

    if(0xFF == ipv6HdrIn->dst[0])
    {
        /* Multicast case use LinlLocal src address */
        generate_linklocal_ipv6_address(g_demo_entity.panId, g_demo_entity.shortAddress,ipv6HdrOut.src);
    }
    else
    {
        R_memcpy(ipv6HdrOut.src, ipv6HdrIn->dst, 16);
    }

    R_memcpy(ipv6HdrOut.dst, ipv6HdrIn->src, 16);

    /* Pack IPv6 header. */
    if (R_IPV6_PackHeader(&ipv6HdrOut,
                            g_demo_buff.Nsdu,
                            &sumWrittenBytes) == R_RESULT_SUCCESS)
    {
        /* Copy payload. */
        R_memcpy((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                 ((nsduIn + R_IPV6_HEADER_SIZE) + (realHopByHopHdrLength + realDspOptionsHdrLength)) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                 (size_t)((uint16_t)((ipv6HdrIn->payloadLength - realHopByHopHdrLength) - realDspOptionsHdrLength) - R_ICMP_ECHO_MESSAGE_HEADER_LENGTH));

        /* Create echo message. */
        if (R_ICMP_CreateEchoMessage(&ioVec,
                                     R_FALSE,
                                     R_BYTE_ArrToUInt16((((nsduIn + R_IPV6_HEADER_SIZE) + realHopByHopHdrLength) + realDspOptionsHdrLength) + R_ICMP_ID_OFFSET),
                                     R_BYTE_ArrToUInt16((((nsduIn + R_IPV6_HEADER_SIZE) + realHopByHopHdrLength) + realDspOptionsHdrLength) + R_ICMP_SEQ_OFFSET),
                                     (uint16_t)(((ipv6HdrIn->payloadLength - R_ICMP_ECHO_MESSAGE_HEADER_LENGTH) - realHopByHopHdrLength) - realDspOptionsHdrLength),
                                     (g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH,
                                     g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) == R_ICMP_RESULT_SUCCESS)
        {
            /* Compute ICMP checksum. */
            if (R_ICMP_Checksum(&ipv6HdrOut,
                                &ioVec) == R_ICMP_RESULT_SUCCESS)
            {
                /* Set data request */
                r_adp_adpd_data_req_t req;
                r_adp_adpd_data_cnf_t *cnf;

                req.discoverRoute   = R_TRUE;
                req.qualityOfService   = R_G3MAC_QOS_NORMAL;
                req.pNsdu              = g_demo_buff.Nsdu;
                req.nsduLength     = (uint16_t)(ipv6HdrOut.payloadLength + R_IPV6_HEADER_SIZE);
                req.nsduHandle     = g_demo_entity.nsduHandle++;

                /* Call ADPD-DATA function. */
                R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH, &req, &cnf);

            }
        }
    }
    }
}
/******************************************************************************
   End of function R_DEMO_ReplyIcmpRequestExtHeaders
******************************************************************************/
#endif

#ifdef R_SYNERGY_PLC
/******************************************************************************
* Function Name: R_DEMO_AppHandleUDPIndication
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleUDPIndication (NX_UDP_SOCKET *socket_ptr)
{
    NX_PACKET   *packet_ptr;
    NXD_ADDRESS ip_address;
    UINT        status;
    UINT        port;
    UCHAR       buffer_test[512];
    ULONG       bytes_copied_test;
    /* Retrieve UDP packet. */
    if (nx_udp_socket_receive(&udp_socket_0xABCD,
                              &packet_ptr,
                              NX_NO_WAIT) == NX_SUCCESS)
    {
        /* This UPD frame needs to be answered. */
        R_STDIO_Printf("\n Received UDP frame on port 0xABCD.");
    }
    else if (nx_udp_socket_receive(&udp_socket_0xF0BF,
                                   &packet_ptr,
                                   NX_NO_WAIT) == NX_SUCCESS)
    {
        /* This UPD frame needs to be answered. */
//        R_STDIO_Printf("\n Received UDP frame on port 0xF0BF.");

        if (nx_packet_data_retrieve(packet_ptr,
                                    buffer_test,
                                    &bytes_copied_test) == NX_SUCCESS)
        {
            /* If status is NX_SUCCESS, buffer_test contains the contents of the
            packet, the size of which is contained in "bytes_copied_test." */
            R_STDIO_Printf("\n Received data: \n buffer_test=%s\n",buffer_test);
//            R_STDIO_Printf("\n Size of received data: \n bytes_copied_test=%lu\n",bytes_copied_test);
        }

        /* In case of cert/auto mode, send response frame. */
        if (g_demo_config.appMode)
        {
            /* Extracting source information. */
            if (nxd_udp_source_extract(packet_ptr,
                                       &ip_address,
                                       &port) != NX_SUCCESS)
            {
                R_STDIO_Printf("\n Could not extract UDP frame.");
                return;
            }

            /* Send reply frame. */
            status = nxd_udp_socket_send(&udp_socket_0xF0BF,
                                         packet_ptr,
                                         &ip_address,
                                         port);

//            R_STDIO_Printf("\n Transmission of UDP response with status 0x%.2X.", status);
        }
    }
    else
    {
        R_STDIO_Printf("\n Could not retrieve UDP frame.");
        return;
    }

    UNUSED (socket_ptr);
}

void R_DEMO_TestTest ()
{
//    R_STDIO_Printf("\n Test Test \n.");

}

void R_DEMO_AppHandleUDPReceive (NX_UDP_SOCKET *socket_ptr)
{
NX_PACKET   *packet_ptr;
    NXD_ADDRESS ip_address;
    UINT        status;
    UINT        port;

    UINT        new_status;
    UCHAR       buffer_test[512];
    uint8_t     buffer_test_uint8[512];
    ULONG       bytes_copied_test;
    uint8_t     q_message[15] = {0};
    /* Retrieve UDP packet. */
    if (nx_udp_socket_receive(&udp_socket_0xABCD,
                              &packet_ptr,
                              NX_NO_WAIT) == NX_SUCCESS)
    {
        /* This UPD frame needs to be answered. */
        R_STDIO_Printf("\n Received UDP frame on port 0xABCD.");
    }
    else if (nx_udp_socket_receive(&udp_socket_0xF0BF,
                                   &packet_ptr,
                                   NX_NO_WAIT) == NX_SUCCESS)
    {
        /* This UPD frame needs to be answered. */
//        R_STDIO_Printf("\n Received UDP frame on port 0xF0BF.");

        /* In case of cert/auto mode, send response frame. */

        /* Retrieve data from packet pointed to by "packet_ptr". */
        if (nx_packet_data_retrieve(packet_ptr,
                                    buffer_test,
                                    &bytes_copied_test) == NX_SUCCESS)
        {
            /* If status is NX_SUCCESS, buffer_test contains the contents of the
            packet, the size of which is contained in "bytes_copied_test." */

            for (uint16_t i=0; i<= bytes_copied_test; i++)
            {
                buffer_test_uint8[i] = (uint8_t) buffer_test[i];
            }
            buffer_test_uint8[bytes_copied_test+1] = '\0';
			
			/* Print the received data */
            R_STDIO_Printf("\n Received data: \n buffer_test=%s\n",buffer_test_uint8);

        }

        if (g_demo_config.appMode)
        {
            /* Extracting source information. */
            if (nxd_udp_source_extract(packet_ptr,
                                       &ip_address,
                                       &port) != NX_SUCCESS)
            {
                R_STDIO_Printf("\n Could not extract UDP frame.");
                return;
            }

            /* Send reply frame. */
            status = nxd_udp_socket_send(&udp_socket_0xF0BF,
                                         packet_ptr,
                                         &ip_address,
                                         port);
        }
    }
    else
    {
        R_STDIO_Printf("\n Could not retrieve UDP frame.");
        return;
    }

    UNUSED (socket_ptr);
}
/******************************************************************************
   End of function R_DEMO_AppHandleUDPReceive
******************************************************************************/
#endif

#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: generate_linklocal_ipv6_address
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void generate_linklocal_ipv6_address(
                                const uint16_t panId,
                                const uint16_t address,
                                uint8_t   *pAddr)
{
    pAddr[0]        = 0xFE; // SRC: Prefix 8*8 bits = 64 bits
    pAddr[1]        = 0x80;
    pAddr[2]        = 0x00;
    pAddr[3]        = 0x00;
    pAddr[4]        = 0x00;
    pAddr[5]        = 0x00;
    pAddr[6]        = 0x00;
    pAddr[7]        = 0x00;
    pAddr[8]        = (uint8_t)((panId >> 8) & 0x00FF); // 16-bit PAN ID
    pAddr[9]        = (uint8_t)(panId & 0x00FF);        // 16-bit PAN ID
    pAddr[10]        = 0x00; // padding
    pAddr[11]        = 0xFF;
    pAddr[12]        = 0xFE;
    pAddr[13]        = 0x00;
    pAddr[14]        = (uint8_t)((address >> 8) & 0x00FF); // 16 bits short addr
    pAddr[15]        = (uint8_t)(address & 0x00FF); // 16 bits short addr

}
/******************************************************************************
   End of function  generate_linklocal_ipv6_address
******************************************************************************/

/******************************************************************************
* Function Name: handle_remote_getset_req
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t handle_remote_getset_req(const uint8_t* nsduIn,
                                     const r_ipv6_hdr_t* ipv6HdrIn,
                                     uint16_t* ipPayloadLength,
                                     r_boolean_t* allFramesSent)
{
    uint16_t                   checkSum;

    r_cap_command_header_t*    capCommandHdrIn;
    r_cap_command_header_t*    capCommandHdrOut;
    r_cap_cmd_capm_set_req_t*  capSetRequest;
    r_cap_cmd_capm_get_req_t*  capGetRequest;
    r_cap_cmd_capm_set_cnf_t*  capSetConfirm;
    r_cap_cmd_capm_get_cnf_t*  capGetConfirm;

    /* Create ioVec element. Two elements for message and dispatch. */
    IOVEC_CREATE(ioVec, 2)
    IOVEC_CREATE(ioVecChecksum, 1)

    UNUSED(ipv6HdrIn);
    UNUSED(allFramesSent);

    IOVEC_INIT(ioVec)
    IOVEC_INIT(ioVecChecksum)

    /* Set pointer for response message. */
    capCommandHdrOut    = (r_cap_command_header_t*) ((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + sizeof(r_cap_icmp_header_t));
    capSetConfirm    = (r_cap_cmd_capm_set_cnf_t*) ((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + (sizeof(r_cap_icmp_header_t) + sizeof(r_cap_command_header_t)));
    capGetConfirm    = (r_cap_cmd_capm_get_cnf_t*) ((g_demo_buff.Nsdu + R_IPV6_HEADER_SIZE) + (sizeof(r_cap_icmp_header_t) + sizeof(r_cap_command_header_t)));

    /* Cast NSDU pointer to CAP command header structure and to ICMP CAP header structure. */
    capCommandHdrIn = (r_cap_command_header_t*) ((nsduIn + R_IPV6_HEADER_SIZE) + sizeof(r_cap_icmp_header_t));

    /* Check if this is a set or get request. */
    if (R_BYTE_ArrToUInt16(capCommandHdrIn->CmdID) == 0x0802)
    {
        /* Handle SET request, fist cast pointer. */
        capSetRequest = (r_cap_cmd_capm_set_req_t*) ((nsduIn + R_IPV6_HEADER_SIZE) + (sizeof(r_cap_icmp_header_t) +  sizeof(r_cap_command_header_t)));

        /* Set length. */
        R_BYTE_UInt16ToArr(sizeof(r_cap_cmd_capm_set_cnf_t), capCommandHdrOut->length);

        /* Set common elements for reply message. */
        R_memcpy(capSetConfirm->index, capSetRequest->index, sizeof(capSetRequest->index));
        R_memcpy(capSetConfirm->id, capSetRequest->id, sizeof(capSetRequest->id));
        capSetConfirm->type = capSetRequest->type;

        /* Switch over types. */
        switch (capSetRequest->type)
        {
            case R_CAP_TYPE_ID_GET_SET_ADP:

                    /* Handle ADPM-SET.request. */
                    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH,(r_adp_ib_id_t) R_BYTE_ArrToUInt16(capSetRequest->id), R_BYTE_ArrToUInt16(capSetRequest->index),capSetRequest->payload) == R_ADP_STATUS_SUCCESS)
                    {
                        /* Set status. */
                        R_BYTE_UInt32ToArr(CAP_SUCCESS, capSetConfirm->Status);
                    }
                    else
                    {
                        /* Set status. */
                        R_BYTE_UInt32ToArr(CAP_FAILURE, capSetConfirm->Status);
                    }
                break;

            case R_CAP_TYPE_ID_GET_SET_MAC:

                    /* Handle MLME-SET.request. */
                    if (R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH,R_BYTE_ArrToUInt16(capSetRequest->id), R_BYTE_ArrToUInt16(capSetRequest->index),capSetRequest->payload) == R_G3MAC_STATUS_SUCCESS)
                    {
                        /* Set status. */
                        R_BYTE_UInt32ToArr(CAP_SUCCESS, capSetConfirm->Status);
                    }
                    else
                    {
                        /* Set status. */
                        R_BYTE_UInt32ToArr(CAP_FAILURE, capSetConfirm->Status);
                    }
                break;

            case R_CAP_TYPE_ID_GET_SET_STATS:

                /* Set status. */
                R_BYTE_UInt32ToArr(CAP_SUCCESS, capSetConfirm->Status);
                break;

            case R_CAP_TYPE_ID_GET_SET_LOGS:

                /* Set status. */
                R_BYTE_UInt32ToArr(CAP_SUCCESS, capSetConfirm->Status);
                break;

            default:
                break;
        }
    }
    else if (R_BYTE_ArrToUInt16(capCommandHdrIn->CmdID) == 0x0803)
    {
        /* Handle GET request, fist cast pointer. */
        capGetRequest = (r_cap_cmd_capm_get_req_t*) ((nsduIn + R_IPV6_HEADER_SIZE) + (sizeof(r_cap_icmp_header_t) +  sizeof(r_cap_command_header_t)));

        /* Set common elements for reply message. */
        R_memcpy(capGetConfirm->index, capGetRequest->index, sizeof(capGetRequest->index));
        R_memcpy(capGetConfirm->id, capGetRequest->id, sizeof(capGetRequest->id));
        capGetConfirm->type = capGetRequest->type;

        /* Switch over types. */
        switch (capGetRequest->type)
        {
            case R_CAP_TYPE_ID_GET_SET_ADP:

                    /* Handle ADPM-GET.request. */

                    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, (r_adp_ib_id_t)R_BYTE_ArrToUInt16(capGetRequest->id), R_BYTE_ArrToUInt16(capGetRequest->index), g_demo_buff.getStringBuffer) == R_ADP_STATUS_SUCCESS)
                    {
                         /* Set length. */
                         R_BYTE_UInt16ToArr(sizeof(r_cap_cmd_capm_get_cnf_t), capCommandHdrOut->length);

                         /* Fill structure. */
                         R_memcpy(capGetConfirm->payload, g_demo_buff.getStringBuffer, R_CAP_CAPSET_MAX_LEN);

                         /* Set status. */
                         R_BYTE_UInt32ToArr(CAP_SUCCESS, capGetConfirm->Status);
                    }
                    else
                    {
                         /* Set status. */
                         R_BYTE_UInt32ToArr(CAP_FAILURE, capGetConfirm->Status);
                    }
                break;

            case R_CAP_TYPE_ID_GET_SET_MAC:

                    /* Handle ADPM-GET.request. */
                    if (R_DEMO_MlmeGetWrap(R_DEMO_G3_USE_PRIMARY_CH, (r_g3mac_ib_id_t)R_BYTE_ArrToUInt16(capGetRequest->id), R_BYTE_ArrToUInt16(capGetRequest->index), g_demo_buff.getStringBuffer) == R_G3MAC_STATUS_SUCCESS)
                    {
                         /* Set length. */
                         R_BYTE_UInt16ToArr(R_CAP_CAPSET_MAX_LEN, capCommandHdrOut->length);

                         /* Fill structure. */
                         R_memcpy(capGetConfirm->payload, g_demo_buff.getStringBuffer, R_CAP_CAPSET_MAX_LEN);

                         /* Set status. */
                         R_BYTE_UInt32ToArr(CAP_SUCCESS, capGetConfirm->Status);
                    }
                    else
                    {
                         /* Set status. */
                         R_BYTE_UInt32ToArr(CAP_FAILURE, capGetConfirm->Status);
                    }
                break;

            default:
                break;
        }
    }
    else
    {
    /* Do nothing. */
    }

    /* Prepare remaining CAP command header parts. */
    R_BYTE_UInt16ToArr(0, capCommandHdrOut->CheckSum);                                                                                //Initialize checksum to zero
    R_BYTE_UInt16ToArr(0, capCommandHdrOut->Reserve);                                                                                  //Reserve set to zero
    R_memcpy(capCommandHdrOut->CmdID, capCommandHdrIn->CmdID, sizeof(capCommandHdrOut->CmdID));                                //Command ID identical to request
    R_memcpy(capCommandHdrOut->SequenceNumber, capCommandHdrIn->SequenceNumber, sizeof(capCommandHdrOut->SequenceNumber));      //Sequence number identical to request
    R_memcpy(capCommandHdrOut->Syncword, capCommandHdrIn->Syncword, sizeof(capCommandHdrOut->Syncword));                          //Syncword identical to request
    capCommandHdrOut->AckReq = 0x00;                                                                                              //Not relevant for confirms
    capCommandHdrOut->Type   = 0x03;                                                                                              //Confirm

    /* Add command header and following payload to ioVec. */
    R_IO_VecAppendElement(&ioVecChecksum,
                     (uint8_t*) &capCommandHdrOut->Syncword,
                      (uint16_t)(sizeof(r_cap_command_header_t) + R_BYTE_ArrToUInt16(capCommandHdrOut->length)));

    /* Compute checksum. */
    R_CS_ComputeCheckSum(&ioVecChecksum,
                         &checkSum);

    /* Set checksum. */
    R_BYTE_UInt16ToArr(checkSum, capCommandHdrOut->CheckSum);

    /* Set IPv6 payload length. */
    *ipPayloadLength = (uint16_t)((R_BYTE_ArrToUInt16(capCommandHdrOut->length) + sizeof(r_cap_command_header_t)) + R_ICMP_ECHO_MESSAGE_HEADER_LENGTH);

    return R_RESULT_SUCCESS;
}
#endif
/******************************************************************************
   End of function  handle_remote_getset_req
******************************************************************************/

