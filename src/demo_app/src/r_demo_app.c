/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_app.c
*    @version
*        $Rev: 1782 $
*    @last editor
*        $Author: khues $
*    @date  
*        $Date:: 2016-10-05 17:02:35 +0200#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include <stdlib.h>
#include <string.h>
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_bsp_api.h"
#include "r_byte_swap.h"

#ifdef R_SYNERGY_PLC
#include "tx_api.h"
#include "nx_api.h"
#else
#include "r_timer_api.h"
#endif

/* g3 part */
#include "r_c3sap_api.h"

#include "r_demo_app.h"
#include "r_demo_app_eap.h"
#include "r_demo_nvm_process.h"
#include "r_demo_print.h"

#ifndef R_SYNERGY_PLC
#include "r_ipv6_headers.h"
#include "r_udp_headers.h"
#endif
#include "r_demo_tools.h"
#include "r_demo_api.h"
#include "r_demo_status2text.h"
#include "r_demo_common.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_DEMPO_APP_ENTRIES_PER_CYCLE    (50)
/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
/*!
   \fn static r_result_t menu_route_discovery(void);
   \brief  Menu for route discovery
 */
static r_result_t menu_route_discovery(void);

/*!
   \fn static r_result_t menu_adpd_data(void);
   \brief  Menu for data sending
 */
static r_result_t menu_adpd_data(void);

/*!
   \fn static r_result_t menu_path_discovery(void);
   \brief  Menu for path discovery
 */
static r_result_t menu_path_discovery(void);

/*!
   \fn static r_result_t menu_config(void);
   \brief  Menu for configuration
 */
static r_result_t menu_config(void);

/*!
   \fn r_result_t get_ib_entry(const r_adp_ib_id_t attributeId);
   \brief  Gets element from ADP IB
 */
static r_result_t get_ib_entry(uint8_t chId, const r_adp_ib_id_t attributeId);

/*!
   \fn r_result_t set_ib_entry(const r_adp_ib_id_t attributeId);
   \brief  Sets element in ADP IB
 */
static r_result_t set_ib_entry(uint8_t chId, const r_adp_ib_id_t attributeId);

/*!
   \fn static r_result_t get_mac_pib_entry(const r_g3mac_ib_id_t attributeId);
   \brief  Gets element from MAC PIB
 */
static r_result_t get_mac_pib_entry(uint8_t chId, const r_g3mac_ib_id_t attributeId);

/*!
   \fn static r_result_t set_mac_pib_entry(const r_g3mac_ib_id_t attributeId);
   \brief  Sets element in MAC PIB
 */
static r_result_t set_mac_pib_entry(uint8_t chId, const r_g3mac_ib_id_t attributeId);


static r_result_t menu_bandplan(void);
static r_result_t menu_dflash(void);
static r_result_t menu_statistics(void);

/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t                  g_demo_config;
extern r_demo_entity_t                  g_demo_entity;
extern r_demo_buff_t                    g_demo_buff;

extern volatile r_demo_g3_cb_str_t      g_g3cb[R_G3_CH_MAX];

extern const uint8_t g_rom_nvm_def_psk[16];
extern const uint8_t g_rom_nvm_def_gmk[2][16];

char msg_kayboard_input[256];
char* msg_kayboard_input_temp;

#ifdef R_SYNERGY_PLC
extern NX_IP                            ip_0;               /* NetXDuo IP Control Block */
#endif

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Functions
******************************************************************************/

#ifdef R_SYNERGY_PLC
/******************************************************************************
* Function Name: wait_for_flag
* Description : waits for flag raise for specified time
* Arguments :
* Return Value :
******************************************************************************/
unsigned long wait_for_flag(void* flags_group, unsigned long flag_to_wait, unsigned long timeout)
{
    ULONG tx_status = TX_NOT_DONE;
    ULONG flags     = 0; /* variable storing 32 flags */

    tx_status = tx_event_flags_get((TX_EVENT_FLAGS_GROUP*)flags_group, flag_to_wait, TX_OR, &flags, timeout);

    if (tx_status != TX_SUCCESS)
    {
        if (tx_status == TX_NO_EVENTS)
        {
            if (g_demo_config.verboseEnabled == R_TRUE)
            {
              R_STDIO_Printf("failed. Timeout %dms passed.\n", timeout);
            }

            return R_RESULT_TIMEOUT;
        }
        else
        {
            if (g_demo_config.verboseEnabled == R_TRUE)
            {
              R_STDIO_Printf("failed. Waiting for event error: %d\n", tx_status);
            }

            return R_RESULT_FAILED;
        }
    }

    return R_RESULT_SUCCESS;
}

/******************************************************************************
* Function Name: clear_flag
* Description : clears specified flag
* Arguments :
* Return Value :
******************************************************************************/
unsigned long  clear_flag(void* flags_group, unsigned long flag_to_clear)
{
    return tx_event_flags_set((TX_EVENT_FLAGS_GROUP*)flags_group, (ULONG)(~flag_to_clear), TX_AND); /* invert flag_to_clear and perform AND logic operation with existing flags */
}
#endif

/*===========================================================================*/
/*    G3MAC callback                                                        */
/*===========================================================================*/
/******************************************************************************
* Function Name: R_DEMO_AppHandleMcpsDataInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleMcpsDataInd(const r_g3mac_mcps_data_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {

    uint8_t *pAddr;
    uint32_t sz;
    uint16_t i;

    R_STDIO_Printf("\n -> McpsData Ind (ch[%d])...",1);

    R_STDIO_Printf( "\n    srcAddrMode=%X srcPanId=0x%04X srcAddr=",
        ind->srcAddrMode, ind->srcPanId);
    pAddr = (uint8_t *)ind->srcAddr;
    if(ind->srcAddrMode==R_G3MAC_ADDR_MODE_EXTENDED)
    {
        R_STDIO_Printf( "0x%02X%02X%02X%02X%02X%02X%02X%02X\n",
            pAddr[0], pAddr[1], pAddr[2], pAddr[3], pAddr[4], pAddr[5], pAddr[6], pAddr[7]);
    }
    else
    {
        R_STDIO_Printf( "0x%02X%02X\n", pAddr[0], pAddr[1]);
    }

    R_STDIO_Printf( "    dstAddrMode=%X dstPanId=0x%04X dstAddr=",
        ind->dstAddrMode, ind->dstPanId);
        
    pAddr = (uint8_t *)ind->dstAddr;
    if(ind->dstAddrMode==R_G3MAC_ADDR_MODE_EXTENDED)
    {
        R_STDIO_Printf( "0x%02X%02X%02X%02X%02X%02X%02X%02X\n",
            pAddr[0], pAddr[1], pAddr[2], pAddr[3], pAddr[4], pAddr[5], pAddr[6], pAddr[7]);
    }
    else
    {
        R_STDIO_Printf( "0x%02X%02X\n", pAddr[0], pAddr[1]);
    }

    R_STDIO_Printf( "    msduLength=0x%03X(%dB)\n    msdu=", ind->msduLength, ind->msduLength);
    
    sz = (ind->msduLength < R_DEMO_APP_MSDU_PRINT_MAXLEN) ? ind->msduLength : R_DEMO_APP_MSDU_PRINT_MAXLEN;

    for(i = 0; i < sz; i++)
    {
        R_STDIO_Printf( "%02X", ind->pMsdu[i]);
        
    }
    if(sz < ind->msduLength)
    {
        R_STDIO_Printf( "...");
        
    }
    R_STDIO_Printf( "\n    msduLinkQuality=0x%02X SecurityLevel=%X\n",
        ind->msduLinkQuality, ind->securityLevel);    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleMcpsDataInd
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandleMacTmrRcvInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleMacTmrRcvInd(const r_g3mac_mlme_tmr_receive_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received TMR Receive indication with srcAddr=0x%.2X%.2X ", ind->srcAddr[0],ind->srcAddr[1]);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleMacTmrRcvInd
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandleMacTmrTransmitInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleMacTmrTransmitInd(const r_g3mac_mlme_tmr_transmit_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        uint8_t *pAddr = (uint8_t *)ind->dstAddr;
        R_STDIO_Printf("\n -> Received TMR Transmit indication with dstAddrMode=%d dstAddr=", ind->dstAddrMode);
        if(ind->dstAddrMode==R_G3MAC_ADDR_MODE_EXTENDED)
        {
            R_STDIO_Printf( "0x%02X%02X%02X%02X%02X%02X%02X%02X\n",
                  pAddr[0], pAddr[1], pAddr[2], pAddr[3], pAddr[4], pAddr[5], pAddr[6], pAddr[7]);
        }
        else
        {
            R_STDIO_Printf( "0x%02X%02X\n", pAddr[0], pAddr[1]);
        }
        R_STDIO_Printf(" pms=%d mod=%d", ind->pms,ind->mod);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleMacTmrTransmitInd
******************************************************************************/

/*===========================================================================*/
/*    ADP callback                                                           */
/*===========================================================================*/
/******************************************************************************
* Function Name: R_DEMO_AppHandleBufferInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleBufferInd(const r_adp_adpm_buffer_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received buffer indication with status: 0x%.2X", ind->bufferReady);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleBufferInd
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandleStatusInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleStatusInd(const r_adp_adpm_network_status_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received network status indication with status: %s(0x%.2X)", status_to_text(R_G3_MODE_ADP, ind->status), ind->status);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleStatusInd
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandlePathDiscInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandlePathDiscInd(const r_adp_adpm_path_discovery_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received PREQ indication with originator address: 0x%.4X", ind->origAddr);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandlePathDiscInd
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppHandleFrameCntInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleFrameCntInd(const r_adp_adpm_framecounter_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received FrameCounter indication with counter value: 0x%.8X", ind->frameCounter);
    }
    R_DEMO_AppPreserveProcess(ind->frameCounter);
}
/******************************************************************************
   End of function  R_DEMO_AppHandleFrameCntInd
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_AppHandleRouteInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleRouteInd(const r_adp_adpm_route_update_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received Route Update indication with dstAddr=0x%.2X%.2X", ind->dstAddr[0],ind->dstAddr[1]);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleRouteInd
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_AppHandleLoadInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleLoadInd(const r_adp_adpm_load_seq_num_ind_t* ind)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received Load Sequence Number indication with adpLoadSeqNumber=0x%.2X%.2X", ind->adpLoadSeqNumber[0],ind->adpLoadSeqNumber[1]);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppHandleLoadInd
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_AppHandleRebootReqInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleRebootReqInd(void)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received Reboot Request Indication. Rebooting device.");
    }
       
    /* Call Reboot Routine. */    
    R_DEMO_ModemReboot();

    /**/

}
/******************************************************************************
   End of function  R_DEMO_AppHandleRebootReqInd
******************************************************************************/

/*===========================================================================*/
/*    APP for Statistics                                                     */
/*===========================================================================*/
/******************************************************************************
* Function Name: R_DEMO_AppGetStatistics
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppGetStatistics(uint8_t chId, r_g3_info_layer_t layer)
{
    uint32_t i;
    const char *pTxt;

    r_g3_get_info_req_t req;
    r_g3_get_info_cnf_t *pCnf;

    req.infoType = R_G3_INFO_TYPE_STATS;
    req.infoLayer = layer;

    if (
        (R_DEMO_G3GetInfo(chId, &req, &pCnf) != R_RESULT_SUCCESS)||
        (pCnf->status != R_G3_STATUS_SUCCESS)
        )
    {
        return R_RESULT_FAILED;   
    }

    switch (layer)
    {
    case R_G3_INFO_LAYER_LMAC_DSP:
        R_STDIO_Printf("\n   LML Statistics\n");
        break;
    case R_G3_INFO_LAYER_UMAC:
        R_STDIO_Printf("\n   MAC Statistics\n");
        break;
    case R_G3_INFO_LAYER_ADP:
        R_STDIO_Printf("\n   ADP Statistics\n");
        break;
    case R_G3_INFO_LAYER_EAP:
        R_STDIO_Printf("\n   EAP Statistics\n");
        break;
    default:
        layer = R_G3_INFO_LAYER_END;
        break;
    }

    if(layer != R_G3_INFO_LAYER_END)
    {
        for(i=0;i<(pCnf->length>>2);i++)
        {
            pTxt = statsindex_to_text((uint8_t)layer,i);
            if(pTxt == NULL)
            {
                break;
            }
            R_STDIO_Printf("%41s(%2d):%10lu\n",pTxt,i,pCnf->pInfo[i]);
        }
    }
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppGetStatistics
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppGetLog
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppGetLog(uint8_t chId, r_g3_info_layer_t layer)
{
    uint32_t            i;
    r_g3_get_info_req_t req;
    r_g3_get_info_cnf_t *pCnf;

    req.infoType  = R_G3_INFO_TYPE_LOG;
    req.infoLayer = layer;

    if (
        (uint32_t)(R_DEMO_G3GetInfo(chId, &req, &pCnf) != R_RESULT_SUCCESS) ||
        (uint32_t)(pCnf->status != R_G3_STATUS_SUCCESS)
        )
    {
        return R_RESULT_FAILED;   
    }

    switch (layer)
    {
    case R_G3_INFO_LAYER_LMAC_DSP:
        R_STDIO_Printf("\n   LML Log\n");
        break;
    case R_G3_INFO_LAYER_UMAC:
        R_STDIO_Printf("\n   MAC Log\n");
        break;
    case R_G3_INFO_LAYER_ADP:
        R_STDIO_Printf("\n   ADP Log\n");
        break;
    case R_G3_INFO_LAYER_EAP:
        R_STDIO_Printf("\n   EAP Log\n");
        break;
    default:
        break;
    }

    if(layer == R_G3_INFO_LAYER_LMAC_DSP)
    {
        for(i=0;i<(pCnf->length>>2);i+=2)
        {
            R_STDIO_Printf("      %4d: 0x%08X(%10lu[4MHz])    0x%08X\n",i,pCnf->pInfo[i],pCnf->pInfo[i],pCnf->pInfo[i+1]);
        }
    }
    else
    {
        for(i=0;i<(pCnf->length>>2);i+=2)
        {
            R_STDIO_Printf("      %4d: 0x%08X(%10lu[ms])    0x%08X\n",i,pCnf->pInfo[i],pCnf->pInfo[i],pCnf->pInfo[i+1]);
        }
    }
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppGetLog
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppClearInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppClearInfo(uint8_t chId, uint8_t type)
{
    r_g3_clear_info_req_t req = {0}; //all zero(or all 1) mean all clear
    r_g3_clear_info_cnf_t *pCnf;

    req.infoTypeBit = type;

    if (
        (uint32_t)(R_DEMO_G3ClearInfo(chId, &req, &pCnf) != R_RESULT_SUCCESS) ||
        (uint32_t)(pCnf->status != R_G3_STATUS_SUCCESS)
        )
    {
        return R_RESULT_FAILED;   
    }
    return R_RESULT_SUCCESS;   
}
/******************************************************************************
   End of function  R_DEMO_AppClearInfo
******************************************************************************/


/******************************************************************************
* Function Name: menu_statistics
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_statistics(void)
{
    /* Configuration Menu */
    while (1)
    {
        R_STDIO_Printf("\f-------------------Configuration Menu-----------------------");
        R_STDIO_Printf("\n 0 - Clear Statistics");
        R_STDIO_Printf("\n 1 - Clear Log");
        R_STDIO_Printf("\n 2 - Get LML Statistics");
        R_STDIO_Printf("\n 3 - Get MAC Statistics");
        R_STDIO_Printf("\n 4 - Get ADP Statistics");
        R_STDIO_Printf("\n 5 - Get EAP Statistics");
        R_STDIO_Printf("\n 6 - Get LML Log");
        R_STDIO_Printf("\n 7 - Get MAC Log");
        R_STDIO_Printf("\n 8 - Get ADP Log");
        R_STDIO_Printf("\n 9 - Get EAP Log");
        R_STDIO_Printf("\n z - Return");
        
        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                R_DEMO_AppClearInfo(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_TYPE_BIT_STATS);
                break;
            case '1':
                R_DEMO_AppClearInfo(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_TYPE_BIT_LOG);
                break;
            case '2':
                R_DEMO_AppGetStatistics(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_LMAC_DSP);
                break;
            case '3':
                R_DEMO_AppGetStatistics(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_UMAC);
                break;
            case '4':
                R_DEMO_AppGetStatistics(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_ADP);
                break;
            case '5':
                R_DEMO_AppGetStatistics(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_EAP);
                break;
            case '6':
                R_DEMO_AppGetLog(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_LMAC_DSP);
                break;
            case '7':
                R_DEMO_AppGetLog(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_UMAC);
                break;
            case '8':
                R_DEMO_AppGetLog(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_ADP);
                break;
            case '9':
                R_DEMO_AppGetLog(R_DEMO_G3_USE_PRIMARY_CH, R_G3_INFO_LAYER_EAP);
                break;
            case 'z':
                  return R_RESULT_SUCCESS;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
            break;
            }
        }
    }
}
/******************************************************************************
   End of function  menu_statistics
******************************************************************************/



/*===========================================================================*/
/*    APP                                                                    */
/*===========================================================================*/
/******************************************************************************
* Function Name: R_DEMO_AppMainMenuProcSAP
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_DEMO_AppMainMenuProcSAP(void)
{
    /* Device Main Menu */
    while (1)
    {
        R_STDIO_Printf("\f----------------------ADP Common Service---------------------");
        R_STDIO_Printf("\n 1 - Send data frame");
        R_STDIO_Printf("\n 2 - Start route discovery");
        R_STDIO_Printf("\n 3 - Start path discovery");
        R_STDIO_Printf("\n 4 - Scan for devices (Discovery)");
        R_STDIO_Printf("\n 5 - Configuration (IB)");
        R_STDIO_Printf("\n 6 - Reset ADP");
        R_STDIO_Printf("\n 7 - Statistics/Log");
        R_STDIO_Printf("\n 8 - Toggle verbose mode");
        R_STDIO_Printf("\n 0 - dflash menu");
        R_STDIO_Printf("\n z - Return ");
    
        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                  menu_dflash();
            break;
            case '1':
                menu_adpd_data();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
                
            case '2':
                menu_route_discovery();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '3':
                menu_path_discovery();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '4':
                R_DEMO_AppNetworkDiscovery();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '5':
                menu_config();
            break;
            case '6':

                /* Call ADPM reset. Depending on short address assignment policiy
                   of the coordinator, macFrameCounter should be restored afterwards. */
                R_DEMO_AppResetDevice();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '7':
                menu_statistics();
            break;
            case '8':

                /* Toggle verbose mode */
                g_demo_config.verboseEnabled ^= 1;
            break;

            case 'z':
                return ;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
            break;
            }
        }
        else
        {
            R_STDIO_Printf("\n\n Invalid option! \n");
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppMainMenuProcSAP
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppMainMenuProc
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppMainMenuProc(void)
{
#ifdef R_SYNERGY_PLC
    NXD_ADDRESS     ip_address;
    ssp_err_t       result;
#endif

    uint16_t    panIndex;
    
    r_adp_adpm_network_join_req_t   nwjReq;
    r_adp_adpm_network_join_cnf_t*  nwjCfm;
    r_adp_adpm_discovery_cnf_t*     disCfm;
    r_adp_adpm_network_leave_cnf_t* nwlCfm;
    
        /* Device Main Menu */
    while (1)
    {
        R_STDIO_Printf("\n\f----------------------Peer Main Menu---------------------");
        R_STDIO_Printf("\n---PAN ID: 0x%.4X --- ShortAddr: 0x%.4X --- Verbose:", g_demo_entity.panId, g_demo_entity.shortAddress);
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf(" On ---");
        }
        else
        {
             R_STDIO_Printf(" Off --");
        }
        r_demo_print_bandplan(g_demo_config.bandPlan);
        R_STDIO_Printf("\n 1 - Join a network(with discovery)");
        R_STDIO_Printf("\n 2 - Leave a network");
        R_STDIO_Printf("\n 4 - Toggle Mac promiscuous mode");
        R_STDIO_Printf("\n 7 - ADP common SAP");
        R_STDIO_Printf("\n 8 - Toggle verbose mode");
        R_STDIO_Printf("\n 0 - dflash menu");
        R_STDIO_Printf("\n z - Return");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                  menu_dflash();
            break;
            case '1':

                /* Start discovery and check if a network has been discovered. */
                while (R_DEMO_AppNetworkDiscovery() == 0){/**/}
                
                disCfm = (r_adp_adpm_discovery_cnf_t *)&g_g3cb[R_DEMO_G3_USE_PRIMARY_CH].adpmDiscoveryCnf;


                R_STDIO_Printf("\n %d - Abort", disCfm->PANCount);
                R_STDIO_Printf("\n----------------Select LBA/LBS from List--------------------");

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                
                if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
                {
                    panIndex = (uint16_t)atoi((char const*) g_demo_buff.getStringBuffer);
                    
                    if (panIndex < disCfm->PANCount)
                    {
                        /* Get corresponding PAN ID and LBA address. */
                        nwjReq.panId = disCfm->PANDescriptor[panIndex].panId;
                        R_memcpy(nwjReq.lbaAddress, disCfm->PANDescriptor[panIndex].address,2);

                        if ((R_DEMO_AdpmNetworkJoin(R_DEMO_G3_USE_PRIMARY_CH, &nwjReq, &nwjCfm) == R_RESULT_SUCCESS) &&
                           (nwjCfm->status == R_ADP_STATUS_SUCCESS))
                        {
                            g_demo_entity.panId           = nwjCfm->panId;
                            g_demo_entity.shortAddress    = R_BYTE_ArrToUInt16((uint8_t *)nwjCfm->networkAddress);

#ifdef R_SYNERGY_PLC
                            /* Once device is joined, configure link local IPv6 address of the primary interface
                               based on PAN ID and short address. */
                            ip_address.nxd_ip_version = NX_IP_VERSION_V6;
                            ip_address.nxd_ip_address.v6[0] = 0xFE800000;
                            ip_address.nxd_ip_address.v6[1] = 0;
                            ip_address.nxd_ip_address.v6[2] = (ULONG) (g_demo_entity.panId << 16) + 0xFFu;
                            ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + g_demo_entity.shortAddress;

                            /* Configure the link local address on the primary interface. */
                            result = nxd_ipv6_address_set(&ip_0,
                                                          R_DEMO_G3_USE_PRIMARY_CH,
                                                          &ip_address,
                                                          10,
                                                          &g_demo_entity.ipv6AddressIndex);

                            if (result != NX_SUCCESS)
                            {
                                /* Indicate error. */
                                R_STDIO_Printf("\n IPv6 address setting failed with 0x%.2X.", result);
                            }
#endif
                        }
                        
                        R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    }
                }
            break;
            case '2':
                if ((R_DEMO_AdpmNetworkLeave(R_DEMO_G3_USE_PRIMARY_CH, &nwlCfm) == R_RESULT_SUCCESS) &&
                   (nwlCfm->status == R_ADP_STATUS_SUCCESS))
                {
                    /* Call ADPM reset. Depending on short address assignment policiy
                       of the coordinator, macFrameCounter should be restored afterwards. */
                    R_DEMO_AppResetDevice();
                }
                
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '4':
                g_demo_config.macPromiscuousEnabled ^= 1;
                R_DEMO_AppToggleMacPromiscuous(g_demo_config.macPromiscuousEnabled);
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '7':
                R_DEMO_AppMainMenuProcSAP();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '8':

                /* Toggle verbose mode */
                g_demo_config.verboseEnabled ^= 1;
            break;
            case 'z':
                return ;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
            break;
            }
        }
        else
        {
            R_STDIO_Printf("\n\n Invalid option! \n");
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppMainMenuProc
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppMainMenuProcLbs
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppMainMenuProcLbs(void)
{
#ifdef R_SYNERGY_PLC
    NXD_ADDRESS     ip_address;
#endif

    while (1)
    {

        R_STDIO_Printf("\n\f------------------Coordinator Main Menu-----------------------");
        R_STDIO_Printf("\n-PAN ID: 0x%.4X --- ShortAddr: 0x%.4X --- Verbose:", g_demo_entity.panId, g_demo_entity.shortAddress);
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf(" On --------");
        }
        else
        {
            R_STDIO_Printf(" Off -------");
        }
        r_demo_print_bandplan(g_demo_config.bandPlan);
        R_STDIO_Printf("\n 1 - Start Network");
        R_STDIO_Printf("\n 2 - Show connected devices");
        R_STDIO_Printf("\n 3 - Kick device");
        R_STDIO_Printf("\n 4 - Toggle Mac promiscuous mode");
        R_STDIO_Printf("\n 7 - ADP common SAP");
        R_STDIO_Printf("\n 8 - Toggle verbose mode");
        R_STDIO_Printf("\n 0 - dflash menu");
        R_STDIO_Printf("\n z - Return");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                menu_dflash();
            break;
            case '1':
                R_DEMO_AppNetworkStart(g_demo_config.panId);
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

#ifdef R_SYNERGY_PLC
                /* Once the network has started, configure link local IPv6 address of the primary interface
                   based on PAN ID and short address. */
                ip_address.nxd_ip_version = NX_IP_VERSION_V6;
                ip_address.nxd_ip_address.v6[0] = 0xFE800000;
                ip_address.nxd_ip_address.v6[1] = 0;
                ip_address.nxd_ip_address.v6[2] = (ULONG) (g_demo_config.panId << 16) + 0xFFu;
                ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + g_demo_config.coordShortAddress;

                /* Configure the link local address on the primary interface. */
                if (nxd_ipv6_address_set(&ip_0,
                                         R_DEMO_G3_USE_PRIMARY_CH,
                                         &ip_address,
                                         10,
                                         &g_demo_entity.ipv6AddressIndex) != NX_SUCCESS)
                {
                    /* Indicate error. */
                    R_STDIO_Printf("\n IPv6 address setting failed.");
                }
#endif
            break;
            case '2':

                /* Show device list */
                R_LBS_ShowDeviceList();
                
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '3':
                R_LBS_ShowDeviceList();
                while(1)
                {
                    R_STDIO_Printf("\n -> Select device to kick No (0-F)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    
                    if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
                    {
                        R_LBS_KickDeviceNo(hex_string_to_uint8(g_demo_buff.getStringBuffer));
                        break;
                    }
                }
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '4':
                g_demo_config.macPromiscuousEnabled ^= 1;
                R_DEMO_AppToggleMacPromiscuous(g_demo_config.macPromiscuousEnabled);
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '7':
                R_DEMO_AppMainMenuProcSAP();
                R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '8':

                /* Toggle verbose mode */
                g_demo_config.verboseEnabled ^= 1;
            break;
            case 'z':
                return ;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
            break;
            }
        }
        else
        {
            R_STDIO_Printf("\n\n Invalid option! \n");
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppMainMenuProcLbs
******************************************************************************/
/******************************************************************************
* Function Name: R_DEMO_AppMainMenu
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppMainMenu(void)
{
    /* Local variables */
    uint8_t eui64arr[8];

    while (1)
    {
    r_demo_print_version();
    while (1)
    {
        R_STDIO_Printf("\n-------------------Start menu ------------------------");
        r_demo_print_bandplan(g_demo_config.bandPlan);
        R_STDIO_Printf("\n 1 - Simple CUI");
        R_STDIO_Printf("\n 2 - Auto Start mode (nearly cert mode)");
        R_STDIO_Printf("\n 3 - certification mode");
        R_STDIO_Printf("\n 4 - Toggle Mac promiscuous mode");
        R_STDIO_Printf("\n 7 - Toggle verbose mode");
        R_STDIO_Printf("\n 8 - Change BandPlan");
        R_STDIO_Printf("\n 0 - dflash menu");
        R_STDIO_Printf("\n z - Return");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
        
        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            if (g_demo_buff.getStringBuffer[0] == '1')
            {
                g_demo_config.appMode = R_DEMO_MODE_SIMPLE;
                break;
            }
            else if (g_demo_buff.getStringBuffer[0] == '2')
            {
                g_demo_config.appMode = R_DEMO_MODE_AUTO;
                break;
            }
            else if (g_demo_buff.getStringBuffer[0] == '3')
            {
                g_demo_config.appMode = R_DEMO_MODE_CERT;
                break;
            }
            else if (g_demo_buff.getStringBuffer[0] == '4')
            {
                g_demo_config.macPromiscuousEnabled ^= 1;
                R_DEMO_AppToggleMacPromiscuous(g_demo_config.macPromiscuousEnabled);
                return;
            }
            else if (g_demo_buff.getStringBuffer[0] == '7')
            {
                g_demo_config.verboseEnabled ^= 1;
                return;
            }
            else if (g_demo_buff.getStringBuffer[0] == '8')
            {
                menu_bandplan();
                return;
            }
            else if (g_demo_buff.getStringBuffer[0] == 'z')
            {
                return ;
            }
            else if (g_demo_buff.getStringBuffer[0] == '0')
            {
                menu_dflash();
                return;
            }
            else
            {
                /**/
            }
        }
    }

    /* Set device type */
    while (1)
    {
        R_STDIO_Printf("\n-------------------Set device type------------------------");
        R_STDIO_Printf("\n 1 - Device");
        R_STDIO_Printf("\n 2 - Server");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
        
        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            if (g_demo_buff.getStringBuffer[0] == '1')
            {
                g_demo_config.devType = R_ADP_DEVICE_TYPE_PEER;
                break;
            }
            else if (g_demo_buff.getStringBuffer[0] == '2')
            {
                g_demo_config.devType = R_ADP_DEVICE_TYPE_COORDINATOR;
                break;
            }
            else
            {
                /**/
            }
        }
    }


    if(g_demo_config.appMode != R_DEMO_MODE_SIMPLE)
    {
        R_DEMO_AppCert();
    }
    else
    {
        /* Set EUI64 address. */
        while (1)
        {
            R_STDIO_Printf("\n\n-----Please enter (HEX) or select (0-F) MAC address-------");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
            {
                /* Terminate string and set MAC address */
                g_demo_buff.getStringBuffer[1] = '\0';
                g_demo_config.deviceEUI64 = 0xABCDABCDABCD0000 + hex_string_to_uint8(g_demo_buff.getStringBuffer);
                break;
            }
            else if (strlen((char*) g_demo_buff.getStringBuffer) == 18)
            {
                g_demo_config.deviceEUI64 = hex_string_to_uint64(g_demo_buff.getStringBuffer);
                break;
            }
            else
            {
                R_STDIO_Printf("\n Invalid option! \n");
            }
        }

        /* Set EUI64 address. */
        R_STDIO_Printf(" -> MAC address set to 0x%.8X%.8X \n", (uint32_t) (g_demo_config.deviceEUI64 >> 32), (uint32_t) g_demo_config.deviceEUI64);
        R_BYTE_Uint64ToArr(g_demo_config.deviceEUI64, eui64arr);

        /* Set PSK. */
        R_STDIO_Printf("\n----------------------Setting PSK.------------------------");
        R_memcpy(g_demo_config.pskKey, g_rom_nvm_def_psk, sizeof(g_rom_nvm_def_psk));

        
        if (g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR)
        {
            /* Set GMK. */
            R_STDIO_Printf("\n----------------------Setting GMK.------------------------");
            R_memcpy(g_demo_config.gmk0, g_rom_nvm_def_gmk[0], 16);
            R_memcpy(g_demo_config.gmk1, g_rom_nvm_def_gmk[1], 16);

            /* Init the EAP */
            if (R_DEMO_EapInit(R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
            {
                R_STDIO_Printf("\nOperation failed. -> Press Reset!");
                while(1){/**/};
            }

            /* Set the device type */
            if (R_DEMO_AppResetDevice() != R_RESULT_SUCCESS)
            {
                R_STDIO_Printf("\nOperation failed. -> Press Reset!");
                while(1){/**/};
            }
                                 
            R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            /* Server processing */
            R_DEMO_AppMainMenuProcLbs();
        }

        /* Peer processing */
        else
        {
            /* Init the ADP */
            if (R_DEMO_AdpInit(R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
            {
                R_STDIO_Printf("\nOperation failed. -> Press Reset!");
                while(1){/**/};
            }

            /* Set the device type */
            if (R_DEMO_AppResetDevice() != R_RESULT_SUCCESS)
            {
                R_STDIO_Printf("\nOperation failed. -> Press Reset!");
                while(1){/**/};
            }

            R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            R_DEMO_AppMainMenuProc();
        }
    }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppMainMenu
******************************************************************************/





/******************************************************************************
* Function Name: menu_route_discovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_route_discovery(void)
{
    r_adp_adpm_route_disc_req_t rdisReq;
    r_adp_adpm_route_disc_cnf_t *rdisCfm;

    while(1)
    {
        R_STDIO_Printf("\n -> Enter destination address (HEX)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
        
        if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
        {
           R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer),rdisReq.dstAddress);
           R_STDIO_Printf("0x%.4X",R_BYTE_ArrToUInt16(rdisReq.dstAddress));
           break;
        }
    }
    while(1)
    {
        R_STDIO_Printf("\n -> Enter maximum number of hops...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
        
        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
           g_demo_buff.getStringBuffer[1] = '\0';
           rdisReq.maxHops = (uint8_t)atoi((char const*) g_demo_buff.getStringBuffer);
           R_STDIO_Printf("%d",rdisReq.maxHops);
           break;
        }
    }

    /* Call route discovery function. */
    R_DEMO_AdpmRouteDiscovery(R_DEMO_G3_USE_PRIMARY_CH, &rdisReq, &rdisCfm);

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  menu_route_discovery
******************************************************************************/

/******************************************************************************
* Function Name: menu_adpd_data
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_adpd_data(void)
{
    uint16_t dstAddress;
    uint16_t frameLength;
    uint16_t i;
    uint16_t nrOfFrames;
    
    while(1)
    {
        R_STDIO_Printf("\n -> Enter destination address (HEX)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
        
        if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
        {
           dstAddress = hex_string_to_uint16(g_demo_buff.getStringBuffer);
           R_STDIO_Printf("0x%.4X", dstAddress);
           break;
        }
    }

    while(1)
    {
        R_STDIO_Printf("\n -> Enter number of frames to send...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
        {
           g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
           nrOfFrames = (uint16_t)atoi((char const*) g_demo_buff.getStringBuffer);
           R_STDIO_Printf("%d", nrOfFrames);
           break;
        }
    }
    while(1)
    {
        //test msg
        R_STDIO_Printf("\n -> Enter message to send...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
        {
           g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
           msg_kayboard_input_temp=g_demo_buff.getStringBuffer;

           uint16_t i;
           frameLength = strlen((char*) msg_kayboard_input_temp);
//           msg_kayboard_input[0]='1';
//           msg_kayboard_input[1]='1';
           for (i = 0; i <= frameLength; i++)
           {
               msg_kayboard_input[i]=msg_kayboard_input_temp[i];
           }
//            msg_kayboard_input[frameLength+1]=4;
//			msg_kayboard_input[frameLength+2]=8;
//			msg_kayboard_input[frameLength+3]=10;
//			msg_kayboard_input[frameLength+4]=13;
//
//			frameLength+=3;

           R_STDIO_Printf("Message:%s\n",msg_kayboard_input);
           break;
        }
    }
    while(1)
    {
        //R_STDIO_Printf("\n -> Enter UDP payload (message) length.....");

        break;
    }

    /* Check length */
    if (((frameLength + R_IPV6_HEADER_SIZE) + R_UDP_HEADER_SIZE) > R_DEMO_APP_NSDU_BUFFER_SIZE)
    {
        frameLength = (R_DEMO_APP_NSDU_BUFFER_SIZE - R_IPV6_HEADER_SIZE)  - R_UDP_HEADER_SIZE;
    }
   
    while(1)
    {
        R_STDIO_Printf("\n -> Allow route discovery (1:yes/0:no)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
           g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
           g_demo_config.discoverRoute = (uint8_t)atoi((char const*) g_demo_buff.getStringBuffer);
           R_STDIO_Printf("%d", g_demo_config.discoverRoute);
           break;
        }
    }
    
    while(1)
    {
        R_STDIO_Printf("\n -> Priority (0:normal/1:high)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
           g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
           if (atoi((char const*) g_demo_buff.getStringBuffer) == 0x01u)
           {
               g_demo_config.qualityOfService = R_G3MAC_QOS_HIGH;
           }
           else
           {
               g_demo_config.qualityOfService = R_G3MAC_QOS_NORMAL;
           }
           R_STDIO_Printf("%d", g_demo_config.qualityOfService);
           break;
        }
    }

    /* Send frames. */
    for (i = 0; i < nrOfFrames; i++)
    {
        /* Call UDP frame send function. */
        R_DEMO_GenerateUdpFrame(frameLength,
                                g_demo_entity.panId,
                                g_demo_entity.shortAddress,
                                dstAddress,
                                msg_kayboard_input);
    }

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  menu_adpd_data
******************************************************************************/




/******************************************************************************
* Function Name: menu_path_discovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_path_discovery(void)
{
    r_adp_adpm_path_discovery_req_t     pdisReq;
    r_adp_adpm_path_discovery_cnf_t     *pdisCfm;
    
    while(1)
    {
        R_STDIO_Printf("\n -> Enter destination address (HEX)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
        {
           R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), pdisReq.dstAddress);
           R_STDIO_Printf("0x%.4X",R_BYTE_ArrToUInt16(pdisReq.dstAddress));
           break;
        }
    }

    while(1)
   {
       R_STDIO_Printf("\n -> Enter metric type (HEX)...");

       R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

       if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
       {
          pdisReq.pathMetricType = hex_string_to_uint8(g_demo_buff.getStringBuffer);
          R_STDIO_Printf("0x%.2X",pdisReq.pathMetricType);
          break;
       }
   }

    /* Call path discovery function. */
    R_DEMO_AdpmPathDiscovery(R_DEMO_G3_USE_PRIMARY_CH, &pdisReq, &pdisCfm);
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  menu_path_discovery
******************************************************************************/




/******************************************************************************
* Function Name: menu_config
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_config(void)
{
    /* Configuration Menu */
    while (1)
    {
        R_STDIO_Printf("\f-------------------Configuration Menu-----------------------");
        R_STDIO_Printf("\n 1 - Get MAC PIB entry");
        R_STDIO_Printf("\n 2 - Set MAC PIB entry");
        R_STDIO_Printf("\n 3 - Show all MAC PIB entries");
        R_STDIO_Printf("\n 4 - Get ADP IB entry");
        R_STDIO_Printf("\n 5 - Set ADP IB entry");
        R_STDIO_Printf("\n 6 - Show all ADP IB entries");
        R_STDIO_Printf("\n 7 - Get EAP IB entry");
        R_STDIO_Printf("\n 8 - Set EAP IB entry");
        R_STDIO_Printf("\n 9 - Show all EAP IB entries");
        R_STDIO_Printf("\n z - Return");
        
        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '1':
                while(1)
                {
                    R_STDIO_Printf("\n -> Enter MAC PIB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                    {
                         get_mac_pib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_g3mac_ib_id_t) hex_string_to_uint16(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                }
            break;
            case '2':
                  while(1)
                  {
                    R_STDIO_Printf("\n -> Enter MAC PIB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                    {
                          set_mac_pib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_g3mac_ib_id_t) hex_string_to_uint16(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                  }
            break;
            case '3':
                r_demo_disp_ib_info(R_G3_MODE_MAC);
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '4':
                while(1)
                {
                    R_STDIO_Printf("\n -> Enter ADP IB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                    {
                       get_ib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_adp_ib_id_t) hex_string_to_uint8(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                }
            break;
            case '5':
                while(1)
                {
                    R_STDIO_Printf("\n -> Enter ADP IB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                    {
                       set_ib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_adp_ib_id_t) hex_string_to_uint8(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                }
            break;
            case '6':
                r_demo_disp_ib_info(R_G3_MODE_ADP);
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case '7':
                if(g_demo_config.devType != R_ADP_DEVICE_TYPE_COORDINATOR)
                {
                    R_STDIO_Printf("\n -> Can not accept ! (Eap sap have not be activated now)");
                    break;
                }
                while(1)
                {
                    R_STDIO_Printf("\n -> Enter EAP IB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                    {
                       get_eap_ib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_eap_ib_id_t) hex_string_to_uint8(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                }
            break;
            case '8':
                if(g_demo_config.devType != R_ADP_DEVICE_TYPE_COORDINATOR)
                {
                    R_STDIO_Printf("\n -> Can not accept ! (Eap sap have not be activated now)");
                    break;
                }
                while(1)
                {

                    R_STDIO_Printf("\n -> Enter EAP IB identifier (HEX)...");

                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                    {
                       set_eap_ib_entry (R_DEMO_G3_USE_PRIMARY_CH, (r_eap_ib_id_t) hex_string_to_uint8(g_demo_buff.getStringBuffer));
                    }

                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                    break;
                }
            break;
            case '9':
                r_demo_disp_ib_info(R_G3_MODE_EAP);
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
            break;
            case 'z':
                  return R_RESULT_SUCCESS;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
            break;
            }
        }
    }
}
/******************************************************************************
   End of function  menu_config
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_AppNetworkStart
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppNetworkStart(uint16_t panId)
{
    /* Set PAN ID. */
    r_adp_adpm_network_start_req_t      nwsReq;
    r_adp_adpm_network_start_cnf_t      *nwsCfm;

    nwsReq.panId = panId;
    
    /* Call network start. */
    if ((R_DEMO_AdpmNetworkStart(R_DEMO_G3_USE_PRIMARY_CH, &nwsReq, &nwsCfm) == R_RESULT_SUCCESS) &&
       (nwsCfm->status == R_ADP_STATUS_SUCCESS))
    {
        g_demo_entity.panId          = nwsReq.panId & 0xFCFF;
        g_demo_entity.shortAddress    = 0x0000;
    }
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppNetworkStart
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_SetDeviceType
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_SetDeviceType(void)
{
    uint8_t  txPower;
    uint8_t  txEnablePolarity;
    uint8_t  txWaitTime;
    uint8_t  txDigitalPreambleGain;
    uint8_t  txDigitalGain;
    uint16_t txFilterScale;

    uint8_t tmpString[R_ADP_MAX_IB_SIZE];

    R_STDIO_Printf("\n -> Re-Setting device type.");

    /* Set the device type. */
    if(R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_DEVICE_TYPE,0, (uint8_t*)&g_demo_config.devType) != R_ADP_STATUS_SUCCESS)
    {
        R_STDIO_Printf("\n Setting failed!");
        return R_RESULT_FAILED;
    }
    else
    {
            /* Set GMKs for coordinator after reset. */
            if (g_demo_config.devType == R_ADP_DEVICE_TYPE_PEER)
            {
                R_STDIO_Printf("\n Set to Device.");
            }
            else if (g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR)
            {
                r_eap_eapm_reset_cnf_t *eRstCfm;
                r_eap_eapm_start_cnf_t *eStaCfm;

                if (!((R_DEMO_EapmReset(R_DEMO_G3_USE_PRIMARY_CH, &eRstCfm) == R_RESULT_SUCCESS) &&
                    (eRstCfm->status == R_EAP_STATUS_SUCCESS)))
                {
                    return R_RESULT_FAILED;
                }

                if(g_demo_config.appMode != R_DEMO_MODE_CERT)
                {
                    tmpString[0] = (uint8_t)(R_TRUE);

                    /* Activate ADPM-ROUTE-UPDATE.indication. */
                    if (R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_TMR_IND_ENABLE, 0, (uint8_t *)tmpString) != (r_g3mac_status_t)R_ADP_STATUS_SUCCESS)
                    {
                        return R_RESULT_FAILED;   
                    } 

                    /* Activate ADPM-ROUTE-UPDATE.indication. */
                    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_ROUTE_IND_ENABLE, 0, (uint8_t *)tmpString) != R_ADP_STATUS_SUCCESS)
                    {
                        return R_RESULT_FAILED;   
                    } 
                }
                /* Set keys. */
                /* GMK 0. */
                if (R_DEMO_EapmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_EAP_IB_GMK, 0, g_demo_config.gmk0) != R_EAP_STATUS_SUCCESS)
                {
                    return R_RESULT_FAILED;
                }

                /* GMK 0. */
                if (R_DEMO_EapmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_EAP_IB_GMK, 1, g_demo_config.gmk1) != R_EAP_STATUS_SUCCESS)
                {
                    return R_RESULT_FAILED;
                }

                /* activeKeyIndex. */
                if (R_DEMO_EapmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_EAP_IB_ACTIVEKEYINDEX, 0, &g_demo_config.activeKeyIndex) != R_EAP_STATUS_SUCCESS)
                {
                    return R_RESULT_FAILED;
                }

                if (!((R_DEMO_EapmStart(R_DEMO_G3_USE_PRIMARY_CH, &eStaCfm) == R_RESULT_SUCCESS) &&
                    (eStaCfm->status == R_EAP_STATUS_SUCCESS)))
                {
                    return R_RESULT_FAILED;
                }
                R_STDIO_Printf("\n Set to Server.");
            }
            else if (g_demo_config.modemPlatformType != R_PLATFORM_TYPE_CPX3)
            {
                return R_RESULT_FAILED;
            }
            else
            {
                /**/
            }

            /* Check band plan. */
            switch (g_demo_config.bandPlan)
            {
                case R_G3_BANDPLAN_CENELEC_A:
                    txPower                 = 15;
#ifdef R_SYNERGY_PLC
                    txFilterScale           = 0x10DE;
#else
                    txFilterScale           = 0x140C;
#endif
                    txDigitalPreambleGain   = 27;
                    txDigitalGain           = 24;
                    txEnablePolarity        = 0;
                    txWaitTime              = 0;
                break;
                case R_G3_BANDPLAN_ARIB:
                    txPower                 = 18;
                    txFilterScale           = 0x01C9;
                    txDigitalPreambleGain   = 25;
                    txDigitalGain           = 22;
                    txEnablePolarity        = 0;
                    txWaitTime              = 0;
                break;
                case R_G3_BANDPLAN_FCC:
                    txPower                 = 18;
                    txFilterScale           = 0x0240;
                    txDigitalPreambleGain   = 25;
                    txDigitalGain           = 22;
                    txEnablePolarity        = 0;
                    txWaitTime              = 0;
                break;
                default:
                    return R_RESULT_SUCCESS;
            }

            /* Set tx gain. */
            tmpString[0] = txPower;
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TX_POWER, 0, tmpString);

            /* Set tx filter scale. */
            R_BYTE_UInt16ToArr(txFilterScale, tmpString);
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TX_FILTER_SCALE, 0, tmpString);

            /* Set tx digital preamble gain. */
            tmpString[0] = txDigitalPreambleGain;
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TX_DIGITAL_PREAMBLE_GAIN, 0, tmpString);

            /* Set tx digital gain. */
            tmpString[0] = txDigitalGain;
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TX_DIGITAL_GAIN, 0, tmpString);

            /* Set tx enable polarity. */
            tmpString[0] = txEnablePolarity;
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TXENB_POLARITY, 0, tmpString);

            /* Set tx wait time. */
            tmpString[0] = txWaitTime;
            R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_PHY_TX_WAIT_TIME, 0, tmpString);

            R_STDIO_Printf("\n Gain settings updated.");
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_SetDeviceType
******************************************************************************/



/******************************************************************************
* Function Name: get_ib_entry
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t get_ib_entry(uint8_t chId, const r_adp_ib_id_t attributeId)
{

    r_adp_broadcast_log_table_t*        broadcastLogTablePointer;
    r_adp_routing_table_t*              routingTablePointer;
    r_adp_prefix_table_t*               prefixTablePointer;
    r_adp_contextinfo_table_t*          contextTablePointer;
    r_adp_blacklisted_nei_table_t*      blacklistTablePointer;
    r_adp_destination_address_set_t*    destinationAddressSetPointer;
    uint16_t i;
    uint16_t j;
    uint8_t tmpString[R_ADP_MAX_IB_SIZE];


    switch (attributeId)
    {
        /* One byte elements. */
        case R_ADP_IB_METRIC_TYPE:
        case R_ADP_IB_RLC_ENABLED:
        case R_ADP_IB_ADD_REV_LINK_COST:
        case R_ADP_IB_UNICAST_RREQ_GEN_ENABLE:
        case R_ADP_IB_MAX_HOPS:
        case R_ADP_IB_DEVICE_TYPE:
        case R_ADP_IB_KR:
        case R_ADP_IB_KM:
        case R_ADP_IB_KC:
        case R_ADP_IB_KQ:
        case R_ADP_IB_KH:
        case R_ADP_IB_RREQ_RETRIES:
        case R_ADP_IB_RREQ_RERR_WAIT:
        case R_ADP_IB_WEAK_LQI_VALUE:
        case R_ADP_IB_KRT:
        case R_ADP_IB_SECURITY_LEVEL:
        case R_ADP_IB_ACTIVE_KEY_INDEX:
        case R_ADP_IB_DISABLE_DEFAULT_ROUTING:
        case R_ADP_IB_LOW_LQI_VALUE:
        case R_ADP_IB_HIGH_LQI_VALUE:
        case R_ADP_IB_RREP_WAIT:
        case R_ADP_IB_DATATYPE:
        case R_ADP_IB_ENABLE_DATATRANS:
        case R_ADP_IB_DISABLE_RELAY:
        case R_ADP_IB_ROUTE_IND_ENABLE:
            R_DEMO_AdpmGetWrap(chId, attributeId, 0, tmpString);
            R_STDIO_Printf("0x%.2X", *(uint8_t*) (tmpString));
        break;

        /* Two byte elements. */
        case R_ADP_IB_BROADCAST_LOG_TABLE_ENTRY_TTL:
        case R_ADP_IB_COORD_SHORT_ADDRESS:
        case R_ADP_IB_MAX_JOIN_WAIT_TIME:
        case R_ADP_IB_BLACKLIST_TABLE_ENTRY_TTL:
        case R_ADP_IB_ROUTING_TABLE_ENTRY_TTL:
        case R_ADP_IB_NET_TRAVERSAL_TIME:
        case R_ADP_IB_PATH_DISCOVERY_TIME:
        case R_ADP_IB_LOAD_SEQ_NUMBER:
        case R_ADP_IB_ROUTE_TABLE_SIZE:
        case R_ADP_IB_VALID_RTABLE_ENTRIES:
        case R_ADP_IB_LOAD_SEQ_NUM_IND_INTERVAL:
            R_DEMO_AdpmGetWrap(chId, attributeId, 0, tmpString);
            R_STDIO_Printf("0x%.2X%.2X", tmpString[0],tmpString[1]);
        break;
        case R_ADP_IB_ROUTE_INDEX_BY_ADDR:
            while(1)
            {
                R_STDIO_Printf("\n -> Enter short address (HEX)...");

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                
                if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                {
                    i = hex_string_to_uint16(g_demo_buff.getStringBuffer);
                    R_STDIO_Printf("0x%.4X",i);
                    break;
                }
            }           
            R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);
            R_STDIO_Printf("0x%.2X%.2X", tmpString[0],tmpString[1]);
        break;
        case R_ADP_IB_ROUTE_TABLE_BY_ADDR:
            while(1)
            {
                R_STDIO_Printf("\n -> Enter short address (HEX)...");

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                
                if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                {
                    i = hex_string_to_uint16(g_demo_buff.getStringBuffer);
                    R_STDIO_Printf("0x%.4X",i);
                    break;
                }
            }           
            if (R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString) == R_ADP_STATUS_SUCCESS)
            {                
                routingTablePointer = (r_adp_routing_table_t*)tmpString;

                if (R_BYTE_ArrToUInt16(routingTablePointer->validTime))
                {
                    R_STDIO_Printf("\nDstAddress: 0x%.4X NextHopAddr: 0x%.4X LifeTime(min): 0x%.8X, Route Cost: 0x%.4X, Hop Count: %d",
                            R_BYTE_ArrToUInt16(routingTablePointer->R_dest_Addr),
                            R_BYTE_ArrToUInt16(routingTablePointer->R_next_Addr),
                            R_BYTE_ArrToUInt16(routingTablePointer->validTime),
                            R_BYTE_ArrToUInt16(routingTablePointer->R_metric),
                            routingTablePointer->R_hop_count);
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }   
            }
        break;
        case R_ADP_IB_PREFIX_TABLE:
            R_STDIO_Printf("\n --- Prefix Table --- ");
            for (i = 0; i < R_ADP_PREFIX_TABLE_SIZE; i++)
            {
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);

                prefixTablePointer = (r_adp_prefix_table_t*) tmpString;

                if (prefixTablePointer->prefixLength > 0)
                {
                    R_STDIO_Printf("\nPrefix Length (bits): %d Valid Lifetime (seconds): %d Prefix Value: 0x",
                            prefixTablePointer->prefixLength,
                            prefixTablePointer->validLifetime);

                    for (j = 0; j < ((prefixTablePointer->prefixLength + 7) / 8); j++)
                    {
                       R_STDIO_Printf("%.2X",prefixTablePointer->prefix[j]);
                    }
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
        break;
        case R_ADP_IB_CONTEXT_INFORMATION_TABLE:
            R_STDIO_Printf("\n --- Context Information Table --- ");
            for (i = 0; i < R_ADP_CONTEXT_TABLE_SIZE; i++)
            {
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);

                contextTablePointer = (r_adp_contextinfo_table_t*) tmpString;

                if (contextTablePointer->contextLength > 0)
                {
                    
                    R_STDIO_Printf("\nCompression Flag: 0x%.2X Context Length (bits): %d \n Valid Lifetime (minutes): %d Prefix Value: 0x",
                           contextTablePointer->compressionFlag,
                           contextTablePointer->contextLength,
                           contextTablePointer->validLifetime);

                    for (j = 0; j < ((contextTablePointer->contextLength + 7) / 8); j++)
                    {
                        R_STDIO_Printf("%.2X",contextTablePointer->context[j]);
                    }
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
        break;
        case R_ADP_IB_BROADCAST_LOG_TABLE:
            R_STDIO_Printf("\n --- Broadcast Log Table ---");

            for (i = 0; i < R_ADP_BROADCAST_LOG_TABLE_SIZE; i++)
            {
                /* For larger tables, show in parts. */
                if ((i > 0) &&
                    ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
                
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);

                broadcastLogTablePointer = (r_adp_broadcast_log_table_t*)tmpString;

                if (R_BYTE_ArrToUInt16(broadcastLogTablePointer->validTime) != 0)
                {
                    R_STDIO_Printf("\nTimeStamp(min): 0x%.8X srcAddress: 0x%.4X seqNumber: 0x%.2X",
                        R_BYTE_ArrToUInt16(broadcastLogTablePointer->validTime),
                        R_BYTE_ArrToUInt16(broadcastLogTablePointer->srcAddr),
                        broadcastLogTablePointer->seqNumber);
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
        break;
        case R_ADP_IB_ROUTING_TABLE:
            R_STDIO_Printf("\n --- Routing Table ---");

            for (i = 0; i < R_DEMO_ADP_ROUTING_TABLE_SIZE; i++)
            {
                /* For larger tables, show in parts. */
                if ((i > 0) &&
                ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
                

                /* Get next element */
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);

                routingTablePointer = (r_adp_routing_table_t*) tmpString;

                if (R_BYTE_ArrToUInt16(routingTablePointer->validTime))
                {
                    R_STDIO_Printf("\nDstAddress: 0x%.4X NextHopAddr: 0x%.4X LifeTime(min): 0x%.8X, Route Cost: 0x%.4X, Hop Count: %d",
                            R_BYTE_ArrToUInt16(routingTablePointer->R_dest_Addr),
                            R_BYTE_ArrToUInt16(routingTablePointer->R_next_Addr),
                            R_BYTE_ArrToUInt16(routingTablePointer->validTime),
                            R_BYTE_ArrToUInt16(routingTablePointer->R_metric),
                            routingTablePointer->R_hop_count);
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
        break;
        case R_ADP_IB_GROUP_TABLE:
            R_STDIO_Printf("\n --- Group Table ---");

            for (i = 0; i < R_ADP_GROUP_TABLE_SIZE; i++)
            {
            /* For larger tables, show in parts. */
                if ((i > 0) &&
                  ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
            
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);

                if (R_BYTE_ArrToUInt16(tmpString) != 0xFFFF)
                {
                    R_STDIO_Printf("Address: 0x%.4X ", R_BYTE_ArrToUInt16(tmpString));
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
            break;
        case R_ADP_IB_SOFT_VERSION:
            {
                r_adp_soft_version_t *pAdpVer = (r_adp_soft_version_t *)tmpString;
                R_DEMO_AdpmGetWrap(chId, attributeId, 0, tmpString);
                R_STDIO_Printf("\n --- ADP Version :0x%02X%02X\r\n",pAdpVer->adpVersion[0],pAdpVer->adpVersion[1]);
                R_STDIO_Printf(" --- MAC Version :0x%02X%02X\r\n",pAdpVer->macVersion[0],pAdpVer->macVersion[1]);
                R_STDIO_Printf(" --- LMAC,PHY Version :0x%02X%02X\r\n",pAdpVer->dspVersion[0],pAdpVer->dspVersion[1]);
            }
            break;
        case R_ADP_IB_BLACKLIST_TABLE:
            R_STDIO_Printf("\n --- Blacklist Table ---");

            for (i = 0; i < R_ADP_BLACKLIST_TABLE_SIZE; i++)
            {
                 /* For larger tables, show in parts. */
                if ((i > 0) &&
                ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
                
                R_DEMO_AdpmGetWrap(chId, attributeId, i, tmpString);
                blacklistTablePointer = (r_adp_blacklisted_nei_table_t*) tmpString;

                if (R_BYTE_ArrToUInt16(blacklistTablePointer->validTime) != 0)
                {
                    R_STDIO_Printf("\nAddress: 0x%.4X ", R_BYTE_ArrToUInt16(blacklistTablePointer->B_neighbour_address));
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }
            }
            break;
        case R_ADP_IB_DESTINATION_ADDRESS_SET:
            R_STDIO_Printf("\n --- Destination address  ---");
            
            for (i = 0; i < R_ADP_DESTINATION_ADDRESS_SET_SIZE; i++)
            {
                R_DEMO_AdpmGetWrap(chId, attributeId, 0, tmpString);
                destinationAddressSetPointer = (r_adp_destination_address_set_t*) tmpString;
                R_STDIO_Printf("\nvalidState: %d ",destinationAddressSetPointer->validState);
                R_STDIO_Printf("\nDstAddress: 0x%.4X ",R_BYTE_ArrToUInt16(destinationAddressSetPointer->dstAddr));
            }
            break;

        default:
        
            R_STDIO_Printf("\n -> Unknown ID");
            R_DEMO_AdpmGetWrap(chId, attributeId, 0, tmpString);
            R_STDIO_Printf("0x%.2X%.2X%.2X%.2X", tmpString[0],tmpString[1],tmpString[2],tmpString[3]);
        break;
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  get_ib_entry
******************************************************************************/


/******************************************************************************
* Function Name: set_ib_entry
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t set_ib_entry(uint8_t chId, const r_adp_ib_id_t attributeId)
{
    uint8_t                     inputData8;
    r_adp_contextinfo_table_t*  contextTablePointer;
    r_adp_prefix_table_t*       prefixTablePointer;
    uint8_t                     tempArray[R_ADP_MAX_IB_SIZE];
    uint16_t                    index = 0;
    uint8_t                     i;
    uint32_t                    tmpU32;
    uint16_t                    tmpU16;
    
    switch (attributeId)
    {
        /* One byte elements. */
        case R_ADP_IB_METRIC_TYPE:
        case R_ADP_IB_RLC_ENABLED:
        case R_ADP_IB_ADD_REV_LINK_COST:
        case R_ADP_IB_UNICAST_RREQ_GEN_ENABLE:
        case R_ADP_IB_MAX_HOPS:
        case R_ADP_IB_DEVICE_TYPE:
        case R_ADP_IB_KR:
        case R_ADP_IB_KM:
        case R_ADP_IB_KC:
        case R_ADP_IB_KQ:
        case R_ADP_IB_KH:
        case R_ADP_IB_RREQ_RETRIES:
        case R_ADP_IB_RREQ_RERR_WAIT:
        case R_ADP_IB_WEAK_LQI_VALUE:
        case R_ADP_IB_KRT:
        case R_ADP_IB_SECURITY_LEVEL:
        case R_ADP_IB_ACTIVE_KEY_INDEX:
        case R_ADP_IB_DEFAULT_COORD_ROUTE_ENABLED:
        case R_ADP_IB_DISABLE_DEFAULT_ROUTING:    
        case R_ADP_IB_LOW_LQI_VALUE:
        case R_ADP_IB_HIGH_LQI_VALUE:
        case R_ADP_IB_RREP_WAIT:
        case R_ADP_IB_ROUTE_IND_ENABLE:
        case R_ADP_IB_DATATYPE:
        case R_ADP_IB_ENABLE_DATATRANS:
        case R_ADP_IB_DISABLE_RELAY:
            R_STDIO_Printf("\n -> Enter 8-bit input (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
            {
               R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));
                tempArray[0] = hex_string_to_uint8(g_demo_buff.getStringBuffer);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }
        break;

        /* Two byte elements. */
        case R_ADP_IB_BROADCAST_LOG_TABLE_ENTRY_TTL:
        case R_ADP_IB_COORD_SHORT_ADDRESS:
        case R_ADP_IB_MAX_JOIN_WAIT_TIME:
        case R_ADP_IB_BLACKLIST_TABLE_ENTRY_TTL:
        case R_ADP_IB_ROUTING_TABLE_ENTRY_TTL:
        case R_ADP_IB_PATH_DISCOVERY_TIME:
        case R_ADP_IB_NET_TRAVERSAL_TIME:
        case R_ADP_IB_LOAD_SEQ_NUMBER:
        case R_ADP_IB_LOAD_SEQ_NUM_IND_INTERVAL:

            R_STDIO_Printf("\n -> Enter 16-bit input (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
            {
                R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));
                R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), tempArray);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }
        break;
        case R_ADP_IB_GROUP_TABLE:
            R_STDIO_Printf("\n -> Enter 8-bit group table index (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
            {
                R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

                inputData8 = hex_string_to_uint8(g_demo_buff.getStringBuffer);

                index = inputData8;
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }

            R_STDIO_Printf("\n -> Enter 16-bit group table address (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
            {
                R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));
                R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), tempArray);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }
        break;
        case R_ADP_IB_PREFIX_TABLE:
            R_STDIO_Printf("\n -> Enter 8-bit prefix table index (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
            {
                R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

                inputData8 = hex_string_to_uint8(g_demo_buff.getStringBuffer);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }
            prefixTablePointer = (r_adp_prefix_table_t*) tempArray;

            R_STDIO_Printf("\n -> Enter prefix length in byte...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
            {
               g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
               prefixTablePointer->prefixLength = (uint8_t)(8 * atoi((char const*) g_demo_buff.getStringBuffer)); //Conversion to bit
               R_STDIO_Printf("0x%.2X", prefixTablePointer->prefixLength / 8);
            }

            R_STDIO_Printf("\n -> Enter valid lifetime (seconds)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
            {
               g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
               tmpU32 = (uint32_t)atoi((char const*) g_demo_buff.getStringBuffer);
               R_BYTE_UInt32ToArr(tmpU32,prefixTablePointer->validLifetime);
               R_STDIO_Printf("%d", tmpU32);
            }

            /* Set prefix. */
            for (i = 0; i < (prefixTablePointer->prefixLength / 8); i++)
            {
                R_STDIO_Printf("\n -> Enter prefix byte %d (HEX): ", i);

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                {
                   prefixTablePointer->prefix[i] = hex_string_to_uint8(g_demo_buff.getStringBuffer);
                   R_STDIO_Printf("0x%.2X", prefixTablePointer->prefix[i]);
                }
            }

        break;
        case R_ADP_IB_CONTEXT_INFORMATION_TABLE:
            R_STDIO_Printf("\n -> Enter 8-bit context table index (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
            {
                R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

                inputData8 = hex_string_to_uint8(g_demo_buff.getStringBuffer);

            }
            else
            {
                return R_RESULT_SUCCESS;;
            }

            contextTablePointer = (r_adp_contextinfo_table_t*) tempArray;
            
            R_STDIO_Printf("\n -> Enter context length in byte...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
            {
                g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
                contextTablePointer->contextLength = (uint8_t)(8 * atoi((char const*) g_demo_buff.getStringBuffer)); //Conversion to bit
                R_STDIO_Printf("%d", contextTablePointer->contextLength);
            }
            
            R_STDIO_Printf("\n -> Enter valid lifetime (minutes)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
            {
               g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
               tmpU16 = (uint16_t)atoi((char const*) g_demo_buff.getStringBuffer);
               R_BYTE_UInt16ToArr(tmpU16,contextTablePointer->validLifetime);
               R_STDIO_Printf("%d", tmpU16);
            }

            R_STDIO_Printf("\n -> Enter compression flag value...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) > 0)
            {
               g_demo_buff.getStringBuffer[strlen((char*) g_demo_buff.getStringBuffer)] = '\0';
               contextTablePointer->compressionFlag = (uint8_t)atoi((char const*) g_demo_buff.getStringBuffer);
               R_STDIO_Printf("%d", contextTablePointer->compressionFlag);
            }

            /* Set prefix. */
            for (i = 0; i < (contextTablePointer->contextLength / 8); i++)
            {
                R_STDIO_Printf("\n -> Enter contex byte %d (HEX): ", i);

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                {
                    contextTablePointer->context[i] = hex_string_to_uint8(g_demo_buff.getStringBuffer);
                    R_STDIO_Printf("0x%.2X", contextTablePointer->context[i]);
                }
            }
        break;
        case R_ADP_IB_DESTINATION_ADDRESS_SET:
            R_STDIO_Printf("\n -> Enter 8-bit Destination address set index (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
            {
                R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

                inputData8 = hex_string_to_uint8(g_demo_buff.getStringBuffer);

                index = inputData8;
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }

            R_STDIO_Printf("\n -> Enter 16-bit destination address (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
            {
                R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));
                R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), tempArray);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }            
            break;        
        default:
            R_STDIO_Printf("\n -> Unknown ID");
            R_STDIO_Printf("\n -> Enter 32-bit input (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 10)
            {
                R_STDIO_Printf("0x%.8X", hex_string_to_uint32(g_demo_buff.getStringBuffer));
                R_BYTE_UInt32ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), tempArray);
            }
            else
            {
                return R_RESULT_SUCCESS;;
            }

        break;

    }

    R_DEMO_AdpmSetWrap(chId, attributeId, index, tempArray);

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  set_ib_entry
******************************************************************************/


/******************************************************************************
* Function Name: get_mac_pib_entry
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t get_mac_pib_entry(uint8_t chId, const r_g3mac_ib_id_t attributeId)
{
    r_g3mac_mac_neighbor_table_t*  neighborTableElement;
    r_g3mac_device_table_t*        deviceTableElement;
    uint16_t                       i;
    uint16_t                       j;
    uint16_t                       numElems;
    r_g3mac_mlme_get_req_t         mlmeGetReq;
    r_g3mac_mlme_get_cnf_t*        mlmeGetCfm;
    
    /* Set request structure. */
    mlmeGetReq.pibAttributeId = attributeId;
    mlmeGetReq.pibAttributeIndex = 0;

    switch (attributeId)
    {
        /* One byte elements. */
        case R_G3MAC_IB_HIGHPRIORITY_WINDOWSIZE:
        case R_G3MAC_IB_CSMA_FAIRNESS_LIMIT:
        case R_G3MAC_IB_BEACON_RAND_WIN_LENGTH:
        case R_G3MAC_IB_A:
        case R_G3MAC_IB_K:
        case R_G3MAC_IB_CENELEC_LEGACY_MODE:
        case R_G3MAC_IB_FCC_LEGACY_MODE:
        case R_G3MAC_IB_ACKWAIT_DURATION:
        case R_G3MAC_IB_BSN:
        case R_G3MAC_IB_DSN:
        case R_G3MAC_IB_MAX_BE:
        case R_G3MAC_IB_MAX_CSMABACKOFFS:
        case R_G3MAC_IB_MAX_FRAME_RETRIES:
        case R_G3MAC_IB_MIN_BE:
        case R_G3MAC_IB_SECURITY_ENABLED:
        case R_G3MAC_IB_PROMISCUOUS_MODE:
        case R_G3MAC_IB_TIMESTAMP_SUPPORTED:
        case R_G3MAC_IB_KEY_VALIDATE:
        
        case R_G3MAC_IB_TX_GAIN:
        case R_G3MAC_IB_COHERENT_TRANSMISSION:
        case R_G3MAC_IB_NEIGHBOUR_FILTER_MODE:
        case R_G3MAC_IB_UNICAST_DATA_TX_TIMEOUT:
        case R_G3MAC_IB_BROADCAST_DATA_TX_TIMEOUT:
        case R_G3MAC_IB_BEACON_REQUEST_TX_TIMEOUT:
        
        case R_G3MAC_IB_PHY_TX_POWER:
        case R_G3MAC_IB_PHY_TX_DIGITAL_PREAMBLE_GAIN:
        case R_G3MAC_IB_PHY_TX_DIGITAL_GAIN:
        case R_G3MAC_IB_PHY_TX_WAIT_TIME:
        case R_G3MAC_IB_PHY_TX_BREAK:
        case R_G3MAC_IB_PHY_TX_ACK_GAIN:
        case R_G3MAC_IB_PHY_TXENB_POLARITY:
        case R_G3MAC_IB_PHY_SATT_CTRL_DISABLE:
        
        case R_G3MAC_IB_TRANSMIT_ATTEN:
        case R_G3MAC_IB_BROADCAST_MAX_CW_EANBLE:
        case R_G3MAC_IB_WEAK_CARRIER_SNR:
        case R_G3MAC_IB_THRESH_CARRIER_NUM:
        case R_G3MAC_IB_TMR_IND_ENABLE:
        case R_G3MAC_IB_KEEP_MOD_ENABLE:
        case R_G3MAC_IB_NEI_UPDATE_AFTER_RETRANSMIT:
        case R_G3MAC_IB_COMM_STATUS_IND_MASK:
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
            R_STDIO_Printf("0x%.2X", *(uint8_t*) (mlmeGetCfm->pibAttributeValue));
        break;

        /* Two byte elements. */
        case R_G3MAC_IB_RCCOORD:
        case R_G3MAC_IB_PANID:
        case R_G3MAC_IB_SHORTADDRESS:
        case R_G3MAC_IB_NEIGHBOUR_TABLE_SIZE:
        case R_G3MAC_IB_DEVICE_TABLE_SIZE:
        case R_G3MAC_IB_VALID_NEITABLE_ENTRIES:
        case R_G3MAC_IB_VALID_DEVTABLE_ENTRIES:
        
        case R_G3MAC_IB_PHY_TX_FILTER_SCALE:
        case R_G3MAC_IB_PHY_AC_PHASE_OFFSET:

             R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
             R_STDIO_Printf("0x%.4X", R_BYTE_ArrToUInt16((mlmeGetCfm->pibAttributeValue)));
        break;

        /* Four byte elements. */
        case R_G3MAC_IB_TXDATAPACKET_COUNT:
        case R_G3MAC_IB_RXDATAPACKET_COUNT: 
        case R_G3MAC_IB_TXCMDPACKET_COUNT:
        case R_G3MAC_IB_RXCMDPACKET_COUNT:
        case R_G3MAC_IB_CSMAFAIL_COUNT:
        case R_G3MAC_IB_CSMANOACK_COUNT:
        case R_G3MAC_IB_RXDATABROADCAST_COUNT:
        case R_G3MAC_IB_TXDATABROADCAST_COUNT:
        case R_G3MAC_IB_BADCRC_COUNT:
        case R_G3MAC_IB_TMR_TTL:
        case R_G3MAC_IB_NEIGH_TABLE_ENTRY_TTL:
        case R_G3MAC_IB_FRAME_COUNTER:
        case R_G3MAC_IB_FRAME_CNT_IND_INTERVAL:
             R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
             R_STDIO_Printf("0x%.8X", R_BYTE_ArrToUInt32((mlmeGetCfm->pibAttributeValue)));
        break;

        case R_G3MAC_IB_STATISTICS:
        case R_G3MAC_IB_PHY_STATISTICS:
            R_STDIO_Printf("\n -> Enter 16-bit index (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
            {
                R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));

                mlmeGetReq.pibAttributeIndex = hex_string_to_uint16(g_demo_buff.getStringBuffer);
            }
            else
            {
                 return R_RESULT_FAILED;
            } 
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
            R_STDIO_Printf("0x%.8X", R_BYTE_ArrToUInt32((mlmeGetCfm->pibAttributeValue)));
            break;
        case R_G3MAC_IB_EXTADDRESS:
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
            R_STDIO_Printf("Extended address: 0x%.2X", *(uint8_t*) (mlmeGetCfm->pibAttributeValue));
            for (i = 1; i < 8; i++)
            {
                R_STDIO_Printf("%.2X", *(uint8_t*) (mlmeGetCfm->pibAttributeValue + i));
            }
        break;
        case R_G3MAC_IB_NEIGHBOUR_INDEX_BY_SHORT_ADDR:
        case R_G3MAC_IB_DEVICE_INDEX_BY_SHORT_ADDR:
             R_STDIO_Printf("\n -> Enter 16-bit address (HEX)...");

             R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

             if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
             {
                 R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));
                 mlmeGetReq.pibAttributeIndex = hex_string_to_uint16(g_demo_buff.getStringBuffer);
             }
             else
             {
                  return R_RESULT_FAILED;
             } 
             
             R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
             R_STDIO_Printf("0x%.4X", R_BYTE_ArrToUInt16((mlmeGetCfm->pibAttributeValue)));
        break;
        
        case R_G3MAC_IB_NEIGHBOUR_TABLE_BY_SHORT_ADDR:
            R_STDIO_Printf("\n -> Enter 16-bit address (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
            {
                R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));

                mlmeGetReq.pibAttributeIndex = hex_string_to_uint16(g_demo_buff.getStringBuffer);
            }
            else
            {
                 return R_RESULT_FAILED;
            } 
            
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

            neighborTableElement = (r_g3mac_mac_neighbor_table_t*) (mlmeGetCfm->pibAttributeValue);

            if ((neighborTableElement->sta.bit.status == 1) && (mlmeGetCfm->status == R_G3MAC_STATUS_SUCCESS))
            {
                R_STDIO_Printf("\nTxGain: 0x%.2X \nTxRes: 0x%.2X \nStatus: 0x%.2X \nShortAddress: 0x%.4X, \nToneMap: 0x%.2X%.2X%.2X, \nModScheme: 0x%.2X, \nPhaseDiff: 0x%.2X, \nModulation: 0x%.2X, \nTMRValid: 0x%.8X, \nNeighborValid: 0x%.8X, \nLQI: 0x%.2X",
                    neighborTableElement->sta.bit.txGain,
                    neighborTableElement->sta.bit.txRes,
                    neighborTableElement->sta.bit.status,
                    R_BYTE_ArrToUInt16(neighborTableElement->shortAddress),
                    neighborTableElement->tonemap[0],
                    neighborTableElement->tonemap[1],
                    neighborTableElement->tonemap[2],
                    neighborTableElement->mod.bit.pms,
                    neighborTableElement->mod.bit.phaseDiff,
                    neighborTableElement->mod.bit.modType,
                    R_BYTE_ArrToUInt32(neighborTableElement->tmrValidTime),
                    R_BYTE_ArrToUInt32(neighborTableElement->neighbourValidTime),
                    neighborTableElement->lqi);

                    R_STDIO_Printf("\nTxCoeff: 0x%.2X%.2X%.2X%.2X%.2X%.2X",
                    neighborTableElement->txCoeff[0],
                    neighborTableElement->txCoeff[1],
                    neighborTableElement->txCoeff[2],
                    neighborTableElement->txCoeff[3],
                    neighborTableElement->txCoeff[4],
                    neighborTableElement->txCoeff[5]);
                    
                    R_STDIO_Printf("\nFwdLqi: 0x%.2X",neighborTableElement->fwdLqi);
            }
            else
            {
               R_STDIO_Printf("Entry not set.");
            }
        break;
        
        case R_G3MAC_IB_NEIGHBOUR_TABLE:
            R_STDIO_Printf("\nReading number of Neighbour Table entries.");
            
            /* Set request structure to read number of elements. */
            mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_NEIGHBOUR_TABLE_SIZE;
            mlmeGetReq.pibAttributeIndex = 0;

            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

            numElems = R_BYTE_ArrToUInt16((mlmeGetCfm->pibAttributeValue));
            
            R_STDIO_Printf("0x%.4X", numElems);

            R_STDIO_Printf("\n --- Neighbour Table ---");

            /* Set request structure to read the elements. */
            mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_NEIGHBOUR_TABLE;
            mlmeGetReq.pibAttributeIndex = 0;
            
            for (i = 0; i < numElems; i++)
            {
                /* For larger tables, show in parts. */
                if ((i > 0) &&
                ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
                
                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

                neighborTableElement = (r_g3mac_mac_neighbor_table_t*) (mlmeGetCfm->pibAttributeValue);

                if (neighborTableElement->sta.bit.status == 1)
                {
                    R_STDIO_Printf("\nTxGain: 0x%.2X \nTxRes: 0x%.2X \nStatus: 0x%.2X \nShortAddress: 0x%.4X, \nToneMap: 0x%.2X%.2X%.2X, \nModScheme: 0x%.2X, \nPhaseDiff: 0x%.2X, \nModulation: 0x%.2X, \nTMRValid: 0x%.8X, \nNeighborValid: 0x%.8X, \nLQI: 0x%.2X",
                        neighborTableElement->sta.bit.txGain,
                        neighborTableElement->sta.bit.txRes,
                        neighborTableElement->sta.bit.status,
                        R_BYTE_ArrToUInt16(neighborTableElement->shortAddress),
                        neighborTableElement->tonemap[0],
                        neighborTableElement->tonemap[1],
                        neighborTableElement->tonemap[2],
                        neighborTableElement->mod.bit.pms,
                        neighborTableElement->mod.bit.phaseDiff,
                        neighborTableElement->mod.bit.modType,
                        R_BYTE_ArrToUInt32(neighborTableElement->tmrValidTime),
                        R_BYTE_ArrToUInt32(neighborTableElement->neighbourValidTime),
                        neighborTableElement->lqi);

                        R_STDIO_Printf("\nTxCoeff: 0x%.2X%.2X%.2X%.2X%.2X%.2X",
                        neighborTableElement->txCoeff[0],
                        neighborTableElement->txCoeff[1],
                        neighborTableElement->txCoeff[2],
                        neighborTableElement->txCoeff[3],
                        neighborTableElement->txCoeff[4],
                        neighborTableElement->txCoeff[5]);
                }
                else
                {
                    R_STDIO_Printf("Entry not set.");
                }

                mlmeGetReq.pibAttributeIndex++;
            }
        break;
        case R_G3MAC_IB_TONEMASK:
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
            R_STDIO_Printf("Tone mask: 0x%.2X", mlmeGetCfm->pibAttributeValue[0]);
            for (i = 1; i < 9; i++)
            {
                 R_STDIO_Printf(" ,0x%.2X", mlmeGetCfm->pibAttributeValue[i]);
            }
        break;
        case R_G3MAC_IB_KEY_TABLE:
            R_STDIO_Printf("\nReading number of key table entries.");

            R_STDIO_Printf("\n --- MAC key table ---");
            mlmeGetReq.pibAttributeIndex = 0;

            for (i = 0; i < R_G3MAC_KEY_NUM; i++)
            {
                /* Set request structure to read the elements. */
                mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_KEY_VALIDATE;

                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

                if ((*(uint8_t*) (mlmeGetCfm->pibAttributeValue)) == 0x01)
                {
                     R_STDIO_Printf("Key with index 0x%.2X valid.", mlmeGetCfm->pibAttributeIndex);
                }
                else
                {  
                     R_STDIO_Printf("Key with index 0x%.2X invalid.", mlmeGetCfm->pibAttributeIndex);
                }
                     
                /* Set request structure to read the elements. */
                mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_KEY_TABLE;
                
                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
                
                if (mlmeGetCfm->status == R_ADP_STATUS_SUCCESS)
                {
                    R_STDIO_Printf("Key value: 0x");

                    for (j = 0; j < 16; j++)
                    {
                         R_STDIO_Printf("%.2X", *(uint8_t*) (mlmeGetCfm->pibAttributeValue + j));
                    }
                }
                else
                {
                     R_STDIO_Printf("Key not set.");
                }
                
                mlmeGetReq.pibAttributeIndex++;
             }
        break;
        case R_G3MAC_IB_DEVICE_TABLE:
            R_STDIO_Printf("\nReading number of Neighbour Table entries.");
            
            /* Set request structure to read number of elements. */
            mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_DEVICE_TABLE_SIZE;
            mlmeGetReq.pibAttributeIndex = 0;

            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

            numElems = R_BYTE_ArrToUInt16((mlmeGetCfm->pibAttributeValue));
            
            R_STDIO_Printf("0x%.4X", numElems);

            R_STDIO_Printf("\n --- Device Table ---");

            /* Set request structure to read the elements. */
            mlmeGetReq.pibAttributeId = (uint16_t) R_G3MAC_IB_DEVICE_TABLE;
            mlmeGetReq.pibAttributeIndex = 0;
            
            for (i = 0; i < numElems; i++)
            {
                /* For larger tables, show in parts. */
                if ((i > 0) &&
                ((i % R_DEMPO_APP_ENTRIES_PER_CYCLE) == 0))
                {
                    R_STDIO_Printf("\n-----------------Press Enter to continue---------------------");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                }
                
                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);

                deviceTableElement = (r_g3mac_device_table_t*) (mlmeGetCfm->pibAttributeValue);

                if (R_BYTE_ArrToUInt32(deviceTableElement->updatedTime))
                {
                R_STDIO_Printf("\nSecurity Status: 0x%.2X \nSource Address: 0x%.4X \nFrame Counter: 0x%.8X",                      
                        R_BYTE_ArrToUInt32(deviceTableElement->updatedTime)? R_TRUE:R_FALSE,
                        R_BYTE_ArrToUInt16(deviceTableElement->shortAddress),
                        R_BYTE_ArrToUInt32(deviceTableElement->frameCounter));
                }
                else
                {
                R_STDIO_Printf("Entry not set.");
                }

                mlmeGetReq.pibAttributeIndex++;
            }
        break;
        case R_G3MAC_IB_SOFT_VERSION:
            {
                r_g3mac_soft_version_t *pAdpVer;
                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
                pAdpVer = (r_g3mac_soft_version_t *)mlmeGetCfm->pibAttributeValue;
                R_STDIO_Printf("\n --- MAC Version :0x%02X%02X\r\n",pAdpVer->macVersion[0],pAdpVer->macVersion[1]);
                R_STDIO_Printf(" --- LMAC,PHY Version :0x%02X%02X\r\n",pAdpVer->dspVersion[0],pAdpVer->dspVersion[1]);
            }
            break;
        case R_G3MAC_IB_DEVICE_TABLE_BY_SHORT_ADDR:
            {
                R_STDIO_Printf("\n -> Enter 16-bit address (HEX)...");

                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                {
                    R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));
                    mlmeGetReq.pibAttributeIndex = hex_string_to_uint16(g_demo_buff.getStringBuffer);
                }
                else
                {
                     return R_RESULT_FAILED;
                } 

                R_STDIO_Printf("\nDevice Table short address.");
                R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
                
                if( mlmeGetCfm->status != R_G3MAC_STATUS_SUCCESS )
                {
                    R_STDIO_Printf("\nStatus:0x%02X\n",mlmeGetCfm->status);
                }
                else
                {
                    r_g3mac_device_table_t *pDstDev = (r_g3mac_device_table_t*)mlmeGetCfm->pibAttributeValue;
                    
                    R_STDIO_Printf("\nindex: %d",mlmeGetCfm->pibAttributeIndex);
                    R_STDIO_Printf("\nshortAddress: 0x%04X",R_BYTE_ArrToUInt16(pDstDev->shortAddress));
                    R_STDIO_Printf("\nframeCounter : 0x%08X",R_BYTE_ArrToUInt32(pDstDev->frameCounter));
                    R_STDIO_Printf("\nupdatedTime  : 0x%08X",R_BYTE_ArrToUInt32(pDstDev->updatedTime));
                }
            }
            break;
        default:
            R_STDIO_Printf("\n -> Unknown ID");
            R_DEMO_MlmeGet(chId, &mlmeGetReq, &mlmeGetCfm);
            R_STDIO_Printf("0x%.8X", R_BYTE_ArrToUInt32((mlmeGetCfm->pibAttributeValue)));
        break;

    }

   return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  get_mac_pib_entry
******************************************************************************/


/******************************************************************************
* Function Name: set_mac_pib_entry
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t set_mac_pib_entry(uint8_t chId, const r_g3mac_ib_id_t attributeId)
{
    uint16_t i = 0;
    r_g3mac_mlme_set_req_t              mlmeSetReq;
    r_g3mac_mlme_set_cnf_t              *mlmeSetCfm;
    
    uint8_t              tempArray[R_ADP_MAX_IB_SIZE];

    /* Set request structure. */
    mlmeSetReq.pibAttributeId = attributeId;
    mlmeSetReq.pibAttributeIndex = 0;
    mlmeSetReq.pibAttributeValue = tempArray;

    switch (attributeId)
    {
        /* One byte elements. */
        case R_G3MAC_IB_HIGHPRIORITY_WINDOWSIZE:
        case R_G3MAC_IB_CSMA_FAIRNESS_LIMIT:
        case R_G3MAC_IB_BEACON_RAND_WIN_LENGTH:
        case R_G3MAC_IB_A:
        case R_G3MAC_IB_K:
        case R_G3MAC_IB_CENELEC_LEGACY_MODE:
        case R_G3MAC_IB_FCC_LEGACY_MODE:
        case R_G3MAC_IB_ACKWAIT_DURATION:
        case R_G3MAC_IB_BSN:
        case R_G3MAC_IB_DSN:
        case R_G3MAC_IB_MAX_BE:
        case R_G3MAC_IB_MAX_CSMABACKOFFS:
        case R_G3MAC_IB_MAX_FRAME_RETRIES:
        case R_G3MAC_IB_MIN_BE:
        case R_G3MAC_IB_SECURITY_ENABLED:
        case R_G3MAC_IB_PROMISCUOUS_MODE:
        case R_G3MAC_IB_TIMESTAMP_SUPPORTED:
        
        case R_G3MAC_IB_TX_GAIN:
        case R_G3MAC_IB_COHERENT_TRANSMISSION:
        case R_G3MAC_IB_NEIGHBOUR_FILTER_MODE:
        case R_G3MAC_IB_UNICAST_DATA_TX_TIMEOUT:
        case R_G3MAC_IB_BROADCAST_DATA_TX_TIMEOUT:
        case R_G3MAC_IB_BEACON_REQUEST_TX_TIMEOUT:
        
        case R_G3MAC_IB_PHY_TX_POWER:
        case R_G3MAC_IB_PHY_TX_DIGITAL_PREAMBLE_GAIN:
        case R_G3MAC_IB_PHY_TX_DIGITAL_GAIN:
        case R_G3MAC_IB_PHY_TX_WAIT_TIME:
        case R_G3MAC_IB_PHY_TX_BREAK:
        case R_G3MAC_IB_PHY_TX_ACK_GAIN:
        case R_G3MAC_IB_PHY_TXENB_POLARITY:
        case R_G3MAC_IB_PHY_SATT_CTRL_DISABLE:
        
        case R_G3MAC_IB_TRANSMIT_ATTEN:
        case R_G3MAC_IB_BROADCAST_MAX_CW_EANBLE:
        case R_G3MAC_IB_WEAK_CARRIER_SNR:
        case R_G3MAC_IB_THRESH_CARRIER_NUM:
        case R_G3MAC_IB_TMR_IND_ENABLE:
        case R_G3MAC_IB_KEEP_MOD_ENABLE:
        case R_G3MAC_IB_NEI_UPDATE_AFTER_RETRANSMIT:
        case R_G3MAC_IB_COMM_STATUS_IND_MASK:
            
        R_STDIO_Printf("\n -> Enter 8-bit input (HEX)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
        {
            R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

            mlmeSetReq.pibAttributeValue[0] = hex_string_to_uint8(g_demo_buff.getStringBuffer);
        }
        else
        {
             return R_RESULT_SUCCESS;;
        }
        R_DEMO_MlmeSet(chId, &mlmeSetReq, &mlmeSetCfm);
        break;

        /* Two byte elements. */
        case R_G3MAC_IB_RCCOORD:
        case R_G3MAC_IB_PANID:
        case R_G3MAC_IB_SHORTADDRESS:
        case R_G3MAC_IB_NEIGHBOUR_TABLE_SIZE:
        case R_G3MAC_IB_DEVICE_TABLE_SIZE:
        case R_G3MAC_IB_VALID_NEITABLE_ENTRIES:
        case R_G3MAC_IB_VALID_DEVTABLE_ENTRIES:
        
        case R_G3MAC_IB_PHY_TX_FILTER_SCALE:
        case R_G3MAC_IB_PHY_AC_PHASE_OFFSET:

        R_STDIO_Printf("\n -> Enter 16-bit input (HEX)...");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
        {
            R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));

            R_BYTE_UInt16ToArr(hex_string_to_uint16(g_demo_buff.getStringBuffer), mlmeSetReq.pibAttributeValue);
        }
        else
        {
             return R_RESULT_SUCCESS;;
        }
        R_DEMO_MlmeSet(chId, &mlmeSetReq, &mlmeSetCfm);
        break;

        /* Four byte elements. */
        case R_G3MAC_IB_TXDATAPACKET_COUNT:
        case R_G3MAC_IB_RXDATAPACKET_COUNT:
        case R_G3MAC_IB_TXCMDPACKET_COUNT:
        case R_G3MAC_IB_RXCMDPACKET_COUNT:
        case R_G3MAC_IB_CSMAFAIL_COUNT:
        case R_G3MAC_IB_CSMANOACK_COUNT:
        case R_G3MAC_IB_RXDATABROADCAST_COUNT:
        case R_G3MAC_IB_TXDATABROADCAST_COUNT:
        case R_G3MAC_IB_BADCRC_COUNT:
        case R_G3MAC_IB_TMR_TTL:
        case R_G3MAC_IB_NEIGH_TABLE_ENTRY_TTL:
        case R_G3MAC_IB_FRAME_COUNTER:
        case R_G3MAC_IB_FRAME_CNT_IND_INTERVAL:

        case R_G3MAC_IB_PHY_STATISTICS:
            R_STDIO_Printf("\n -> Enter 32-bit input (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 10)
            {
                 R_STDIO_Printf("0x%.8X", hex_string_to_uint32(g_demo_buff.getStringBuffer));

                 R_BYTE_UInt32ToArr(hex_string_to_uint32(g_demo_buff.getStringBuffer), mlmeSetReq.pibAttributeValue);
            }
            else
            {
                  return R_RESULT_SUCCESS;;
            }
            R_DEMO_MlmeSet(chId, &mlmeSetReq, &mlmeSetCfm);
        break;
        case R_G3MAC_IB_NEIGHBOUR_TABLE_BY_SHORT_ADDR:
            R_STDIO_Printf("-> Writing of this attribute is currently not supported.");
        break;        
        case R_G3MAC_IB_TONEMASK:
            while (i < 9)
            {
                i=0;
                R_STDIO_Printf("\n -> Enter tone mask element %d (HEX, 8bit)...", i);
                
                R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);
                
                if (strlen((char*) g_demo_buff.getStringBuffer) == 4)
                {
                    R_STDIO_Printf("0x%.2X", hex_string_to_uint8(g_demo_buff.getStringBuffer));

                    mlmeSetReq.pibAttributeValue[i] = hex_string_to_uint8(g_demo_buff.getStringBuffer);
                    
                    i++;
                }
            }
            R_DEMO_MlmeSet(chId, &mlmeSetReq, &mlmeSetCfm);
        break;  
        case R_G3MAC_IB_EXTADDRESS:
        case R_G3MAC_IB_NEIGHBOUR_TABLE:
        case R_G3MAC_IB_KEY_TABLE:
        case R_G3MAC_IB_KEY_VALIDATE:
        case R_G3MAC_IB_DEVICE_TABLE:
        case R_G3MAC_IB_DEVICE_TABLE_BY_SHORT_ADDR:
            R_STDIO_Printf("\nWrite access to attribute not supported by demo application.");
        break;
        default:
        
            R_STDIO_Printf("\n -> Unknown ID");
            R_STDIO_Printf("\n -> Enter 32-bit input (HEX)...");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 10)
            {
                 R_STDIO_Printf("0x%.8X", hex_string_to_uint32(g_demo_buff.getStringBuffer));

                 R_BYTE_UInt32ToArr(hex_string_to_uint32(g_demo_buff.getStringBuffer), mlmeSetReq.pibAttributeValue);
            }
            else
            {
                  return R_RESULT_SUCCESS;;
            }
            R_DEMO_MlmeSet(chId, &mlmeSetReq, &mlmeSetCfm);
        break;

    }

    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  set_mac_pib_entry
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppNetworkDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint16_t R_DEMO_AppNetworkDiscovery(void)
{
    r_adp_adpm_discovery_req_t disReq;
    r_adp_adpm_discovery_cnf_t *disCfm;
    
    /* Set scan duration (default randomization time plus one). */
    disReq.duration = 12 + 1; 

    /* Call Discovery function. */
    if (R_DEMO_AdpmDiscovery(R_DEMO_G3_USE_PRIMARY_CH, &disReq, &disCfm) == R_RESULT_SUCCESS)
    {
        return disCfm->PANCount;
    }
    else
    {
        return 0;
    }
}
/******************************************************************************
   End of function  R_DEMO_AppNetworkDiscovery
******************************************************************************/



/******************************************************************************
* Function Name: menu_dflash
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_dflash(void)
{
    uint16_t tmp16;
    uint8_t tempArray[16] = {0};
    r_cap_dev_cfg_t     config;
    r_demo_backup_t     tmpBackup;

    while (1)
    {
        R_STDIO_Printf("\n\f-------------------DFlash Menu-----------------------");
        R_STDIO_Printf("\n 0 - init dev config and reboot RX");
        R_STDIO_Printf("\n 1 - edit dev config and reboot RX");
        R_STDIO_Printf("\n 2 - disp all config");
        R_STDIO_Printf("\n 3 - erase preserved");
        R_STDIO_Printf("\n z - Return");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                {
                    tempArray[0] = 0x0;
                    tempArray[1] = 0x1;
                    r_demo_nvm_config_init();
                    r_demo_nvm_erase(R_DEMO_G3_USE_PRIMARY_CH, NVM_ID_BACKUP);
                    r_demo_nvm_erase(R_DEMO_G3_USE_PRIMARY_CH, NVM_ID_SETTING);

                    R_STDIO_Printf("  ------  Success Initialized Device Config on FlashMemory  ------\r\n");
#ifdef R_SYNERGY_PLC
                    tx_thread_sleep(1000 / (1000 / TX_TIMER_TICKS_PER_SECOND)); /* (1000ms / ticks_in_1000ms) = one_tick_period_in_ms */
#else
                    R_TIMER_BusyWait(1000);
#endif
                    R_BSP_SoftReset();
                }
                break;
            case '1':
                while(1)
                {

                    R_STDIO_Printf("\n -> Enter last 16-bit address of EUI64 (HEX) ex)0xXXXX ...");
                    R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

                    if (strlen((char*) g_demo_buff.getStringBuffer) == 6)
                    {
                        R_STDIO_Printf("0x%.4X", hex_string_to_uint16(g_demo_buff.getStringBuffer));

                        tmp16 = hex_string_to_uint16(g_demo_buff.getStringBuffer);
                        R_BYTE_UInt16ToArr(tmp16, tempArray);

                        r_demo_nvm_config_edit(R_DEMO_G3_USE_PRIMARY_CH, tempArray);
                        r_demo_nvm_erase(R_DEMO_G3_USE_PRIMARY_CH, NVM_ID_BACKUP);
                        r_demo_nvm_erase(R_DEMO_G3_USE_PRIMARY_CH, NVM_ID_SETTING);

                        R_STDIO_Printf("  ------  Success Initialized Device Config on FlashMemory  ------\r\n");
#ifdef R_SYNERGY_PLC
                        tx_thread_sleep(1000 / (1000 / TX_TIMER_TICKS_PER_SECOND)); /* (1000ms / ticks_in_1000ms) = one_tick_period_in_ms */
#else
                        R_TIMER_BusyWait(1000);
#endif
                        R_BSP_SoftReset();
                        break;
                    }
                }
              break;
              case '2':
                r_demo_et_read_dev_config(R_DEMO_G3_USE_PRIMARY_CH, (uint8_t *)&config);
                r_demo_nvm_read(R_DEMO_G3_USE_PRIMARY_CH,NVM_ID_BACKUP,sizeof(r_demo_backup_t),(uint8_t *)&tmpBackup);
                r_demo_print_config(R_DEMO_G3_USE_PRIMARY_CH, &g_demo_config, &config, &tmpBackup);
              break;
              case '3':
                r_demo_nvm_erase(R_DEMO_G3_USE_PRIMARY_CH, NVM_ID_BACKUP);

                R_STDIO_Printf("  ------  Success Erase Preserved info on FlashMemory  ------\r\n");
              break;
              case 'z':
                  return R_RESULT_SUCCESS;
              default:
                R_STDIO_Printf("\n\n Invalid option! \n");
              break;
            }
        }
    }
}
/******************************************************************************
   End of function  menu_dflash
******************************************************************************/



/******************************************************************************
* Function Name: menu_bandplan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t menu_bandplan(void)
{
    while (1)
    {
        R_STDIO_Printf("\n\f-----------------Change BandPlam--------------------");
        R_STDIO_Printf("\n 0 - Cenelec-A");
        R_STDIO_Printf("\n 2 - ARIB");
        R_STDIO_Printf("\n 3 - FCC");
        R_STDIO_Printf("\n z - Return");

        R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

        if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
        {
            switch (g_demo_buff.getStringBuffer[0])
            {
            case '0':
                {
                    g_demo_config.bandPlan = R_G3_BANDPLAN_CENELEC_A;
                }
                return R_RESULT_SUCCESS;
            case '2':
                {
                    g_demo_config.bandPlan = R_G3_BANDPLAN_ARIB;
                }
                return R_RESULT_SUCCESS;
            case '3':
                {
                    g_demo_config.bandPlan = R_G3_BANDPLAN_FCC;
                }
                return R_RESULT_SUCCESS;
            case 'z':
                return R_RESULT_SUCCESS;
            default:
                R_STDIO_Printf("\n\n Invalid option! \n");
                break;
            }
        }
    }
}
/******************************************************************************
   End of function  menu_bandplan
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_AppToggleMacPromiscuous
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppToggleMacPromiscuous(uint8_t on)
{
    /* Local variables */
    uint8_t                  tempArray[R_ADP_MAX_IB_SIZE];
    r_g3mac_mlme_reset_req_t req;
    r_g3mac_mlme_reset_cnf_t *resCfm;


    if(on)
    {
        /* Init the MAC */
        if (R_DEMO_MacInit(R_DEMO_G3_USE_SECONDARY_CH) != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\nOperation failed. -> Press Reset!");
            while(1){/**/};
        }
        req.setDefaultPIB = R_TRUE;
        if (!((R_DEMO_MlmeReset(R_DEMO_G3_USE_SECONDARY_CH, &req, &resCfm) == R_RESULT_SUCCESS) &&
           (resCfm->status == R_ADP_STATUS_SUCCESS)))
        {
            return R_RESULT_FAILED;
        }

        tempArray[0] = R_TRUE;
        if (R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_SECONDARY_CH, R_G3MAC_IB_PROMISCUOUS_MODE, 0, tempArray) != R_G3MAC_STATUS_SUCCESS)
        {
            return R_RESULT_FAILED;   
        }
            R_STDIO_Printf("  ------  Mac Promiscuous Mode On  ------\r\n");

        }
    else
    {
        req.setDefaultPIB = R_TRUE;
        if (!((R_DEMO_MlmeReset(R_DEMO_G3_USE_SECONDARY_CH, &req, &resCfm) == R_RESULT_SUCCESS) &&
           (resCfm->status == R_ADP_STATUS_SUCCESS)))
        {
            return R_RESULT_FAILED;
        }

        if (R_DEMO_DeInit(R_DEMO_G3_USE_SECONDARY_CH) != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\nOperation failed. -> Press Reset!");
            while(1){/**/};
        }
    
        R_STDIO_Printf("  ------  Mac Promiscuous Mode Off  ------\r\n");
    }

    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppToggleMacPromiscuous
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_CheckModeBandPlan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_CheckModeBandPlan(uint8_t bandPlan, r_apl_mode_ch_t *pModeCh)
{
    r_result_t ret;

    switch (bandPlan) {
    case R_G3_BANDPLAN_CENELEC_A:
    case R_G3_BANDPLAN_FCC:
    case R_G3_BANDPLAN_ARIB:
        ret = R_RESULT_SUCCESS;
        break;
    default:
        ret = R_RESULT_FAILED;
        break;
    }

    switch (pModeCh->g3mode) {
    case R_G3_MODE_ADP:
    case R_G3_MODE_EAP:
        ret = (ret)? ret: R_RESULT_SUCCESS;
        break;
    default:
        ret = R_RESULT_FAILED;
        break;
    }

    switch (pModeCh->startMode) {
    case R_DEMO_MODE_SIMPLE:
    case R_DEMO_MODE_AUTO:
    case R_DEMO_MODE_CERT:
        ret = (ret)? ret: R_RESULT_SUCCESS;
        break;
    default:
        ret = R_RESULT_FAILED;
        break;
    }

    switch (pModeCh->routeType) {
    case R_G3_ROUTE_TYPE_NORMAL:
    case R_G3_ROUTE_TYPE_JP_B:
        ret = (ret)? ret: R_RESULT_SUCCESS;
        break;
    default:
        ret = R_RESULT_FAILED;
        break;
    }

    return ret;
}
/******************************************************************************
   End of function  R_DEMO_CheckModeBandPlan
******************************************************************************/


