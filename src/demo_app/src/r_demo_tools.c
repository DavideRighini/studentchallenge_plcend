/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_tools.c
*    @version
*        $Rev: 1590 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-08-03 09:30:23 +0200#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include <stdlib.h>
#include "r_typedefs.h"
#include "r_demo_tools.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/
/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: hex_string_to_uint32
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint32_t hex_string_to_uint32(uint8_t* uartRxBuffer)
{
    /* Terminate string */
    uartRxBuffer[10] = '\0';

    /* Convert to integer */
    return strtoul((char const*)uartRxBuffer, NULL, 16);
}
/******************************************************************************
   End of function  hex_string_to_uint32
******************************************************************************/


/******************************************************************************
* Function Name: hex_string_to_uint16
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint16_t hex_string_to_uint16(uint8_t* uartRxBuffer)
{
    /* Terminate string */
    uartRxBuffer[6] = '\0';

    /* Convert to integer */
    return (uint16_t)strtoul((char const*)uartRxBuffer, NULL, 16);
}
/******************************************************************************
   End of function  hex_string_to_uint16
******************************************************************************/


/******************************************************************************
* Function Name: hex_string_to_uint8
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint8_t hex_string_to_uint8(uint8_t* uartRxBuffer)
{
    /* Terminate string */
    uartRxBuffer[4] = '\0';

    /* Convert to integer */
    return (uint8_t)strtoul((char const*)uartRxBuffer, NULL, 16);
}
/******************************************************************************
   End of function  hex_string_to_uint8
******************************************************************************/


/******************************************************************************
* Function Name: hex_string_to_uint64
* Description :
* Arguments :
* Return Value :
******************************************************************************/
uint64_t hex_string_to_uint64(uint8_t* uartRxBuffer)
{
    uint8_t     firstPart[9];
    uint8_t     secondPart[9];
    uint8_t     i;
    uint32_t    firstPartConv;
    uint32_t    secondPartConv;

    /* Prepare buffer */
    firstPart[8] = '\0';
    secondPart[8] = '\0';

    /* Copy to structure */
    for (i = 0; i < 8; i++)
    {
        firstPart[i] = uartRxBuffer[i + 2];
        secondPart[i] = uartRxBuffer[i + 10];
    }

    /* Convert to integer */
    firstPartConv  = strtoul((char const*) firstPart, NULL, 16);
    secondPartConv = strtoul((char const*) secondPart, NULL, 16);

    return (((uint64_t) firstPartConv) << 32) + (uint64_t) secondPartConv;
}
/******************************************************************************
   End of function  hex_string_to_uint64
******************************************************************************/
