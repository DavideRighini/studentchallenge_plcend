/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_main.c
*    @version
*        $Rev: 2176 $
*    @last editor
*        $Author: a0202438 $
*    @date  
*        $Date:: 2016-09-20 14:31:52 +0900#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_bsp_api.h"

#include "sf_plc_cpx_api.h"
#include "r_fw_download.h"
#include "r_config.h"
#include "r_stdio_api.h"

/* g3 part */
#include "r_c3sap_api.h"
#include "r_byte_swap.h"

#ifdef R_SYNERGY_PLC
#include "tx_api.h"
#include "nx_api.h"
#include "main_thread.h"
#include "r_demo_driver.h"
#else
#include "r_timer_api.h"
#endif

/* app part */
#include "r_demo_app.h"
#include "r_demo_app_thread.h"
#include "r_demo_app_eap.h"

#include "r_demo_nvm_process.h"
#include "r_demo_main.h"
#include "r_demo_common.h"

#ifndef _MSC_VER
#include "cpx3_fw_release.h"
#else
#define R_CPX3FW_BOOT_START_OFFSET_SROM (0x00000000u) /* SROM boot CPX3 firmware start offset in table */
#define R_CPX3FW_BOOT_START_OFFSET_UART (0x00000010u) /* UART boot CPX3 firmware start offset in table */
const uint8_t g_cpxprogtbl[10];
#endif

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/


/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/

r_demo_config_t                 g_demo_config = {
                                            R_PLATFORM_TYPE_CPX3,           //r_modem_platform_type_t
                                            R_BOARD_TYPE_G_CPX3,            //r_modem_board_type_t
                                            R_G3_BANDPLAN_CENELEC_A,        //r_g3_bandplan_t
                                            R_ADP_DEVICE_TYPE_NOT_DEFINED,  //r_adp_device_type_t
                                            R_G3_ROUTE_TYPE_NORMAL,         //r_g3_route_type_t
                                            R_TRUE,                         //verboseEnabled
                                            R_FALSE,                        //macPromiscuousEnabled
                                            R_DEMO_MODE_SIMPLE,             //appMode
                                            R_G3MAC_QOS_NORMAL,             //Normal QoS
                                            R_TRUE,                         //discoverRoute
                                            0,                              //Eui64
                                            0x781D,                         //panId
                                            0,                              //coordSHort
                                            {0,},                           //PSK
                                            {0,},                           //GMK0
                                            {0,},                           //GMK1
                                            0,                              //activeKeyIndex
                                            {0,},                           //wait[4]
                                            {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,},  //tonemask[9]
                                            {0,}                            //extId
                                            };
r_demo_buff_t                   g_demo_buff;
r_demo_entity_t                 g_demo_entity = {0xFFFF, 0xFFFF, 0xFF, 0x00};

volatile r_demo_g3_cb_str_t              g_g3cb[R_G3_CH_MAX];

#ifdef R_SYNERGY_PLC
NX_IP           ip_0;                                                /* NetXDuo IP control block */
NX_PACKET_POOL  pool_0;                                              /* NetXDuo packet pool */
NX_UDP_SOCKET   udp_socket_0xABCD;                                   /* UDP socket control block for port 0xABCD */
NX_UDP_SOCKET   udp_socket_0xF0BF;                                   /* UDP socket control block for port 0xF0BF */
static uint8_t  g_netx_packet_pool[R_NETX_PACKET_POOL_SIZE];         /* Packet pool buffer */
static uint8_t  g_netx_thread_stack_pool[R_NETX_THREAD_STACK_SIZE];  /* Helper thread memory pool */
#endif

/******************************************************************************
Exported global functions
******************************************************************************/
void R_SYS_PingCnf (const r_sys_ping_cnf_t* cnf);
void R_SYS_VersionCnf (const r_sys_version_cnf_t* cnf);
void R_SYS_EventInd (const r_sys_event_ind_t* ind);
void R_SYS_ReBootReqInd (const r_sys_rebootreq_ind_t* ind);

/******************************************************************************
Private global variables and functions
******************************************************************************/

/******************************************************************************
Functions
******************************************************************************/


/******************************************************************************
* Function Name: R_SYS_PingCnf
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_SYS_PingCnf (const r_sys_ping_cnf_t* cnf)
{
    UNUSED(cnf);

    /* discard */
}
/******************************************************************************
   End of function  R_SYS_PingCnf
******************************************************************************/

/******************************************************************************
* Function Name: R_SYS_VersionCnf
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_SYS_VersionCnf (const r_sys_version_cnf_t* cnf)
{
    UNUSED(cnf);

    /* discard */
}
/******************************************************************************
   End of function  R_SYS_VersionCnf
******************************************************************************/

/******************************************************************************
* Function Name: R_SYS_EventInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_SYS_EventInd (const r_sys_event_ind_t* ind)
{
    UNUSED(ind);

    /*discard */
}
/******************************************************************************
   End of function  R_SYS_EventInd
******************************************************************************/

/******************************************************************************
* Function Name: R_SYS_ReBootReqInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_SYS_ReBootReqInd (const r_sys_rebootreq_ind_t* ind)
{
    UNUSED(ind);

    /*discard */
}
/******************************************************************************
   End of function  R_SYS_ReBootReqInd
******************************************************************************/

/******************************************************************************
* Function Name: R_CPX3_Boot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static r_result_t R_CPX3_Boot (const uint8_t *pCpxFw)
{
    UNUSED(pCpxFw);

    ssp_err_t ssp_err_g_sf_plc_cpx0;

    ssp_err_g_sf_plc_cpx0 = g_sf_plc_cpx0.p_api->open (g_sf_plc_cpx0.p_ctrl, g_sf_plc_cpx0.p_cfg);

    if (SSP_SUCCESS != ssp_err_g_sf_plc_cpx0)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_CPX3_Boot
******************************************************************************/


/******************************************************************************
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_ModemBoot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_ModemBoot(void) 
{
    /* This is the code performing the FW download */
    /* Start firmware download. */
    if (R_CPX3_Boot(g_cpxprogtbl) != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        
        return R_RESULT_FAILED;
    }
    else
    {
        /* Indicate that CPX start-up finished successfully. */
        R_BSP_LedOn(R_BSP_LED_5);
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_ModemBoot
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_ModemReboot
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_ModemReboot(void)
{    
    R_memset((uint8_t *)&g_demo_buff, 0, sizeof(r_demo_buff_t));
    R_memset((uint8_t *)&g_demo_entity, 0, sizeof(r_demo_entity_t));

    g_demo_config.macPromiscuousEnabled = R_FALSE;

    /* Boot the modem. */
    R_DEMO_ModemBoot();
}
/******************************************************************************
   End of function  R_DEMO_ModemReboot
******************************************************************************/

/******************************************************************************
* Function Name: R_DEMO_Main
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_Main (void)
{
    r_apl_mode_t apl_mode;
    r_apl_mode_ch_t *pModeCh = &apl_mode.ch[R_DEMO_G3_USE_PRIMARY_CH];

    /* Boot the modem. */
    if (R_DEMO_ModemBoot() != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

#ifdef R_SYNERGY_PLC
    /* Initialize the NetXDuo IPv6 stack. */
    nx_system_initialize();

    /* Create a packet pool of R_NETX_PACKET_POOL_SIZE bytes starting at
       address of g_netx_packet_pool. */
    if (nx_packet_pool_create(&pool_0,
                              "Default Pool",
                              1280,
                              (void *) g_netx_packet_pool,
                              R_NETX_PACKET_POOL_SIZE) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* First create an IP instance with packet pool, source address, and
       driver. The IPv4 address is set to invalid as it is not used. */
    if (nx_ip_create(&ip_0,
                     "NetX IP Instance 0",
                     IP_ADDRESS(0,0,0,0),
                     0xFFFFFFFFUL,
                     &pool_0,
                     R_DEMO_G3_NetworkDriver,
                     g_netx_thread_stack_pool,
                     R_NETX_THREAD_STACK_SIZE,
                     1) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Enable UDP */
    if(nx_udp_enable(&ip_0) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Create an UDP socket. */
    if(nx_udp_socket_create(&ip_0,
                            &udp_socket_0xABCD,
                            "Sample UDP Socket for port 0xABCD",
                            NX_IP_NORMAL,
                            NX_FRAGMENT_OKAY,
                            NX_IP_TIME_TO_LIVE,
                            R_NETX_UDP_QUEUE_MAXIMUM) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Bind certification test UDP port. */
    if(nx_udp_socket_bind(&udp_socket_0xABCD,
                          0xABCD,
                          NX_NO_WAIT) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Create an UDP socket. */
     if(nx_udp_socket_create(&ip_0,
                             &udp_socket_0xF0BF,
                             "Sample UDP Socket for port 0xF0BF",
                             NX_IP_NORMAL,
                             NX_FRAGMENT_OKAY,
                             NX_IP_TIME_TO_LIVE,
                             R_NETX_UDP_QUEUE_MAXIMUM) != NX_SUCCESS)
     {
         /* Indicate error. */
         R_BSP_LedOn(R_BSP_LED_4);
         while(1){/**/};
     }

    /* Bind certification test UDP port. */
    if(nx_udp_socket_bind(&udp_socket_0xF0BF,
                          0xF0BF,
                          NX_NO_WAIT) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Bind certification test UDP port. */
    if(nx_udp_socket_receive_notify(&udp_socket_0xABCD,
                                    R_DEMO_AppHandleUDPReceive) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

    /* Bind certification test UDP port. */
    if(nx_udp_socket_receive_notify(&udp_socket_0xF0BF,
                                    R_DEMO_AppHandleUDPReceive) != NX_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }
#endif

    /* Initialize demo application thread. */
    if (R_DEMO_AppThreadInit() != R_RESULT_SUCCESS)
    {
        /* Indicate error. */
        R_BSP_LedOn(R_BSP_LED_4);
        while(1){/**/};
    }

#if(R_DEFINE_APP_MODE == R_DEMO_APP_MODE_DEMO)
    r_demo_nvm_read(R_NVM_COMMON_CH,R_NVM_ID_BOOT_MODE,sizeof(r_apl_mode_t),(uint8_t *)&apl_mode);
    if(R_DEMO_CheckModeBandPlan(apl_mode.bandPlan, pModeCh) != R_RESULT_SUCCESS)
    {
        R_memset(&apl_mode, 0, sizeof(r_apl_mode_t));
    }
    else
    {
        /**/
        R_memcpy(g_demo_config.tonemask,apl_mode.tonemask, 9);
    }

#else
    if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 0u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 0u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_CENELEC_A;
    }
    else if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 0u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 1u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_ARIB;
    }
    else if(
        (apl_get_port_bit(PORT_ID_BANDPLAN_0) == 1u) &&
        (apl_get_port_bit(PORT_ID_BANDPLAN_1) == 0u))
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_FCC;
    }
    else
    {
        apl_mode.bandPlan = R_G3_BANDPLAN_CENELEC_A;
    }
    if(apl_get_port_bit(PORT_ID_DEVICE_TYPE))
    {
        pModeCh->g3mode = R_G3_MODE_EAP;
    }
    else
    {
        pModeCh->g3mode = R_G3_MODE_ADP;
    }

#if(R_DEFINE_APP_MODE == R_DEMO_APP_MODE_CERT)
    apl_mode.ch[R_DEMO_G3_USE_PRIMARY_CH].startMode = R_DEMO_MODE_CERT;
#else
    if(apl_get_port_bit(PORT_ID_AUTO_MODE))
    {
        if(apl_get_port_bit(PORT_ID_CERT_MODE))
        {
            pModeCh->startMode = R_DEMO_MODE_CERT;
        }
        else
        {
            pModeCh->startMode = R_DEMO_MODE_AUTO;
        }
    }
    else
    {
        pModeCh->startMode = R_DEMO_MODE_SIMPLE;
    }

    if(apl_get_port_bit(PORT_ID_ROUTE_TYPE))
    {
        pModeCh->routeType = R_G3_ROUTE_TYPE_JP_B;
    }
    else
    {
        pModeCh->routeType = R_G3_ROUTE_TYPE_NORMAL;
    }
#endif

#endif

    g_demo_config.bandPlan = (r_g3_bandplan_t)apl_mode.bandPlan;
    g_demo_config.appMode = (r_demo_operation_mode_t)pModeCh->startMode;
    g_demo_config.devType = (pModeCh->g3mode == R_G3_MODE_EAP)? R_ADP_DEVICE_TYPE_COORDINATOR:R_ADP_DEVICE_TYPE_PEER;
    if(g_demo_config.appMode == R_DEMO_MODE_CERT)
    {
        g_demo_config.verboseEnabled = R_FALSE;

        if(g_demo_config.bandPlan == R_G3_BANDPLAN_ARIB)
        {
            g_demo_config.routeType = R_G3_ROUTE_TYPE_JP_B;
        }
        else
        {
            g_demo_config.routeType = (r_g3_route_type_t)pModeCh->routeType;;
        }
    }

    while (1)
    {
        R_memset((uint8_t *)&g_demo_buff, 0, sizeof(r_demo_buff_t));
        R_memset((uint8_t *)&g_demo_entity, 0, sizeof(r_demo_entity_t));

        if(g_demo_config.appMode)
        {
            R_DEMO_AppCert();
        }
        else
        {
            /* Start demo application menu. */
            R_DEMO_AppMainMenu();
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_Main
******************************************************************************/
