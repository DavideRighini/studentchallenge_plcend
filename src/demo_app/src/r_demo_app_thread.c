/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_app_thread.c
*    @version
*        $Rev: 1579 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-07-25 10:00:52 +0200#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#include "r_byte_swap.h"
#ifndef R_SYNERGY_PLC
#include "r_memory_api.h"
#include "r_queue_api.h"
#endif

#include "tx_api.h"
#include "nx_api.h"
#include "r_sample_app_proc_thread.h"

/* g3 part */
#include "r_c3sap_api.h"

/* app part */
#include "r_demo_app.h"
#include "r_demo_app_eap.h"
#include "r_demo_app_thread.h"

#include "r_demo_api.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_DEMO_APP_QUEUE_SIZE           (8)
#define R_DEMO_APP_MSG_SIZE             (1500)

/******************************************************************************
Typedef definitions
******************************************************************************/
#if (defined R_SYNERGY_PLC)
/*!
    \struct r_queue_element_t
    \brief Structure for the queue array elements
 */
typedef struct
{
    uint8_t* pdata;  // The address of the element in the queue
    uint16_t size;   // Size of element
    uint8_t  handle; // The handle of the element
} r_queue_element_t;
#endif

/******************************************************************************
Private global variables and functions
******************************************************************************/
#ifndef R_SYNERGY_PLC
static uint8_t              demoAppMsgsMemory[R_DEMO_APP_MSG_SIZE * R_DEMO_APP_QUEUE_SIZE];     //!< Memory for demo application queue
#endif

/******************************************************************************
Exported global variables
******************************************************************************/
/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
volatile    r_boolean_t     g_app_timeout_detect;
extern volatile r_demo_g3_cb_str_t g_g3cb[R_G3_CH_MAX];
/******************************************************************************
Functions
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppThreadInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppThreadInit(void)
{
    UINT            tx_status;
    uint32_t        i;

    for (i = 0 ; i < R_G3_CH_MAX ; ++i)
    {
        tx_status = tx_event_flags_create ((TX_EVENT_FLAGS_GROUP*)&g_g3cb[i].statusFlags, (CHAR*)"Status flags");

        if (tx_status != TX_SUCCESS)
        {
            return R_RESULT_FAILED;
        }
    }

#ifndef R_SYNERGY_PLC
    /* Initialize a memory pool for the UART rx */
    if (R_MEMORY_Init(R_MEM_INSTANCE_DEMO, demoAppMsgsMemory, R_DEMO_APP_MSG_SIZE * R_DEMO_APP_QUEUE_SIZE) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
#endif
    
    /* Start application processing thread. */ 
    R_BSP_CMTimerOn (R_BSP_CMT_TIMER_ID_1);
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppThreadInit
******************************************************************************/

#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueDataInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueDataInd(const r_adp_adpd_data_ind_t* ind)
{
    uint8_t*            pAddr;
    r_queue_element_t   deque;

    r_adp_adpd_data_ind_t dataIndLocal;

    pAddr = R_MEMORY_Malloc(R_MEM_INSTANCE_DEMO, (uint16_t)(sizeof(r_adp_adpd_data_ind_t) + ind->nsduLength));

    /* Copy data indication structure. */
    dataIndLocal.linkQualityIndicator = ind->linkQualityIndicator;
    dataIndLocal.nsduLength           = ind->nsduLength;
    dataIndLocal.pNsdu                = pAddr + sizeof(r_adp_adpd_data_ind_t);

    /* Obtain the next receive buffer */
    if (pAddr != NULL)
    {
        R_memcpy(pAddr, (uint8_t*) &dataIndLocal, sizeof(r_adp_adpd_data_ind_t));
        R_memcpy(pAddr + sizeof(r_adp_adpd_data_ind_t), ind->pNsdu, ind->nsduLength);

        deque.pdata = pAddr;
        deque.size = (uint16_t)(sizeof(r_adp_adpd_data_ind_t) + ind->nsduLength);
        deque.handle = R_DEMO_APP_HANDLE_DATA_IND;

        (void)tx_queue_send (&g_demo_app_processing_queue, &deque, TX_WAIT_FOREVER);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueDataInd
******************************************************************************/
#endif



/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueMacDataInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueMacDataInd(const r_g3mac_mcps_data_ind_t* ind)
{
    uint8_t*            pAddr;

    r_g3mac_mcps_data_ind_t dataIndLocal;

    r_queue_element_t   deque;

#ifdef R_SYNERGY_PLC
    pAddr = malloc((uint16_t)(sizeof(r_g3mac_mcps_data_ind_t) + ind->msduLength));
#else
    pAddr = R_MEMORY_Malloc(R_MEM_INSTANCE_DEMO, (uint16_t)(sizeof(r_g3mac_mcps_data_ind_t) + ind->msduLength));
#endif

    /* Copy data indication structure. */
    dataIndLocal = *ind;
    dataIndLocal.pMsdu                 = pAddr + sizeof(r_g3mac_mcps_data_ind_t);

    /* Obtain the next receive buffer */
    if (pAddr != NULL)
    {
        R_memcpy(pAddr, (uint8_t*) &dataIndLocal, sizeof(r_g3mac_mcps_data_ind_t));
        R_memcpy(pAddr + sizeof(r_g3mac_mcps_data_ind_t), ind->pMsdu, ind->msduLength);

        deque.pdata = pAddr;
        deque.size = (uint16_t)(sizeof(r_g3mac_mcps_data_ind_t) + ind->msduLength);
        deque.handle = R_DEMO_APP_HANDLE_MAC_DATA_IND;

        (void)tx_queue_send (&g_demo_app_processing_queue, &deque, TX_WAIT_FOREVER);
    }
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueMacDataInd
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppThreadEnqueInd
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThreadEnqueInd(const uint8_t* indPtr,
                                         const uint8_t handle,
                                         const uint16_t size)
{
    uint8_t*            pAddr = NULL;
    r_queue_element_t   deque;

    if ((size > 0) &&
        (indPtr == NULL))
    {
        return;
    }

    if (size > 0)
    {
        pAddr = malloc(size);

        /* Obtain the next receive buffer */
        if (pAddr != NULL)
        {
            R_memcpy(pAddr, indPtr, size);
        }
    }

    /* Set queue structure. */
    deque.pdata  = pAddr;
    deque.size   = size;
    deque.handle = handle;

    /* Put onto the queue. */
    (void) tx_queue_send (&g_demo_app_processing_queue, &deque, TX_WAIT_FOREVER);
}
/******************************************************************************
   End of function  R_DEMO_AppThreadEnqueInd
******************************************************************************/


/******************************************************************************
* Function Name: app_process_thread
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppThread (void)
{ 
    r_queue_element_t deque;
    uint32_t          queueReturn;

    /* Enable interrupts. */
    R_BSP_EnableInterrupt ();

    /* Try to dequeue a new message. */
    queueReturn = tx_queue_receive (&g_demo_app_processing_queue, &deque, TX_WAIT_FOREVER);

    if (TX_SUCCESS == queueReturn)
    {
        switch (deque.handle)
        {
#ifndef R_SYNERGY_PLC
            case R_DEMO_APP_HANDLE_DATA_IND:
                R_DEMO_AppHandleDataIndication((const r_adp_adpd_data_ind_t*) deque.pdata);
                break;
#endif
            case R_DEMO_APP_HANDLE_LEAVE_IND:
                R_DEMO_AppHandleLeaveIndication();
                break;
            case R_DEMO_APP_HANDLE_BUFFER_IND:
                R_DEMO_AppHandleBufferInd((const r_adp_adpm_buffer_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_STATUS_IND:
                R_DEMO_AppHandleStatusInd((const r_adp_adpm_network_status_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_LBP_IND:
                break;
            case R_DEMO_APP_HANDLE_PATH_DIS_IND:
                R_DEMO_AppHandlePathDiscInd((r_adp_adpm_path_discovery_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_REBOOT_REQUEST_IND:
                R_DEMO_AppHandleRebootReqInd();
                break;
            case R_DEMO_APP_HANDLE_FRAMECOUNT_IND:
                R_DEMO_AppHandleFrameCntInd((r_adp_adpm_framecounter_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_EAP_NETWORKJOIN_IND:
                R_DEMO_AppHandleEapNwkJoinInd((r_eap_eapm_network_join_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_EAP_NETWORKLEAVE_IND:
                R_DEMO_AppHandleEapNwkLeaveInd((r_eap_eapm_network_leave_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_EAP_NEWDEVICE_IND:
                R_DEMO_AppHandleEapNewDeviceInd((r_eap_eapm_newdevice_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_MAC_DATA_IND:
                R_DEMO_AppHandleMcpsDataInd((r_g3mac_mcps_data_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_ADP_ROUTE_UPDATE_IND:
                R_DEMO_AppHandleRouteInd((const r_adp_adpm_route_update_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_ADP_LOAD_SEQ_NUM_IND:
                R_DEMO_AppHandleLoadInd((const r_adp_adpm_load_seq_num_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_MAC_TMR_RCV_IND:
                R_DEMO_AppHandleMacTmrRcvInd((const r_g3mac_mlme_tmr_receive_ind_t*) deque.pdata);
                break;
            case R_DEMO_APP_HANDLE_MAC_TMR_TRANSMIT_IND:
                R_DEMO_AppHandleMacTmrTransmitInd((const r_g3mac_mlme_tmr_transmit_ind_t*) deque.pdata);
                break;
            default:
                break;
        }

        /* Free memory. */
#ifdef R_SYNERGY_PLC
        free(deque.pdata);
#else
        R_MEMORY_Free(R_MEM_INSTANCE_DEMO, deque.pdata);
#endif
    }
}
/******************************************************************************
   End of function  app_process_thread
******************************************************************************/

