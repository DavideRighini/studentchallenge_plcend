/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_cert_app.c
*    @version
*        $Rev: 1782 $
*    @last editor
*        $Author: khues $
*    @date  
*        $Date:: 2016-10-05 17:02:35 +0200#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include <string.h>
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_byte_swap.h"
#include "r_config.h"

#ifdef R_SYNERGY_PLC
#include "tx_api.h"
#include "nx_api.h"
#else
#include "r_io_vec.h"
#include "r_timer_api.h"
#endif

/* g3 part */
#include "r_c3sap_api.h"
#include "r_demo_app.h"
#include "r_demo_app_eap.h"
#include "r_demo_nvm_process.h"
#include "r_demo_print.h"
#include "r_demo_tools.h"
#include "r_demo_api.h"
#include "r_demo_metric_computation.h"
#ifdef R_SYNERGY_PLC
#include "r_demo_driver.h"
#else
#include "r_ipv6_headers.h"
#include "r_udp_headers.h"
#include "r_icmp_v6.h"
#include "r_check_sum.h"
#endif
#include "r_demo_common.h"
#include "r_cert_app_version.h"

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_MAX_ROUTE_DISCOVERY_ATTEMPTS      (5u)
#define R_MIN_JOIN_LQI_START                (80u)   /*!< Minimum LQI value for the first discovery */
#define R_MIN_JOIN_LQI_STEP                 (5u)    /*!< Minimum LQI step for following discoveries */
#define R_WAIT_BETWEEN_DISCOVERIES          (3000u) /*!< Period between two route discoveries (milliseconds) */
#define R_SCAN_DURATION                     (15u)   /*!< Network discovery period */
#define R_JOIN_RETRYNUM_TO_SAME_LBA         (2u)    /*!< Retry number for ADPM-NETOWORK-JOIN */
#define R_JOIN_RETRYNUM_PER_DISCOVERY       (3u)    /*!< change lba from pan descriptor */
#define R_WAIT_BETWEEN_JOINREQ              (5000u) /*!< Period between two join requests (milliseconds) */

#define R_FRAMECOUNT_INTERVAL               (0x100)
#define R_WEAK_LQI_TH                       (60u)   /*!< Minimum LQI value for the first discovery */

#define R_INIT_DATA_FLASH

#define R_VALIDATION_OPTIMIZATION_ENABLED

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
static r_demo_backup_t                  sDemoBackup;

/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t                  g_demo_config;
extern r_demo_entity_t                  g_demo_entity;
extern r_demo_buff_t                    g_demo_buff;

extern r_demo_g3_cb_str_t               g_g3cb[R_G3_CH_MAX];
extern const uint8_t                    g_rom_nvm_sycnword[];

#ifdef R_SYNERGY_PLC
extern NX_IP                            ip_0;               /* NetXDuo IP Control Block */
#endif

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
const uint8_t g_group_table_entry_8001[] = {0x01,0x80,0x01};
const uint8_t g_group_table_entry_8567[] = {0x01,0x85,0x67};
const uint8_t g_contextinfo_table_entry_00[] = {0x01,0x50,0xFE,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x78,0x1D,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF};
const uint8_t g_contextinfo_table_entry_01[] = {0x01,0x30,0x11,0x22,0x33,0x44,0x55,0x66,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF};

/******************************************************************************
Functions
******************************************************************************/
#ifndef R_SYNERGY_PLC
/******************************************************************************
* Function Name: R_DEMO_AppHandleDataIndication
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleDataIndication(const r_adp_adpd_data_ind_t* ind)
{
    uint32_t i;
    r_ipv6_hdr_t ipv6Hdr;

    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Received data frame (length: %d, quality: %d)", ind->nsduLength, ind->linkQualityIndicator);

        R_STDIO_Printf("\n------- Received NSDU ------\n");

        for (i = 0; i < ind->nsduLength; i++)
        {
             R_STDIO_Printf("%.2X",ind->pNsdu[i]);
        } 
    }

    if(g_demo_config.appMode)
    {
    if (R_IPV6_UnpackHeader(ind->pNsdu,
                            &ipv6Hdr) == R_RESULT_SUCCESS)
    {
    if (ipv6Hdr.nextHdr == R_IPV6_NEXT_HDR_ICMPV6)
    {
        R_DEMO_ReplyIcmpRequest(ind->pNsdu,
                                &ipv6Hdr);
    }
    else if (ipv6Hdr.nextHdr == R_IPV6_NEXT_HDR_UDP)
    {
        R_DEMO_ReplyUdpFrame(ind->pNsdu,
                             &ipv6Hdr);
    }
    else if (ipv6Hdr.nextHdr == R_IPV6_NEXT_HDR_HOP_BY_HOP)
    {
        R_DEMO_ReplyIcmpRequestExtHeaders(ind->pNsdu,
                                          &ipv6Hdr);
    }   
    else
    {
        /* Do nothing. */
    }
    }
    }
    
}
/******************************************************************************
   End of function  R_DEMO_AppHandleDataIndication
******************************************************************************/
#endif

/******************************************************************************
* Function Name: R_DEMO_AppHandleLeaveIndication
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppHandleLeaveIndication(void)
{
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Device has been kicked from the network. Resetting.");
    }
    
    if(g_demo_config.appMode != R_DEMO_MODE_SIMPLE)
    {
        /* GET BACKUP INFO */
        r_demo_nvm_read(R_DEMO_G3_USE_PRIMARY_CH,NVM_ID_BACKUP,sizeof(r_demo_backup_t),(uint8_t *)&sDemoBackup);
    }

    /* Call ADPM reset. Depending on short address assignment policiy
       of the coordinator, macFrameCounter should be restored afterwards. */
    R_DEMO_AppResetDevice();

    if(g_demo_config.appMode != R_DEMO_MODE_SIMPLE)
    {
        /* Start network join after being kicked */
        R_DEMO_AppJoinNetwork();
    }

}
/******************************************************************************
   End of function  R_DEMO_AppHandleLeaveIndication
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppWaitInsert
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_DEMO_AppWaitInsert(void)
{
    r_demo_nvm_read(R_DEMO_G3_USE_PRIMARY_CH,NVM_ID_RANDWAIT,sizeof(r_demo_randwait_t),(uint8_t *)g_demo_config.wait);
    g_demo_config.wait[0] = (uint8_t)(g_demo_config.wait[0] + 53);

#ifdef R_SYNERGY_PLC
    tx_thread_sleep(g_demo_config.wait[0] / (1000 / TX_TIMER_TICKS_PER_SECOND)); /* (1000ms / ticks_in_1000ms) = one_tick_period_in_ms */
#else
    R_TIMER_BusyWait(g_demo_config.wait[0]);
#endif
    r_demo_nvm_write(R_DEMO_G3_USE_PRIMARY_CH,NVM_ID_RANDWAIT,sizeof(r_demo_randwait_t),(uint8_t *)g_demo_config.wait);
}
/******************************************************************************
   End of function  R_DEMO_AppWaitInsert
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppCert
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppCert(void)
{     
    r_cap_dev_cfg_t config;
    uint8_t         tempArray[R_ADP_MAX_IB_SIZE];
#ifdef R_SYNERGY_PLC
    NXD_ADDRESS     ip_address;
#endif
    
    r_demo_print_cert_version(g_demo_config.modemPlatformType, R_CERT_APP_VERSION);

    /* GET DEV INFO */
#ifdef R_DEFINE_CERT_MODE
    r_demo_et_gen_cert_dev_config(&config);
#else
   if(g_demo_config.appMode == R_DEMO_MODE_CERT)
   {
       r_demo_et_gen_cert_dev_config(&config);
   }
   else
   {
       r_demo_et_read_dev_config(R_DEMO_G3_USE_PRIMARY_CH, (uint8_t *)&config);
   }
#endif
    g_demo_config.deviceEUI64 = R_BYTE_ArrToUInt64(config.extendedAddress);
    R_memcpy(g_demo_config.pskKey, config.psk, 16);
    R_memcpy(g_demo_config.gmk0, config.gmk[0], 16);
    R_memcpy(g_demo_config.gmk1, config.gmk[1], 16);
    R_memcpy(&g_demo_config.extId, config.extID, sizeof(r_g3_config_extid_t));

    g_demo_config.panId = R_BYTE_ArrToUInt16(config.panid);
    g_demo_config.coordShortAddress = R_BYTE_ArrToUInt16(config.coordAddr);
    
    /* GET BACKUP INFO */
    r_demo_nvm_read(R_DEMO_G3_USE_PRIMARY_CH,NVM_ID_BACKUP,sizeof(r_demo_backup_t),(uint8_t *)&sDemoBackup);

    r_demo_print_config(R_DEMO_G3_USE_PRIMARY_CH, &g_demo_config, &config, &sDemoBackup);

    /* Server processing */
    if (g_demo_config.devType == R_ADP_DEVICE_TYPE_COORDINATOR)
    {
        /* Init the ADP */
        if (R_DEMO_EapInit(R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\n EAP init failed. -> Press Reset!");
            while(1){/**/};
        }

        /* Reset device. */
        if (R_DEMO_AppResetDevice() != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\n Device startup failed. -> Press Reset!");
            while(1){/**/};
        }
        
              
        /* Discover already running networks. */
        R_DEMO_AppNetworkDiscovery();

        /* Start network. */
        R_DEMO_AppNetworkStart(g_demo_config.panId);

        R_DEMO_MlmeGetWrap(R_DEMO_G3_USE_PRIMARY_CH,R_G3MAC_IB_FRAME_COUNTER, 0, tempArray);
        if(R_BYTE_ArrToUInt32(tempArray) == 0)
        {
            R_DEMO_AppPreserveProcess(0);
        }

#ifdef R_SYNERGY_PLC
        /* Configure link local IPv6 address of the primary interface
           based on PAN ID and short address. */
        ip_address.nxd_ip_version = NX_IP_VERSION_V6;
        ip_address.nxd_ip_address.v6[0] = 0xFE800000;
        ip_address.nxd_ip_address.v6[1] = 0;
        ip_address.nxd_ip_address.v6[2] = (ULONG) (g_demo_config.panId << 16) + 0xFFu;
        ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + g_demo_config.coordShortAddress;

        /* Configure the link local address on the primary interface. */
        if (nxd_ipv6_address_set(&ip_0,
                                 R_DEMO_G3_USE_PRIMARY_CH,
                                 &ip_address,
                                 10,
                                 &g_demo_entity.ipv6AddressIndex) != NX_SUCCESS)
        {
            /* Indicate error. */
            R_STDIO_Printf("\n IPv6 address setting failed.");
        }

        /* Enable ICMPv6 */
        if(nxd_icmp_enable(&ip_0) != NX_SUCCESS)
        {
            /* Indicate error. */
            R_STDIO_Printf("\n ICMPv6 enabling failed. -> Press Reset!");
            while(1){/**/};
        }
#endif

        while(1)
        {
            R_STDIO_Printf("\n------- G3 Certification Menu (Coordinator)  -------");
            r_demo_print_bandplan(g_demo_config.bandPlan);
#ifndef R_DEFINE_CERT_MODE
            R_STDIO_Printf("\n 0 - main menu");
#endif
            R_STDIO_Printf("\n 1 - Send test frame");
            R_STDIO_Printf("\n 3 - Reboot device");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
            {
                switch (g_demo_buff.getStringBuffer[0])
                {
                case '0':
                    R_DEMO_AppMainMenuProcLbs();
                break;
                case '1':

                    /* Start sending test frames. */
                    R_DEMO_GenerateIcmpRequest(100,
                                               g_demo_entity.panId,
                                               g_demo_entity.shortAddress,
                                               0x0001); //interim
                    
                    R_DEMO_GenerateUdpFrame(100,
                                            g_demo_entity.panId,
                                            g_demo_entity.shortAddress,
                                            0x0001,
                                            0); //interim
                break;
                case '3':
                    R_DEMO_ModemReboot();

                    R_STDIO_Printf("\n Reboot successful");
                    return ;
                default:
                break;
                }
            }
        }
    }

    /* Peer processing */
    else
    {
        /* Init the ADP */
        if (R_DEMO_AdpInit(R_DEMO_G3_USE_PRIMARY_CH) != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\n ADP init failed. -> Press Reset!");
            while(1){/**/};
        }

        /* Reset device. */
        if (R_DEMO_AppResetDevice() != R_RESULT_SUCCESS)
        {
            R_STDIO_Printf("\n Device startup failed. -> Press Reset!");
            while(1){/**/};
        }

        if(g_demo_config.appMode == R_DEMO_MODE_AUTO)
        {
            tempArray[0] = (uint8_t)(R_TRUE);

            /* Set adpDefaultCoordRouteEnabled for CCTT187. */
            if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_DEFAULT_COORD_ROUTE_ENABLED, 0, (uint8_t *)tempArray) != R_ADP_STATUS_SUCCESS)
            {
                return;   
            } 
        }

        R_DEMO_AppWaitInsert();

        /* Join the network. */
        R_DEMO_AppJoinNetwork();


#ifdef R_SYNERGY_PLC
        /* Configure link local IPv6 address of the primary interface
           based on PAN ID and short address. */
        ip_address.nxd_ip_version = NX_IP_VERSION_V6;
        ip_address.nxd_ip_address.v6[0] = 0xFE800000;
        ip_address.nxd_ip_address.v6[1] = 0;
        ip_address.nxd_ip_address.v6[2] = (ULONG) (g_demo_entity.panId << 16) + 0xFFu;
        ip_address.nxd_ip_address.v6[3] = (ULONG) (0xFEu << 24) + g_demo_entity.shortAddress;

        /* Configure the link local address on the primary interface. */
        if (nxd_ipv6_address_set(&ip_0,
                                 R_DEMO_G3_USE_PRIMARY_CH,
                                 &ip_address,
                                 10,
                                 &g_demo_entity.ipv6AddressIndex) != NX_SUCCESS)
        {
            /* Indicate error. */
            R_STDIO_Printf("\n IPv6 address setting failed.");
        }

        /* Enable ICMPv6 */
        if(nxd_icmp_enable(&ip_0) != NX_SUCCESS)
        {
            /* Indicate error. */
            R_STDIO_Printf("\n ICMPv6 enabling failed. -> Press Reset!");
            while(1){/**/};
        }
#endif

        while(1)
        {
            R_STDIO_Printf("\n------- G3 Certification Menu (Peer)  -------");
            r_demo_print_bandplan(g_demo_config.bandPlan);
#ifndef R_DEFINE_CERT_MODE
            R_STDIO_Printf("\n 0 - main menu");
#endif
            R_STDIO_Printf("\n 1 - Send test frame");
            R_STDIO_Printf("\n 3 - Reboot device");

            R_STDIO_Gets((char*) g_demo_buff.getStringBuffer);

            if (strlen((char*) g_demo_buff.getStringBuffer) == 1)
            {
                switch (g_demo_buff.getStringBuffer[0])
                {
                case '0':
                    R_DEMO_AppMainMenuProc();
                break;
                case '1':

                    /* Start sending test frames. */
                    R_DEMO_GenerateIcmpRequest(100,
                                               g_demo_entity.panId,
                                               g_demo_entity.shortAddress,
                                               g_demo_config.coordShortAddress);
                    
                    R_DEMO_GenerateUdpFrame(100,
                                            g_demo_entity.panId,
                                            g_demo_entity.shortAddress,
                                            g_demo_config.coordShortAddress,
                                            0);
                break;
                case '3':
                    R_DEMO_ModemReboot();
                    R_STDIO_Printf("\n Reboot successful");
                    return ;
                default:
                break;
                }
            }
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppCert
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_PkupPan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_DEMO_PkupPan(const r_loadng_ib_info_t* ibInfo, r_loadng_link_info_t* linkInfo, uint8_t thresh_lqi, r_adp_adpm_discovery_cnf_t *in_cnf, r_demo_paninfo_t *ont_pan)
{
    uint32_t    write_flg = 0;

    uint8_t pan_cnt = 0;
    uint16_t i;
    uint16_t j;
    uint16_t k;
    uint8_t linkCost;

    r_adp_pan_descriptor_t *in_pan;
    r_adp_pan_descriptor_t *cur_pan;

    
    R_memset((uint8_t *)ont_pan,0, sizeof(r_demo_paninfo_t));

    for(i=0; i<in_cnf->PANCount ;i++)
    {
        in_pan = &in_cnf->PANDescriptor[i];

        linkInfo->lqi = in_pan->linkQuality;
        linkCost = R_DEMO_ComputeDirectionalLinkCost(ibInfo, linkInfo);
        in_pan->rcCoord = (((uint32_t)in_pan->rcCoord+linkCost)>=0xFFFFu)?0xFFFFu:(uint16_t)(in_pan->rcCoord+linkCost);

        if(in_pan->linkQuality > thresh_lqi)
        {
            write_flg = 0;
            if(pan_cnt == 0)
            {
                ont_pan->pan[0] = *in_pan;
                pan_cnt++;
                continue;
            }
            for(j=0; j<pan_cnt ;j++)
            {
                cur_pan = &ont_pan->pan[j];
                if(in_pan->rcCoord == cur_pan->rcCoord){
                    if(in_pan->linkQuality > cur_pan->linkQuality){
                        k=(pan_cnt == R_DEMO_APP_PAN_MAXNUM)? (uint16_t)(pan_cnt-1u):(uint16_t)pan_cnt;
                        for( ; k>(j) ; k--){
                            ont_pan->pan[k]=ont_pan->pan[k-1];
                        }
                        *cur_pan = *in_pan;
                        pan_cnt = (pan_cnt < R_DEMO_APP_PAN_MAXNUM)? (uint8_t)(pan_cnt+1u):(uint8_t)pan_cnt;
                        write_flg = 1;
                        break;
                    }
                }else if(in_pan->rcCoord < cur_pan->rcCoord){
                    k=(pan_cnt == R_DEMO_APP_PAN_MAXNUM)? (uint16_t)(pan_cnt-1u):(uint16_t)pan_cnt;
                    for( ; k>(j) ; k--){
                        ont_pan->pan[k]=ont_pan->pan[k-1];
                    }
                    *cur_pan = *in_pan;
                    pan_cnt = (pan_cnt < R_DEMO_APP_PAN_MAXNUM)? (uint8_t)(pan_cnt+1u):(uint8_t)pan_cnt;
                    write_flg = 1;
                    break;
                }
                else
                {
                    /**/
                }
            }
            if((write_flg == 0)&&
                (pan_cnt < R_DEMO_APP_PAN_MAXNUM))
            {
                ont_pan->pan[pan_cnt] = *in_pan;
                pan_cnt++;
                continue;
            }
        }

    }
    ont_pan->panCount = pan_cnt;

    return ;
}
/******************************************************************************
   End of function  R_DEMO_PkupPan
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_PanShuffle
* Description :
* Arguments :
* Return Value :
******************************************************************************/
static void R_DEMO_PanShuffle(r_demo_paninfo_t *pPan)
{
    r_adp_pan_descriptor_t *pSrc = pPan->pan;
    r_adp_pan_descriptor_t dst;

    uint32_t tmp;
    uint16_t i,j,size;

    if(pPan->panCount > 1)
    {
        size = pPan->panCount;
        for(i=0;i<size;i++)
        {
            if(pSrc[i].rcCoord >= 0x7FFF)
            {
                size = (uint16_t)((i>0)? i-1:0);
                break;
            }
        }
        if(size <= 2)
        {
            /* targets of shuffle are only LBA which have route to coordinator */
            return;
        }

        tmp = (uint32_t)(g_demo_config.deviceEUI64 >> 32);
        tmp = tmp ^ (uint32_t)g_demo_config.deviceEUI64;
        srand(tmp);
        for(i=0;i<size;i++)
        {
            j = (uint16_t) rand()%size;
            dst = pSrc[i];
            pSrc[i] = pSrc[j];
            pSrc[j] = dst;
        }
    }

    return;
}
/******************************************************************************
   End of function  R_DEMO_PanShuffle
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppJoinNetwork
* Description :
* Arguments :
* Return Value :
******************************************************************************/
void R_DEMO_AppJoinNetwork(void)
{
    uint8_t joinCnt;
    uint8_t beaconIndex;
    uint8_t minJoinLqi = R_MIN_JOIN_LQI_START;
    r_boolean_t joinFinished = R_FALSE;
 
    r_adp_adpm_network_join_req_t nwjReq;
    r_adp_adpm_network_join_cnf_t* nwjCfm;
    r_adp_adpm_discovery_cnf_t  *disCfm;

    r_demo_paninfo_t *pSortPan;

    r_loadng_ib_info_t loadIb; 
    r_loadng_link_info_t loadLink; 

    uint8_t         tempArray[R_ADP_MAX_IB_SIZE];

    loadIb.bandPlan = g_demo_config.bandPlan;

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_METRIC_TYPE, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpMetricType = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_LOW_LQI_VALUE, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpLowLQIValue = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_HIGH_LQI_VALUE, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpHighLQIValue = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KR, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKr = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KM, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKr = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KC, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKc = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KQ, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKq = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KH, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKh = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_KRT, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return;
    }
    loadIb.adpKrt = tempArray[0];

    loadLink.modType = R_G3MAC_MOD_TYPE_ROBO;
    loadLink.modScheme = R_G3MAC_MOD_SCHEME_DIFFERENTIAL;
    if(loadIb.bandPlan == R_G3_BANDPLAN_ARIB)
    {
        loadLink.numTones = R_MAX_NUMBER_OF_TONES_ARIB;
    }
    else if(loadIb.bandPlan == R_G3_BANDPLAN_ARIB)
    {
        loadLink.numTones = R_MAX_NUMBER_OF_TONES_FCC;
    }
    else
    {
        loadLink.numTones = R_MAX_NUMBER_OF_TONES_CENELEC_A;
    }

    /* Loop until join process finished successfully. */ 
    while (joinFinished == R_FALSE)
    {
    /* Start discovery and check if a network has been discovered. */
    while (R_DEMO_AppNetworkDiscovery() == 0){/**/};

    disCfm = &g_g3cb[R_DEMO_G3_USE_PRIMARY_CH].adpmDiscoveryCnf;
    pSortPan = &g_demo_buff.panInfo;

    R_DEMO_PkupPan(&loadIb, &loadLink, minJoinLqi, disCfm, pSortPan);

    if(
        (pSortPan->panCount)&&
        (R_BYTE_ArrToUInt16(pSortPan->pan[0].address) != g_demo_config.coordShortAddress)
        )
    {
        /* shuffle LBA to avoid heavy biased relay node to coordinator */
        R_DEMO_PanShuffle(pSortPan);
    }

    joinCnt = 0x0u;
    beaconIndex   = 0x0u;

    /* Check if a suitable beacon has been found. Check that the beacon has a minimum LQI
         to avoid nodes in lower stages to join the coordinator immediately. */
    if (pSortPan->panCount)
    {
        while(beaconIndex < pSortPan->panCount)
        {
            /* Set LBA address and PAN ID for join request. */
            nwjReq.panId = pSortPan->pan[beaconIndex].panId;
            R_memcpy(nwjReq.lbaAddress, pSortPan->pan[beaconIndex].address,2);

            if ((R_DEMO_AdpmNetworkJoin(R_DEMO_G3_USE_PRIMARY_CH, &nwjReq, &nwjCfm) == R_RESULT_SUCCESS) &&
                 (nwjCfm->status == R_ADP_STATUS_SUCCESS))
            {
                g_demo_entity.panId = nwjCfm->panId;
                g_demo_entity.shortAddress = R_BYTE_ArrToUInt16((uint8_t *)nwjCfm->networkAddress);
                joinFinished = R_TRUE;
                break;
            }
            else
            {
                if((joinCnt++) >= R_JOIN_RETRYNUM_TO_SAME_LBA)
                {
                    joinCnt = 0;
                    beaconIndex++;
                }

                /* Wait some time to allow other devices to join the network before flooding
                 the network with beacon requests and beacons. */
#ifdef R_SYNERGY_PLC
                tx_thread_sleep(R_WAIT_BETWEEN_JOINREQ / (1000 / TX_TIMER_TICKS_PER_SECOND)); /* (1000ms / ticks_in_1000ms) = one_tick_period_in_ms */
#else
                R_TIMER_BusyWait(R_WAIT_BETWEEN_JOINREQ);
#endif

                if(beaconIndex > R_JOIN_RETRYNUM_PER_DISCOVERY)
                {
                    break;
                }
            }
        }
    }
    else
    {

        /* Decrement minJoinLqi and start new discovery until it matches weakLqiValue. */
        if (minJoinLqi > R_WEAK_LQI_TH)
        {
            minJoinLqi = (uint8_t)(minJoinLqi - R_MIN_JOIN_LQI_STEP);
        }
        else{
            minJoinLqi = R_MIN_JOIN_LQI_START;
        }

        /* Wait some time to allow other devices to join the network before flooding
         the network with beacon requests and beacons. */

#ifdef R_SYNERGY_PLC
        tx_thread_sleep(R_WAIT_BETWEEN_DISCOVERIES / (1000 / TX_TIMER_TICKS_PER_SECOND)); /* (1000ms / ticks_in_1000ms) = one_tick_period_in_ms */
#else
        R_TIMER_BusyWait(R_WAIT_BETWEEN_DISCOVERIES);
#endif
    }

    } /* while (joinFinished == R_FALSE) */

    /* post process after joinning for DEMO and Certification mode */
    {
        R_DEMO_MlmeGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_FRAME_COUNTER, 0, tempArray);

        if(R_BYTE_ArrToUInt32(tempArray) == 0)
        {
            R_DEMO_AppPreserveProcess(0);
        }

        if(g_demo_config.appMode == R_DEMO_MODE_CERT)
        {
            R_memcpy(tempArray, g_contextinfo_table_entry_00, sizeof(g_contextinfo_table_entry_00));
            tempArray[10] = (uint8_t)(g_demo_entity.panId >> 8);
            tempArray[11] = (uint8_t)(g_demo_entity.panId & 0xFF);

            /* Set context table entry 0. */
            if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_CONTEXT_INFORMATION_TABLE, 0, (uint8_t *)tempArray) != R_ADP_STATUS_SUCCESS)
            {
                return;   
            } 
        }
        

        if(g_demo_config.appMode == R_DEMO_MODE_AUTO)
        {
            r_adp_adpm_route_disc_req_t rdisReq;
            r_adp_adpm_route_disc_cnf_t *rdisCfm;

            R_BYTE_UInt16ToArr(g_demo_config.coordShortAddress,rdisReq.dstAddress);
            rdisReq.maxHops = 8u;
            /* Call route discovery function. */
            R_DEMO_AdpmRouteDiscovery(R_DEMO_G3_USE_PRIMARY_CH, &rdisReq, &rdisCfm);
        }
    }
}
/******************************************************************************
   End of function  R_DEMO_AppJoinNetwork
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppPreserveProcess
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppPreserveProcess(uint32_t framecounter)
{
    r_demo_backup_payload_t tmpBkup = {0};
    uint8_t                 tempArray[R_ADP_MAX_IB_SIZE];
    
    R_BYTE_UInt32ToArr(framecounter, tmpBkup.FrameCounter);
    tmpBkup.bandPlan = g_demo_config.bandPlan;
    tmpBkup.RouteType = g_demo_config.routeType;
    tmpBkup.DeviceType = g_demo_config.devType;

    R_BYTE_UInt32ToArr(g_demo_entity.panId, tmpBkup.PanId);
    R_BYTE_UInt32ToArr(g_demo_entity.shortAddress, tmpBkup.NetworkAddr);

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_ACTIVE_KEY_INDEX, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    tmpBkup.ActiveKeyIndex = tempArray[0];

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_LOAD_SEQ_NUMBER, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    R_memcpy(tmpBkup.LoadSeqNo, tempArray, 2);

    r_demo_nvm_backup_write(R_DEMO_G3_USE_PRIMARY_CH, &tmpBkup);
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        r_demo_print_frame_conter(R_DEMO_G3_USE_PRIMARY_CH, framecounter);
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppPreserveProcess
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AppPresetProcess
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppPresetProcess(void)
{
    uint8_t                 tempArray[R_ADP_MAX_IB_SIZE];
    uint32_t tmp32;
    uint16_t tmp16;

    if(
        ((R_memcmp(g_rom_nvm_sycnword, sDemoBackup.SyncWord, 4))||
        (sDemoBackup.bkup.RouteType != g_demo_config.routeType))||
        (sDemoBackup.bkup.DeviceType != g_demo_config.devType)
        )
    {
        return R_RESULT_FAILED;
    }
    tmp32 = (R_BYTE_ArrToUInt32(sDemoBackup.bkup.FrameCounter) + R_FRAMECOUNT_INTERVAL) - 1;
    R_BYTE_UInt32ToArr(tmp32, tempArray);
    R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_FRAME_COUNTER, 0, tempArray);

    tmp16 = (uint16_t)((R_BYTE_ArrToUInt16(sDemoBackup.bkup.LoadSeqNo) + R_FRAMECOUNT_INTERVAL) - 1);
    R_BYTE_UInt16ToArr(tmp16, tempArray);
    R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_LOAD_SEQ_NUMBER, 0, tempArray);

    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n#### [ Load FrameCounter = 0x%08X ] ####\r\n", tmp32);
        R_STDIO_Printf("\n#### [ Load LoadngSeqNo = 0x%04X ] ####\r\n", tmp16);
    }
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppPresetProcess
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AppResetDevice
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AppResetDevice(void)
{
    /* Local variables */
    uint32_t i;
    uint8_t                 tempArray[R_ADP_MAX_IB_SIZE];
    r_adp_adpm_reset_cnf_t *resCfm;

    /* Reset PAN ID and short address. */
    g_demo_entity.panId       = 0xFFFF;
    g_demo_entity.shortAddress = 0xFFFF;

    /* Set Eui64 and PSK */
    if (R_DEMO_AdpSetConfig(R_DEMO_G3_USE_PRIMARY_CH) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    /* Reset ADP after band plan setting. */
    if (!((R_DEMO_AdpmReset(R_DEMO_G3_USE_PRIMARY_CH, &resCfm) == R_RESULT_SUCCESS) &&
       (resCfm->status == R_ADP_STATUS_SUCCESS)))
    {
        return R_RESULT_FAILED;
    }

    if (R_DEMO_AdpmGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_SOFT_VERSION, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;   
    } 
    if (g_demo_config.verboseEnabled == R_TRUE)
    {

        R_STDIO_Printf("\n --- ADP Version :%d.%02d(0x%02X%02X)\r\n",tempArray[0],tempArray[1],tempArray[0],tempArray[1]);
        R_STDIO_Printf(" --- MAC Version :%d.%02d(0x%02X%02X)\r\n",tempArray[2],tempArray[3],tempArray[2],tempArray[3]);
        R_STDIO_Printf(" --- LMAC,PHY Version :0x%02X%02X\r\n",tempArray[4],tempArray[5]);
    }

    /* Set device type */
    if (R_DEMO_SetDeviceType() != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    
#ifdef R_SYNERGY_PLC
    /* Enable IPv6 in case it is not yet activated. */
    nxd_ipv6_enable(&ip_0);

    /* Delete registered IPv6 address in case it's already registered. */
    nxd_ipv6_address_delete(&ip_0,
                            g_demo_entity.ipv6AddressIndex);
#endif

    if(g_demo_config.appMode == R_FALSE)
    {
        return R_RESULT_SUCCESS;
    }

    /* Set Preserved Information */
    R_DEMO_AppPresetProcess();

    R_BYTE_UInt32ToArr(R_FRAMECOUNT_INTERVAL, tempArray);
    R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_FRAME_CNT_IND_INTERVAL, 0, tempArray);

    if(g_demo_config.routeType == R_G3_ROUTE_TYPE_JP_B)
    {
        /* Set context table entry 0. */
        tempArray[0] = R_TRUE;
        if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_DISABLE_DEFAULT_ROUTING, 0, (uint8_t *)tempArray) != R_ADP_STATUS_SUCCESS)
        {
            return R_RESULT_FAILED;   
        } 
    }

    /* Set tone mask. */
    R_DEMO_MlmeGetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_TONEMASK, 0, (uint8_t *)tempArray);
    for(i=0;i<9;i++)
    {
        tempArray[i] &= g_demo_config.tonemask[i];
    }
    if (R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_TONEMASK, 0, (uint8_t *)tempArray) != R_G3MAC_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

#ifndef R_SYNERGY_PLC
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_GROUP_TABLE, 0, (uint8_t *)g_group_table_entry_8001) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
#endif

    if(g_demo_config.appMode != R_DEMO_MODE_CERT)
    {
        return R_RESULT_SUCCESS;
    }

    /* Set context table entry 0. */
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_CONTEXT_INFORMATION_TABLE, 0, (uint8_t *)g_contextinfo_table_entry_00) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;   
    } 

    /* Set context table entrie 1. */
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_CONTEXT_INFORMATION_TABLE, 1, (uint8_t *)g_contextinfo_table_entry_01) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;   
    } 

    /* Set routing table TTL. */
    R_BYTE_UInt16ToArr(5, tempArray);
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_ROUTING_TABLE_ENTRY_TTL, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    
    /* Set blacklist table TTL. */
    R_BYTE_UInt16ToArr(2, tempArray);
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_BLACKLIST_TABLE_ENTRY_TTL, 0, tempArray) != R_ADP_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

#ifdef R_SYNERGY_PLC
    /* Join multicast group. */
    NX_IP_DRIVER                            driver_req;         /* Driver request structure. */

    /* Configure request (common settings). */
    driver_req.nx_ip_driver_command                 = NX_LINK_MULTICAST_JOIN;                           //Join group
    driver_req.nx_ip_driver_interface               = &ip_0.nx_ip_interface[R_DEMO_G3_USE_PRIMARY_CH];  //Pointer to interface
    driver_req.nx_ip_driver_packet                  = NULL;                                             //No packet to send
    driver_req.nx_ip_driver_ptr                     = &ip_0;                                            //IP instance
    driver_req.nx_ip_driver_return_ptr              = NULL;                                             //Nothing to return

    /* Configure request (multicast following conformance test specification, 64 bit IID only). */
    driver_req.nx_ip_driver_physical_address_msw    = 0x55660000;                                       //Most significant
    driver_req.nx_ip_driver_physical_address_lsw    = 0x01234567;                                       //Least significant

    /* Call G3 IPv6 network driver. */
    R_DEMO_G3_NetworkDriver(&driver_req);

    if (driver_req.nx_ip_driver_status != NX_SUCCESS)
#else
    /* Set group table entry. */
    if (R_DEMO_AdpmSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_ADP_IB_GROUP_TABLE, 1, (uint8_t *)g_group_table_entry_8567) != R_ADP_STATUS_SUCCESS)
#endif
    {
        return R_RESULT_FAILED;
    }
    
    /* Set TMR ttl. */
    R_BYTE_UInt32ToArr(2, tempArray);
    if (R_DEMO_MlmeSetWrap(R_DEMO_G3_USE_PRIMARY_CH, R_G3MAC_IB_TMR_TTL, 0, (uint8_t *)tempArray) != R_G3MAC_STATUS_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    
    return R_RESULT_SUCCESS;
}
/******************************************************************************
   End of function  R_DEMO_AppResetDevice
******************************************************************************/

