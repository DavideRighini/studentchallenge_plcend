/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_main.h
*    @version
*        $Rev: 1732 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-09-26 07:41:35 +0200#$
* Description : 
******************************************************************************/

#ifndef R_DEMO_APP_THREAD_H
#define R_DEMO_APP_THREAD_H

/******************************************************************************
Macro definitions
******************************************************************************/
#define R_DEMO_APP_HANDLE_DATA_IND              (0u)
#define R_DEMO_APP_HANDLE_LEAVE_IND             (1u)
#define R_DEMO_APP_HANDLE_BUFFER_IND            (2u)
#define R_DEMO_APP_HANDLE_STATUS_IND            (3u)
#define R_DEMO_APP_HANDLE_LBP_IND               (4u)
#define R_DEMO_APP_HANDLE_PATH_DIS_IND          (5u)
#define R_DEMO_APP_HANDLE_REBOOT_REQUEST_IND    (6u)
#define R_DEMO_APP_HANDLE_FRAMECOUNT_IND        (7u)
#define R_DEMO_APP_HANDLE_EAP_NETWORKJOIN_IND   (8u)
#define R_DEMO_APP_HANDLE_EAP_NETWORKLEAVE_IND  (9u)
#define R_DEMO_APP_HANDLE_EAP_NEWDEVICE_IND     (10u)
#define R_DEMO_APP_HANDLE_MAC_DATA_IND          (11u)
#define R_DEMO_APP_HANDLE_ADP_ROUTE_UPDATE_IND  (12u)
#define R_DEMO_APP_HANDLE_ADP_LOAD_SEQ_NUM_IND  (13u)
#define R_DEMO_APP_HANDLE_MAC_TMR_RCV_IND       (14u)
#define R_DEMO_APP_HANDLE_MAC_TMR_TRANSMIT_IND  (15u)

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Functions prottype
******************************************************************************/

/***********************************************************************
* Function Name     : R_DEMO_AppThreadInit
* Description       : Initializes the demo application thread
* Argument          : None
* Return Value      : R_RESULT_SUCCESS or R_RESULT_FAILED
***********************************************************************/
/*!
   \fn r_result_t R_DEMO_AppThreadInit(void)
   \brief Initializes the demo application thread
 */
r_result_t R_DEMO_AppThreadInit(void);

/***********************************************************************
* Function Name     : R_DEMO_AppThreadEnqueInd
* Description       : Enqueues incoming indications into application queue
* Argument          : indPtr : Pointer to indication structure
*                     handle : Handle associated with the indication
*                     size : Size of the structure to be enqueued
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppThreadEnqueInd(const uint8_t* indPtr,
                                                const uint8_t handle,
                                                const uint16_t size)
   \brief Enqueues incoming indications into application queue
 */
void R_DEMO_AppThreadEnqueInd(const uint8_t* indPtr,
                                         const uint8_t handle,
                                         const uint16_t size);

/***********************************************************************
* Function Name     : R_DEMO_AppThreadEnqueInd
* Description       : Enqueues incoming indications into application queue
* Argument          : indPtr : Pointer to indication structure
*                     handle : Handle associated with the indication
*                     size : Size of the structure to be enqueued
* Return Value      : None
***********************************************************************/
/*!
   \fn void R_DEMO_AppThreadEnqueDataInd(const r_adp_adpd_data_indication_t* ind)
   \brief Enqueues incoming indications into application queue
 */
void R_DEMO_AppThreadEnqueDataInd(const r_adp_adpd_data_ind_t* ind);

void R_DEMO_AppThreadEnqueMacDataInd(const r_g3mac_mcps_data_ind_t* ind);

void R_DEMO_AppThread(void);

#ifndef R_SYNERGY_PLC
r_boolean_t R_DEMO_WaitcnfTimeout();
void R_DEMO_WaitcnfTimerOn();
void R_DEMO_WaitcnfTimerOff();
#endif

#endif /* R_DEMO_APP_THREAD_H */
