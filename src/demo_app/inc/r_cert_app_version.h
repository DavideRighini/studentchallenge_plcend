﻿/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_cert_app_version.h
*    @version
*        $Rev: 1127 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-02-10 11:20:00 +0100#$
* Description : 
******************************************************************************/

#ifndef R_CERT_APP_VERSION_H
#define R_CERT_APP_VERSION_H

/******************************************************************************
Macro definitions
******************************************************************************/

#define R_CERT_APP_MAJOR_VERSION       (0x00)
#define R_CERT_APP_MINOR_VERSION       (0x04)

#define R_CERT_APP_VERSION             (R_CERT_APP_MAJOR_VERSION<<8 | R_CERT_APP_MINOR_VERSION)

/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Functions prottype
******************************************************************************/


#endif /* R_CERT_APP_VERSION_H */
